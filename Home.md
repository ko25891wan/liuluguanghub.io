

打开站点配置文件
![打开站点配置文件](_config.yml)
打开主题配置文件
![打开主题配置文件](themes/butterfly/_config.yml)
运行本地博客命令
![运行博客](order/RunBlog.bat)

清理博客缓存
![运行博客](order/clear.bat)

上传到gitee博客命令
![上传博客](order/upload.bat)
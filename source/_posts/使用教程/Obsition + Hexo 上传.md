---
title: Obsition + Hexo 上传
categories:
  - 使用教程
date: 2023-11-29 20:01:29
tags:
---
已完成教程：
1. https://zhuanlan.zhihu.com/p/554333805
1. [Hexo + Obsidian + Git 完美的博客部署与编辑方案 - 掘金 (juejin.cn)](https://juejin.cn/post/7120189614660255781#heading-8)
未完成教程：[Hexo&Obsidian不完全工作流 | 𝓐𝓮𝓻𝓸𝓼𝓪𝓷𝓭 (aerosand.cn)](https://aerosand.cn/posts/202201301024/)


## 1.目录结构
![Pasted image 20231129200626.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-50-41_b4962e9ed4fd832562a28bc7e51609d7_iRM6fjGVxG.png)

打开 obsition

## 2.忽略多余的文件

我们主要是编辑和管理Markdown文件，所以一些多余的文件要忽略掉，这样在知识库里搜索文件、关键字时才不会搜索到多余的，也能有效提高检索效率。

打开 设置>文件与链接>Exclude Files

![image.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-51-28_da9ef09fd724fd0b8792aca24840f5a6_mz3XXvUEJ7.png)


文章都在source下，所以只保留source，其它的忽略掉

![Pasted image 20231129200738.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-51-44_a283c15cd18057b0d0143b62ff00175f_YAN2tFEEBm.png)


## 3.博客文章管理

### 3.1 新建文章

新建Markdown文件就很方便了：

![image.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-52-04_6cc00735cd89b25bb690f01237c42fcc_wGuP6FoxPl.png)


可以设置下新建Markdown文件生成的位置：

![Pasted image 20231129200840.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-52-16_7c8de5dccf5b1888c04369ebfc187b94_Oc7RJf8W6l.png)


### 3.2 快速插入Front-matter模板

Obsidian有一个模板功能，允许我们预先写好一些内容模板，写文章时可以随时快捷插入。

在核心插件中开启模板功能：

打开模板插件的设置界面，设置模板文件夹位置以及日期格式：

![Pasted image 20231129200916.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-52-27_71621581310f8d94c247895794854868_d1NcYhUq5V.png)


编写Front-matter模板：

![Pasted image 20231129200939.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-52-37_e0d364bad82c18cdffd1c5f40f5eec52_QXbBG4iey7.png)


在左侧菜单栏中点插入模板：

![Pasted image 20231129201024.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-52-43_6ccf74485759c78bcefc41e18914a014_rFBf9ALhll.png)



## 4.Obsidian Git插件的使用

Obsidian知识库多端同步，我之前一直用的方案是OnDrive，除了.obsidian文件夹下个别文件会出现冲突以外，各方面都挺不错的。但是现在加入了Hexo博客后，冲突恐怕会更多，OnDrive没法处理，显然用Git来管理会更好，还有Hexo博客要部署到服务器一般也是用Git。

得益于Obsidian强大的插件生态，我们可以在Obsidian中直接操作Git，用到的是一款社区插件，叫做Obsidian Git

### 4.1 安装

先设置里启用社区插件功能，然后再搜索安装即可：
![Pasted image 20231129201146.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-53-10_a397de9094fb9cfc96bc64403442321e_4Md0uHGWH0.png)

要确保知识库是一个Git仓库，打开才不会报如下错误：

![image.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-53-31_2facdf880315d4d56f4b7cb500e1f6a3_t4ur0UcKiQ.png)


### 4.2 忽略配置



根目录目录下创建`.gitignore`， 

```
.DS_Store
Thumbs.db
db.json
*.log
node_modules/
public/
.deploy*/
_multiconfig.yml
```
忽略掉`.obsidian/workspace`

![Pasted image 20231129201213.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-53-42_56ca4cda6c9a3458809e0a10842bfd66_EGjCxX4lNA.png)

### 4.3 使用

打开插件设置界面，可以修改一下自动提交和手动提交的日志，我设置的是主机名+日期：



在提交信息设置里，可以修改主机名和日期格式，修改完成后点Preview可以预览提交信息：


快捷键`Ctrl + P`打开命令面板，输入open source control view启用可视化操作面板：

![image.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-54-01_58713ce23c9e5af3b6b8c952e293a93c_hda2RRj0Cy.png)


然后在右侧菜单栏就可以看到操作面板了：
![Pasted image 20231129201355.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-54-11_eec375fd89c039df75b6a3603b174700_V7JRIbXC3X.png)


一般操作就是：保存所有>提交>推送，就可以更新到Git服务器了，如下图顺序

![image.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-54-35_18da7d6abc31833be21883ae155544de_ZLQwJbmXEA.png)


启用自动拉取功能，每次打开知识库就会自动拉取：

![image.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-54-54_1e2b88941348d59ba3ccca5d226fef80_0obeq2mPzL.png)


如果在使用过程中有报错的话，`Ctrl+Shift+I`在控制台里可以查看详细日志，所有插件的日志都可以在这里看到：

![image.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-55-08_a8547de6c630b0c0e99668329416d9af_CxOOa9xRbF.png)


## 5.自动更新Front-matter分类信息

前文我们实现了Front-matter模板的快速插入，但是有些变量如categories还是需要手动维护。结合Obsidian，最好的方案就是我们通过文件夹来对文章进行分类，然后自动生成和更新Front-matter的分类信息。

### 5.1 hexo-auto-category安装与配置

这里我们使用的是[hexo-auto-category](https://github.com/xu-song/hexo-auto-category)这个基于文件夹自动分类的插件，安装：

```
npm install hexo-auto-category --save
```

在站点配置文件 中添加配置：

```
# Generate categories from directory-tree
# Dependencies: https://github.com/xu-song/hexo-auto-category
# depth: the max_depth of directory-tree you want to generate, should > 0

auto_category:
 enable: true
 depth:
```
![Pasted image 20231129201446.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-55-22_0e248f2e061f8fbfe05c1264aec306aa_WFIflDRNPS.png)



## 6利用Git钩子函数触发更新
### 6.1 根据文件目录自动生成 categories 信息

虽然我们把文章放的井井有条了，但是每个文章里的 `categorys` 字段还是要我们手动自己维护的，比如在 `source/_post/前端/Javascript/Javascript原型链机制.md` 文件中，我们要通过手写 `categories` 来让 hexo 知道这篇文章被放在 `前端-Javascript` 分类下：

```
title: Javascript原型链机制
categories:   
	- 前端   
	- Javascript 
date: 2022-06-05 12:06:47 

这里是正文`
```

为了省去手动维护 `categorys` 字段的这个问题，我们可以使用 [hexo-auto-category](https://link.juejin.cn?target=hexo-auto-category "hexo-auto-category") 这个插件。这个插件在 Hexo 进行 build 的时候会去自动根据文章目录情况来自动修改文章的 `categories` 信息，更详细的部分可以看[作者的文章](https://link.juejin.cn?target=https%3A%2F%2Fblog.eson.org%2Fpub%2Fe2f6e239%2F "https://blog.eson.org/pub/e2f6e239/")。

除此之外最好修改一下 `_config.yml` 中的两处默认配置：

```
# 修改 permalink 让你的文章链接更加友好，并且有益于 SEO 
permalink: :year/:month/:hash.html 

# 规定你的新文章在 _post 目录下是以 
cateory  new_post_name: :category/:title`
```

![Pasted image 20231129201957.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-55-34_06f0ac17f27025ec0902ba8898034981_EA0SihAs08.png)

### 6.2 提交代码时自动生成新文章的 categories

但是这里有一个问题，就是只有 hexo 在执行 `hexo generate` 或者 `hexo server` 时候才会去触发 `categories` 的生成，那么每次我们创建文章都要经历这样的工作流：

1. 创建分类目录，写文章，文件名推荐与文章标题一致（不用关心 `categories` 写什么）；
2. 填写 `title`、`date`、`tag` 等元信息（这个文章后续再讨论如何省去这一步）;
3. 执行 `npx hexo generate` 在构建博客的时候触发 `hexo-auto-category` 插件的自动矫正 `categories` 功能；
4. 检查文章中的 `categories` 是否正确；
5. 添加 git 工作区变更，并提交并推送代码到 github。

为了简化这些工作，我们可以使用 git hook，在我们每次执行 `commit` 前都自动运行 `npx hexo generate` 触发自动生成 `categories` 的行为，并将生成后的变更自动添加到本次提交中，然后一同 push 到 github 上去。这里可以使用 husky 来很方便的设置这样一个 git hook。

> GitHook 可以在执行代码的 commit、push、rebase 等阶段前触发，做一些前置行为，比如在每次提交代码时候执行一段 shell 脚本，来做一些代码检查或者通知 ci 等操作。
> 
> Husky 采用了更简单的一种方式，让管理 GitHook 更加现代化
> 
> 关于 Husky 的使用可以参考我之前的文章[《使用 husky 每次提交时进行代码检查》](https://link.juejin.cn?target=https%3A%2F%2Fblog.esunr.xyz%2F2022%2F05%2Fd36522b1089c.html "https://blog.esunr.xyz/2022/05/d36522b1089c.html")

你可以按照如下步骤快速完成设置：

1. 安装 huksy：`npm install husky --save-dev`
2. 执行 huksy 初始化指令：`npx husky install`
3. 在 `package.json` 中的 `scripts` 中写入：`"prepare": "husky install"`
4. 在生成的 `.husky` 目录创建 `pre-commit` 文件（有的话就不用创建），并写入以下内容：

```
#!/usr/bin/env sh
. "$(dirname -- "$0")/_/husky.sh"

npx hexo generate && git add .

```

如果提交代码的时候，终端出现类似的构建过程，就说明由 husky 创建的 git hook 生效了：

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-55-45_c2c0f8c4974d0b50bf525b03c9baf0cc_TM2gfMDrXp.awebp)

这样你新建一篇博客的工作流就简化为：

1. 创建分类目录，写文章；
2. 填写 `title`、`date`、`tag` 等元信息;
3. 添加 git 工作区变更，并提交并推送代码到 github。

这样就解决了令人头疼的文章分类问题~

## 7 使用 File Tree 插件

Obsidian 很不好的一点就是会把所有的文件都列在左侧的文件列表中，但是对于我们的 Hexo 项目写文章来说，我们只会修改 `_post` 目录下的文件，因此我们希望左侧的文件列表中只显示 `_post` 文件夹，但是目前为止 Obsidian 并没有推出类似『聚焦』到某一文件夹内的功能。

好在 Obsidian 强大的插件库中有一个 `File Tree Alternative Plugin` 第三方插件可以满足这一需求。按照 Obsidian Git 相同的方法去下载这个第三方插件，下载完成之后我们会发现左侧菜单出现了一个 `File Tree` 的 Tab 页，点击后就可以看到文件以树形的结构呈现：

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-55-51_e315bd30446d8b598b1f382350f8c33f_hJZJ3GxiIh.awebp)

我们展开 `source` 文件夹，并右键 `_post` 文件夹，选择 `Focuse on Folder` 后，左侧的文件列表中就只会显示 `_post` 文件夹中的内容了：

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-56-02_98d5d5370520724868afda55e2d84e97_b6LP0ejek9.awebp)

  

## 8.快捷操作

Obsidian中通过URL可以访问链接或打开文件，所以我们可以在根目录创建一个Markdown文件，相当于我们知识库的主页，把一些常用的链接放这里，方便快速访问：

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-56-10_c4ca4238a0b923820dcc509a6f75849b_RnXbp2voGh)

> **在写URL的时候要注意：如果路径有空格的话，要替换为%20**

### 8.1 快捷打开站点或主题配置文件

站点配置文件和主题配置文件是我们DIY博客经常要编辑的两个文件，在Obsidian中没法编辑yml文件，可以通过URL来打开yml文件，会自动调用默认的编辑器打开。

在主页Markdown中，按Ctrl+K插入链接，写入我们两个配置文件所在的相对路径：

```
[打开站点配置文件](Blog/_config.yml)
[打开主题配置文件](Blog/themes/butterfly4.3.1/_config.yml)

# 或者写成Obsidian URI的形式
[打开站点配置文件](obsidian://open?file=Blog/_config.yml)
[打开主题配置文件](obsidian://open?file=Blog/themes/butterfly4.3.1/_config.yml)
```

效果演示：

![动图封面](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-56-16_c4ca4238a0b923820dcc509a6f75849b_QfAxr3Z9ja)

### 8.2 快捷运行博客

我们还可以运行bat文件来执行一些命令，比如运行我们的博客了。

在我们的Hexo博客目录下，创建一个`RunBlog.bat`文件，写入以下命令：

```
start http://localhost:4000/
hexo s
```

然后在我们的主页添加链接：

```
[运行博客](Blog/RunBlog.bat)

# 或者
[运行博客](obsidian://open?file=Blog/RunBlog.bat)
```

效果演示：（测试完成后，按两次Ctrl+C关闭）

![动图封面](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-56-26_c4ca4238a0b923820dcc509a6f75849b_AqdbbFmbJT)

### 8.3 优化方案

### 8.3.1 嵌入文件

觉得链接太小了不好点击？Obsidian嵌入文件完美解决，只需在链接前加上感叹号!（注意嵌入文件的链接就不能用Obsidian URI的形式了）

```
打开站点配置文件
![打开站点配置文件](_config.yml)
打开主题配置文件
![打开主题配置文件](themes/butterfly/_config.yml)
运行博客命令
![运行博客](RunBlog.bat)
```

是不是瞬间舒服多了？

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_12-56-34_c4ca4238a0b923820dcc509a6f75849b_hfIuPa0LJt)
#### 注意： 运行博客命令后，cmd闪退，表示配置文件有误
---
categories:
  - 技术教程
  - mall
  - 架构篇
title: mall整合SpringSecurity和JWT实现认证和授权（二）
date: 2024-01-21 17:27:01
tags:
---
> 接上一篇，controller 和 service 层的代码实现及登录授权流程演示。

## [#](https://www.macrozheng.com/mall/architect/mall_arch_05.html#%F0%9F%91%8D-%E7%9B%B8%E5%85%B3%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B)👍 相关视频教程

[mall整合Spring Security和JWT实现认证和授权open in new window](https://t.zsxq.com/0et6nCQmM)

## [#](https://www.macrozheng.com/mall/architect/mall_arch_05.html#%E7%99%BB%E5%BD%95%E6%B3%A8%E5%86%8C%E5%8A%9F%E8%83%BD%E5%AE%9E%E7%8E%B0)登录注册功能实现

### [#](https://www.macrozheng.com/mall/architect/mall_arch_05.html#%E6%B7%BB%E5%8A%A0umsadmincontroller%E7%B1%BB)添加UmsAdminController类

> 实现了后台用户登录、注册及获取权限的接口

```
package com.macro.mall.tiny.controller;

import com.macro.mall.tiny.common.api.CommonResult;
import com.macro.mall.tiny.dto.UmsAdminLoginParam;
import com.macro.mall.tiny.mbg.model.UmsAdmin;
import com.macro.mall.tiny.mbg.model.UmsPermission;
import com.macro.mall.tiny.service.UmsAdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 后台用户管理
 * Created by macro on 2018/4/26.
 */
@Controller
@Api(tags = "UmsAdminController", description = "后台用户管理")
@RequestMapping("/admin")
public class UmsAdminController {
    @Autowired
    private UmsAdminService adminService;
    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @ApiOperation(value = "用户注册")
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<UmsAdmin> register(@RequestBody UmsAdmin umsAdminParam, BindingResult result) {
        UmsAdmin umsAdmin = adminService.register(umsAdminParam);
        if (umsAdmin == null) {
            CommonResult.failed();
        }
        return CommonResult.success(umsAdmin);
    }

    @ApiOperation(value = "登录以后返回token")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult login(@RequestBody UmsAdminLoginParam umsAdminLoginParam, BindingResult result) {
        String token = adminService.login(umsAdminLoginParam.getUsername(), umsAdminLoginParam.getPassword());
        if (token == null) {
            return CommonResult.validateFailed("用户名或密码错误");
        }
        Map<String, String> tokenMap = new HashMap<>();
        tokenMap.put("token", token);
        tokenMap.put("tokenHead", tokenHead);
        return CommonResult.success(tokenMap);
    }

    @ApiOperation("获取用户所有权限（包括+-权限）")
    @RequestMapping(value = "/permission/{adminId}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<List<UmsPermission>> getPermissionList(@PathVariable Long adminId) {
        List<UmsPermission> permissionList = adminService.getPermissionList(adminId);
        return CommonResult.success(permissionList);
    }
}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_05.html#%E6%B7%BB%E5%8A%A0umsadminservice%E6%8E%A5%E5%8F%A3)添加UmsAdminService接口

```
package com.macro.mall.tiny.service;

import com.macro.mall.tiny.mbg.model.UmsAdmin;
import com.macro.mall.tiny.mbg.model.UmsPermission;

import java.util.List;

/**
 * 后台管理员Service
 * Created by macro on 2018/4/26.
 */
public interface UmsAdminService {
    /**
     * 根据用户名获取后台管理员
     */
    UmsAdmin getAdminByUsername(String username);

    /**
     * 注册功能
     */
    UmsAdmin register(UmsAdmin umsAdminParam);

    /**
     * 登录功能
     * @param username 用户名
     * @param password 密码
     * @return 生成的JWT的token
     */
    String login(String username, String password);

    /**
     * 获取用户所有权限（包括角色权限和+-权限）
     */
    List<UmsPermission> getPermissionList(Long adminId);
}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_05.html#%E6%B7%BB%E5%8A%A0umsadminserviceimpl%E7%B1%BB)添加UmsAdminServiceImpl类

```
package com.macro.mall.tiny.service.impl;

import com.macro.mall.tiny.common.utils.JwtTokenUtil;
import com.macro.mall.tiny.dao.UmsAdminRoleRelationDao;
import com.macro.mall.tiny.dto.UmsAdminLoginParam;
import com.macro.mall.tiny.mbg.mapper.UmsAdminMapper;
import com.macro.mall.tiny.mbg.model.UmsAdmin;
import com.macro.mall.tiny.mbg.model.UmsAdminExample;
import com.macro.mall.tiny.mbg.model.UmsPermission;
import com.macro.mall.tiny.service.UmsAdminService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * UmsAdminService实现类
 * Created by macro on 2018/4/26.
 */
@Service
public class UmsAdminServiceImpl implements UmsAdminService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UmsAdminServiceImpl.class);
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Value("${jwt.tokenHead}")
    private String tokenHead;
    @Autowired
    private UmsAdminMapper adminMapper;
    @Autowired
    private UmsAdminRoleRelationDao adminRoleRelationDao;

    @Override
    public UmsAdmin getAdminByUsername(String username) {
        UmsAdminExample example = new UmsAdminExample();
        example.createCriteria().andUsernameEqualTo(username);
        List<UmsAdmin> adminList = adminMapper.selectByExample(example);
        if (adminList != null && adminList.size() > 0) {
            return adminList.get(0);
        }
        return null;
    }

    @Override
    public UmsAdmin register(UmsAdmin umsAdminParam) {
        UmsAdmin umsAdmin = new UmsAdmin();
        BeanUtils.copyProperties(umsAdminParam, umsAdmin);
        umsAdmin.setCreateTime(new Date());
        umsAdmin.setStatus(1);
        //查询是否有相同用户名的用户
        UmsAdminExample example = new UmsAdminExample();
        example.createCriteria().andUsernameEqualTo(umsAdmin.getUsername());
        List<UmsAdmin> umsAdminList = adminMapper.selectByExample(example);
        if (umsAdminList.size() > 0) {
            return null;
        }
        //将密码进行加密操作
        String encodePassword = passwordEncoder.encode(umsAdmin.getPassword());
        umsAdmin.setPassword(encodePassword);
        adminMapper.insert(umsAdmin);
        return umsAdmin;
    }

    @Override
    public String login(String username, String password) {
        String token = null;
        try {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            if (!passwordEncoder.matches(password, userDetails.getPassword())) {
                throw new BadCredentialsException("密码不正确");
            }
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
            token = jwtTokenUtil.generateToken(userDetails);
        } catch (AuthenticationException e) {
            LOGGER.warn("登录异常:{}", e.getMessage());
        }
        return token;
    }


    @Override
    public List<UmsPermission> getPermissionList(Long adminId) {
        return adminRoleRelationDao.getPermissionList(adminId);
    }
}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_05.html#%E4%BF%AE%E6%94%B9swagger%E7%9A%84%E9%85%8D%E7%BD%AE)修改Swagger的配置

> 通过修改配置实现调用接口自带Authorization头，这样就可以访问需要登录的接口了。

```
package com.macro.mall.tiny.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * Swagger2API文档的配置
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {
    @Bean
    public Docket createRestApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                //为当前包下controller生成API文档
                .apis(RequestHandlerSelectors.basePackage("com.macro.mall.tiny.controller"))
                .paths(PathSelectors.any())
                .build()
                //添加登录认证
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("SwaggerUI演示")
                .description("mall-tiny")
                .contact("macro")
                .version("1.0")
                .build();
    }

    private List<ApiKey> securitySchemes() {
        //设置请求头信息
        List<ApiKey> result = new ArrayList<>();
        ApiKey apiKey = new ApiKey("Authorization", "Authorization", "header");
        result.add(apiKey);
        return result;
    }

    private List<SecurityContext> securityContexts() {
        //设置需要登录认证的路径
        List<SecurityContext> result = new ArrayList<>();
        result.add(getContextByPath("/brand/.*"));
        return result;
    }

    private SecurityContext getContextByPath(String pathRegex){
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.regex(pathRegex))
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        List<SecurityReference> result = new ArrayList<>();
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        result.add(new SecurityReference("Authorization", authorizationScopes));
        return result;
    }
}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_05.html#%E7%BB%99pmsbrandcontroller%E6%8E%A5%E5%8F%A3%E4%B8%AD%E7%9A%84%E6%96%B9%E6%B3%95%E6%B7%BB%E5%8A%A0%E8%AE%BF%E9%97%AE%E6%9D%83%E9%99%90)给PmsBrandController接口中的方法添加访问权限

- 给查询接口添加pms:brand:read权限
- 给修改接口添加pms:brand:update权限
- 给删除接口添加pms:brand:delete权限
- 给添加接口添加pms:brand:create权限

例子：

```
@PreAuthorize("hasAuthority('pms:brand:read')")
public CommonResult<List<PmsBrand>> getBrandList() {
    return CommonResult.success(brandService.listAllBrand());
}
```

## [#](https://www.macrozheng.com/mall/architect/mall_arch_05.html#%E8%AE%A4%E8%AF%81%E4%B8%8E%E6%8E%88%E6%9D%83%E6%B5%81%E7%A8%8B%E6%BC%94%E7%A4%BA)认证与授权流程演示

### [#](https://www.macrozheng.com/mall/architect/mall_arch_05.html#%E8%BF%90%E8%A1%8C%E9%A1%B9%E7%9B%AE-%E8%AE%BF%E9%97%AEapi)运行项目，访问API

Swagger api地址：http://localhost:8080/swagger-ui.html

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:45_arch_screen_14-d0e56003.png)

### [#](https://www.macrozheng.com/mall/architect/mall_arch_05.html#%E6%9C%AA%E7%99%BB%E5%BD%95%E5%89%8D%E8%AE%BF%E9%97%AE%E6%8E%A5%E5%8F%A3)未登录前访问接口

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:45_arch_screen_15-df1c06fc.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:45_arch_screen_16-5a767892.png)

### [#](https://www.macrozheng.com/mall/architect/mall_arch_05.html#%E7%99%BB%E5%BD%95%E5%90%8E%E8%AE%BF%E9%97%AE%E6%8E%A5%E5%8F%A3)登录后访问接口

- 进行登录操作：登录帐号test 123456

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:45_arch_screen_17-31a5112f.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:45_arch_screen_18-fde07ce7.png)

- 点击Authorize按钮，在弹框中输入登录接口中获取到的token信息

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:45_arch_screen_19-510cc7c4.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:45_arch_screen_20-6d041b76.png)

- 登录后访问获取权限列表接口，发现已经可以正常访问

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:45_arch_screen_15-df1c06fc.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:45_arch_screen_21-42d126b5.png)

### [#](https://www.macrozheng.com/mall/architect/mall_arch_05.html#%E8%AE%BF%E9%97%AE%E9%9C%80%E8%A6%81%E6%9D%83%E9%99%90%E7%9A%84%E6%8E%A5%E5%8F%A3)访问需要权限的接口

> 由于test帐号并没有设置任何权限，所以他无法访问具有pms:brand:read权限的获取品牌列表接口。

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:45_arch_screen_22-b275addc.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:45_arch_screen_23-4203dba6.png)

### [#](https://www.macrozheng.com/mall/architect/mall_arch_05.html#%E6%94%B9%E7%94%A8%E5%85%B6%E4%BB%96%E6%9C%89%E6%9D%83%E9%99%90%E7%9A%84%E5%B8%90%E5%8F%B7%E7%99%BB%E5%BD%95)改用其他有权限的帐号登录

> 改用admin 123456登录后访问，点击Authorize按钮打开弹框,点击logout登出后再重新输入新token。

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:45_arch_screen_22-b275addc.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:45_arch_screen_24-be4a59e3.png)

## [#](https://www.macrozheng.com/mall/architect/mall_arch_05.html#%E9%A1%B9%E7%9B%AE%E6%BA%90%E7%A0%81%E5%9C%B0%E5%9D%80)项目源码地址

[https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-04](https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-04)
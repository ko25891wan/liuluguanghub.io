---
categories:
  - 技术教程
  - mall
  - 架构篇
title: mall整合MongoDB实现文档操作
date: 2024-01-21 17:27:16
tags:
---
> 本文主要讲解 mall 整合 Mongodb 的过程，以实现商品浏览记录在 Mongodb 中的添加、删除、查询为例。

## [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%F0%9F%91%8D-%E7%9B%B8%E5%85%B3%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B)👍 相关视频教程

[mall整合MongoDB实现文档操作open in new window](https://t.zsxq.com/0e3SQlzVw)

## [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E9%A1%B9%E7%9B%AE%E4%BD%BF%E7%94%A8%E6%A1%86%E6%9E%B6%E4%BB%8B%E7%BB%8D)项目使用框架介绍

### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#mongodb)Mongodb

> Mongodb是为快速开发互联网Web应用而构建的数据库系统，其数据模型和持久化策略就是为了构建高读/写吞吐量和高自动灾备伸缩性的系统。

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#mongodb%E7%9A%84%E5%AE%89%E8%A3%85%E5%92%8C%E4%BD%BF%E7%94%A8)Mongodb的安装和使用

1. 下载Mongodb安装包，下载地址：[https://fastdl.mongodb.org/win32/mongodb-win32-x86_64-2008plus-ssl-3.2.21-signed.msiopen in new window](https://fastdl.mongodb.org/win32/mongodb-win32-x86_64-2008plus-ssl-3.2.21-signed.msi)
    
2. 选择安装路径进行安装
    

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:06:39_arch_screen_37-91e60eec.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:06:39_arch_screen_38-38adc5fb.png)

3. 在安装路径下创建data\db和data\log两个文件夹

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:06:39_arch_screen_39-953a7588.png)

4. 在安装路径下创建mongod.cfg配置文件

```
systemLog:
    destination: file
    path: D:\developer\env\MongoDB\data\log\mongod.log
storage:
    dbPath: D:\developer\env\MongoDB\data\db
```

5. 安装为服务（运行命令需要用管理员权限）

```
D:\developer\env\MongoDB\bin\mongod.exe --config "D:\developer\env\MongoDB\mongod.cfg" --install
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:06:39_arch_screen_40-676ced03.png)

6. 服务相关命令

```
启动服务：net start MongoDB
关闭服务：net stop MongoDB
移除服务：D:\developer\env\MongoDB\bin\mongod.exe --remove
```

7. 下载客户端程序：[https://download.robomongo.org/1.2.1/windows/robo3t-1.2.1-windows-x86_64-3e50a65.zipopen in new window](https://download.robomongo.org/1.2.1/windows/robo3t-1.2.1-windows-x86_64-3e50a65.zip)
    
8. 解压到指定目录，打开robo3t.exe并连接到localhost:27017
    

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:06:39_arch_screen_41-ec3d5223.png)

### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#spring-data-mongodb)Spring Data Mongodb

> 和Spring Data Elasticsearch类似，Spring Data Mongodb是Spring提供的一种以Spring Data风格来操作数据存储的方式，它可以避免编写大量的样板代码。

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E5%B8%B8%E7%94%A8%E6%B3%A8%E8%A7%A3)常用注解

- @Document:标示映射到Mongodb文档上的领域对象
- @Id:标示某个域为ID域
- @Indexed:标示某个字段为Mongodb的索引字段

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#sping-data%E6%96%B9%E5%BC%8F%E7%9A%84%E6%95%B0%E6%8D%AE%E6%93%8D%E4%BD%9C)Sping Data方式的数据操作

##### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E7%BB%A7%E6%89%BFmongorepository%E6%8E%A5%E5%8F%A3%E5%8F%AF%E4%BB%A5%E8%8E%B7%E5%BE%97%E5%B8%B8%E7%94%A8%E7%9A%84%E6%95%B0%E6%8D%AE%E6%93%8D%E4%BD%9C%E6%96%B9%E6%B3%95)继承MongoRepository接口可以获得常用的数据操作方法

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:06:39_arch_screen_42-0a75a3c4.png)

##### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E5%8F%AF%E4%BB%A5%E4%BD%BF%E7%94%A8%E8%A1%8D%E7%94%9F%E6%9F%A5%E8%AF%A2)可以使用衍生查询

> 在接口中直接指定查询方法名称便可查询，无需进行实现，以下为根据会员id按时间倒序获取浏览记录的例子。

```
/**
 * 会员商品浏览历史Repository
 * Created by macro on 2018/8/3.
 */
public interface MemberReadHistoryRepository extends MongoRepository<MemberReadHistory,String> {
    /**
     * 根据会员id按时间倒序获取浏览记录
     * @param memberId 会员id
     */
    List<MemberReadHistory> findByMemberIdOrderByCreateTimeDesc(Long memberId);
}
```

> 在idea中直接会提示对应字段

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:06:39_arch_screen_43-07ef59a9.png)

##### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E4%BD%BF%E7%94%A8-query%E6%B3%A8%E8%A7%A3%E5%8F%AF%E4%BB%A5%E7%94%A8mongodb%E7%9A%84json%E6%9F%A5%E8%AF%A2%E8%AF%AD%E5%8F%A5%E8%BF%9B%E8%A1%8C%E6%9F%A5%E8%AF%A2)使用@Query注解可以用Mongodb的JSON查询语句进行查询

```
@Query("{ 'memberId' : ?0 }")
List<MemberReadHistory> findByMemberId(Long memberId);
```

## [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E6%95%B4%E5%90%88mongodb%E5%AE%9E%E7%8E%B0%E6%96%87%E6%A1%A3%E6%93%8D%E4%BD%9C)整合Mongodb实现文档操作

### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E5%9C%A8pom-xml%E4%B8%AD%E6%B7%BB%E5%8A%A0%E7%9B%B8%E5%85%B3%E4%BE%9D%E8%B5%96)在pom.xml中添加相关依赖

```
<!---mongodb相关依赖-->
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-data-mongodb</artifactId>
</dependency>
```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E4%BF%AE%E6%94%B9springboot%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6)修改SpringBoot配置文件

> 修改application.yml文件，在spring:data节点下添加Mongodb相关配置。

```
mongodb:
  host: localhost # mongodb的连接地址
  port: 27017 # mongodb的连接端口号
  database: mall-port # mongodb的连接的数据库
```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E6%B7%BB%E5%8A%A0%E4%BC%9A%E5%91%98%E6%B5%8F%E8%A7%88%E8%AE%B0%E5%BD%95%E6%96%87%E6%A1%A3%E5%AF%B9%E8%B1%A1memberreadhistory)添加会员浏览记录文档对象MemberReadHistory

> 文档对象的ID域添加@Id注解，需要检索的字段添加@Indexed注解。

```
package com.macro.mall.tiny.nosql.mongodb.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * 用户商品浏览历史记录
 * Created by macro on 2018/8/3.
 */
@Document
public class MemberReadHistory {
    @Id
    private String id;
    @Indexed
    private Long memberId;
    private String memberNickname;
    private String memberIcon;
    @Indexed
    private Long productId;
    private String productName;
    private String productPic;
    private String productSubTitle;
    private String productPrice;
    private Date createTime;

    //省略了所有getter和setter方法

}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E6%B7%BB%E5%8A%A0memberreadhistoryrepository%E6%8E%A5%E5%8F%A3%E7%94%A8%E4%BA%8E%E6%93%8D%E4%BD%9Cmongodb)添加MemberReadHistoryRepository接口用于操作Mongodb

> 继承MongoRepository接口，这样就拥有了一些基本的Mongodb数据操作方法，同时定义了一个衍生查询方法。

```
package com.macro.mall.tiny.nosql.mongodb.repository;


import com.macro.mall.tiny.nosql.mongodb.document.MemberReadHistory;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * 会员商品浏览历史Repository
 * Created by macro on 2018/8/3.
 */
public interface MemberReadHistoryRepository extends MongoRepository<MemberReadHistory,String> {
    /**
     * 根据会员id按时间倒序获取浏览记录
     * @param memberId 会员id
     */
    List<MemberReadHistory> findByMemberIdOrderByCreateTimeDesc(Long memberId);
}
```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E6%B7%BB%E5%8A%A0memberreadhistoryservice%E6%8E%A5%E5%8F%A3)添加MemberReadHistoryService接口

```
package com.macro.mall.tiny.service;


import com.macro.mall.tiny.nosql.mongodb.document.MemberReadHistory;

import java.util.List;

/**
 * 会员浏览记录管理Service
 * Created by macro on 2018/8/3.
 */
public interface MemberReadHistoryService {
    /**
     * 生成浏览记录
     */
    int create(MemberReadHistory memberReadHistory);

    /**
     * 批量删除浏览记录
     */
    int delete(List<String> ids);

    /**
     * 获取用户浏览历史记录
     */
    List<MemberReadHistory> list(Long memberId);
}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E6%B7%BB%E5%8A%A0memberreadhistoryservice%E6%8E%A5%E5%8F%A3%E5%AE%9E%E7%8E%B0%E7%B1%BBmemberreadhistoryserviceimpl)添加MemberReadHistoryService接口实现类MemberReadHistoryServiceImpl

```
package com.macro.mall.tiny.service.impl;

import com.macro.mall.tiny.nosql.mongodb.document.MemberReadHistory;
import com.macro.mall.tiny.nosql.mongodb.repository.MemberReadHistoryRepository;
import com.macro.mall.tiny.service.MemberReadHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 会员浏览记录管理Service实现类
 * Created by macro on 2018/8/3.
 */
@Service
public class MemberReadHistoryServiceImpl implements MemberReadHistoryService {
    @Autowired
    private MemberReadHistoryRepository memberReadHistoryRepository;
    @Override
    public int create(MemberReadHistory memberReadHistory) {
        memberReadHistory.setId(null);
        memberReadHistory.setCreateTime(new Date());
        memberReadHistoryRepository.save(memberReadHistory);
        return 1;
    }

    @Override
    public int delete(List<String> ids) {
        List<MemberReadHistory> deleteList = new ArrayList<>();
        for(String id:ids){
            MemberReadHistory memberReadHistory = new MemberReadHistory();
            memberReadHistory.setId(id);
            deleteList.add(memberReadHistory);
        }
        memberReadHistoryRepository.deleteAll(deleteList);
        return ids.size();
    }

    @Override
    public List<MemberReadHistory> list(Long memberId) {
        return memberReadHistoryRepository.findByMemberIdOrderByCreateTimeDesc(memberId);
    }
}
```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E6%B7%BB%E5%8A%A0memberreadhistorycontroller%E5%AE%9A%E4%B9%89%E6%8E%A5%E5%8F%A3)添加MemberReadHistoryController定义接口

```
package com.macro.mall.tiny.controller;

import com.macro.mall.tiny.common.api.CommonResult;
import com.macro.mall.tiny.nosql.mongodb.document.MemberReadHistory;
import com.macro.mall.tiny.service.MemberReadHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 会员商品浏览记录管理Controller
 * Created by macro on 2018/8/3.
 */
@Controller
@Api(tags = "MemberReadHistoryController", description = "会员商品浏览记录管理")
@RequestMapping("/member/readHistory")
public class MemberReadHistoryController {
    @Autowired
    private MemberReadHistoryService memberReadHistoryService;

    @ApiOperation("创建浏览记录")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult create(@RequestBody MemberReadHistory memberReadHistory) {
        int count = memberReadHistoryService.create(memberReadHistory);
        if (count > 0) {
            return CommonResult.success(count);
        } else {
            return CommonResult.failed();
        }
    }

    @ApiOperation("删除浏览记录")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult delete(@RequestParam("ids") List<String> ids) {
        int count = memberReadHistoryService.delete(ids);
        if (count > 0) {
            return CommonResult.success(count);
        } else {
            return CommonResult.failed();
        }
    }

    @ApiOperation("展示浏览记录")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<List<MemberReadHistory>> list(Long memberId) {
        List<MemberReadHistory> memberReadHistoryList = memberReadHistoryService.list(memberId);
        return CommonResult.success(memberReadHistoryList);
    }
}

```

## [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E8%BF%9B%E8%A1%8C%E6%8E%A5%E5%8F%A3%E6%B5%8B%E8%AF%95)进行接口测试

### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E6%B7%BB%E5%8A%A0%E5%95%86%E5%93%81%E6%B5%8F%E8%A7%88%E8%AE%B0%E5%BD%95%E5%88%B0mongodb)添加商品浏览记录到Mongodb

![](undefined)

### [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E6%9F%A5%E8%AF%A2mongodb%E4%B8%AD%E7%9A%84%E5%95%86%E5%93%81%E6%B5%8F%E8%A7%88%E8%AE%B0%E5%BD%95)查询Mongodb中的商品浏览记录

![](undefined)

## [#](https://www.macrozheng.com/mall/architect/mall_arch_08.html#%E9%A1%B9%E7%9B%AE%E6%BA%90%E7%A0%81%E5%9C%B0%E5%9D%80)项目源码地址

[https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-07](https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-07)
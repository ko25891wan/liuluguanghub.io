---
categories:
  - 技术教程
  - mall
  - 架构篇
title: mall整合RabbitMQ实现延迟消息
date: 2024-01-21 17:27:09
tags:
---
> 本文主要讲解 mall 整合 RabbitMQ 实现延迟消息的过程，以发送延迟消息取消超时订单为例。

## [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%F0%9F%91%8D-%E7%9B%B8%E5%85%B3%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B)👍 相关视频教程

[mall整合RabbitMQ实现延迟消息open in new window](https://t.zsxq.com/0eYUeTaBk)

## [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E9%A1%B9%E7%9B%AE%E4%BD%BF%E7%94%A8%E6%A1%86%E6%9E%B6%E4%BB%8B%E7%BB%8D)项目使用框架介绍

### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#rabbitmq)RabbitMQ

> RabbitMQ是一个被广泛使用的开源消息队列。它是轻量级且易于部署的，它能支持多种消息协议。RabbitMQ可以部署在分布式和联合配置中，以满足高规模、高可用性的需求。

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#rabbitmq%E7%9A%84%E5%AE%89%E8%A3%85%E5%92%8C%E4%BD%BF%E7%94%A8)RabbitMQ的安装和使用

1. 安装Erlang，下载地址：[http://erlang.org/download/otp_win64_21.3.exeopen in new window](http://erlang.org/download/otp_win64_21.3.exe)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:07:44_arch_screen_53-e6e83c02.png)

2. 安装RabbitMQ，下载地址：[https://dl.bintray.com/rabbitmq/all/rabbitmq-server/3.7.14/rabbitmq-server-3.7.14.exeopen in new window](https://dl.bintray.com/rabbitmq/all/rabbitmq-server/3.7.14/rabbitmq-server-3.7.14.exe)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:07:44_arch_screen_54-a1fa2b98.png)

3. 安装完成后，进入RabbitMQ安装目录下的sbin目录

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:07:44_arch_screen_55-cff0e57a.png)

4. 在地址栏输入cmd并回车启动命令行，然后输入以下命令启动管理功能：

```
rabbitmq-plugins enable rabbitmq_management
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:07:44_arch_screen_56-ebd1846b.png)

5. 访问地址查看是否安装成功：[http://localhost:15672/open in new window](http://localhost:15672/)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:07:44_arch_screen_57-f0fc20b9.png)

6. 输入账号密码并登录：guest guest
    
7. 创建帐号并设置其角色为管理员：mall mall
    

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:07:44_arch_screen_58-2d67e0fe.png)

8. 创建一个新的虚拟host为：/mall

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:07:44_arch_screen_59-6d1d4f9f.png)

9. 点击mall用户进入用户配置页面

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:07:44_arch_screen_60-6ad3fe16.png)

10. 给mall用户配置该虚拟host的权限

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:07:44_arch_screen_61-f2470471.png)

11. 至此，RabbitMQ的安装和配置完成。

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#rabbitmq%E7%9A%84%E6%B6%88%E6%81%AF%E6%A8%A1%E5%9E%8B)RabbitMQ的消息模型

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:07:44_arch_screen_52-b0519322.png)

|标志|中文名|英文名|描述|
|---|---|---|---|
|P|生产者|Producer|消息的发送者，可以将消息发送到交换机|
|C|消费者|Consumer|消息的接收者，从队列中获取消息进行消费|
|X|交换机|Exchange|接收生产者发送的消息，并根据路由键发送给指定队列|
|Q|队列|Queue|存储从交换机发来的消息|
|type|交换机类型|type|direct表示直接根据路由键（orange/black）发送消息|

### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#lombok)Lombok

> Lombok为Java语言添加了非常有趣的附加功能，你可以不用再为实体类手写getter,setter等方法，通过一个注解即可拥有。

注意：需要安装idea的Lombok插件，并在项目中的pom文件中添加依赖。

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:07:44_arch_screen_48-79b8b569.png)

## [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E4%B8%9A%E5%8A%A1%E5%9C%BA%E6%99%AF%E8%AF%B4%E6%98%8E)业务场景说明

> 用于解决用户下单以后，订单超时如何取消订单的问题。

- 用户进行下单操作（会有锁定商品库存、使用优惠券、积分一系列的操作）；
- 生成订单，获取订单的id；
- 获取到设置的订单超时时间（假设设置的为60分钟不支付取消订单）；
- 按订单超时时间发送一个延迟消息给RabbitMQ，让它在订单超时后触发取消订单的操作；
- 如果用户没有支付，进行取消订单操作（释放锁定商品库存、返还优惠券、返回积分一系列操作）。

## [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E6%95%B4%E5%90%88rabbitmq%E5%AE%9E%E7%8E%B0%E5%BB%B6%E8%BF%9F%E6%B6%88%E6%81%AF)整合RabbitMQ实现延迟消息

### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E5%9C%A8pom-xml%E4%B8%AD%E6%B7%BB%E5%8A%A0%E7%9B%B8%E5%85%B3%E4%BE%9D%E8%B5%96)在pom.xml中添加相关依赖

```
<!--消息队列相关依赖-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-amqp</artifactId>
</dependency>
<!--lombok依赖-->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <optional>true</optional>
</dependency>
```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E4%BF%AE%E6%94%B9springboot%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6)修改SpringBoot配置文件

> 修改application.yml文件，在spring节点下添加Mongodb相关配置。

```
  rabbitmq:
    host: localhost # rabbitmq的连接地址
    port: 5672 # rabbitmq的连接端口号
    virtual-host: /mall # rabbitmq的虚拟host
    username: mall # rabbitmq的用户名
    password: mall # rabbitmq的密码
    publisher-confirms: true #如果对异步消息需要回调必须设置为true
```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E6%B7%BB%E5%8A%A0%E6%B6%88%E6%81%AF%E9%98%9F%E5%88%97%E7%9A%84%E6%9E%9A%E4%B8%BE%E9%85%8D%E7%BD%AE%E7%B1%BBqueueenum)添加消息队列的枚举配置类QueueEnum

> 用于延迟消息队列及处理取消订单消息队列的常量定义，包括交换机名称、队列名称、路由键名称。

```
package com.macro.mall.tiny.dto;

import lombok.Getter;

/**
 * 消息队列枚举配置
 * Created by macro on 2018/9/14.
 */
@Getter
public enum QueueEnum {
    /**
     * 消息通知队列
     */
    QUEUE_ORDER_CANCEL("mall.order.direct", "mall.order.cancel", "mall.order.cancel"),
    /**
     * 消息通知ttl队列
     */
    QUEUE_TTL_ORDER_CANCEL("mall.order.direct.ttl", "mall.order.cancel.ttl", "mall.order.cancel.ttl");

    /**
     * 交换名称
     */
    private String exchange;
    /**
     * 队列名称
     */
    private String name;
    /**
     * 路由键
     */
    private String routeKey;

    QueueEnum(String exchange, String name, String routeKey) {
        this.exchange = exchange;
        this.name = name;
        this.routeKey = routeKey;
    }
}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E6%B7%BB%E5%8A%A0rabbitmq%E7%9A%84%E9%85%8D%E7%BD%AE)添加RabbitMQ的配置

> 用于配置交换机、队列及队列与交换机的绑定关系。

```
package com.macro.mall.tiny.config;

import com.macro.mall.tiny.dto.QueueEnum;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 消息队列配置
 * Created by macro on 2018/9/14.
 */
@Configuration
public class RabbitMqConfig {

    /**
     * 订单消息实际消费队列所绑定的交换机
     */
    @Bean
    DirectExchange orderDirect() {
        return (DirectExchange) ExchangeBuilder
                .directExchange(QueueEnum.QUEUE_ORDER_CANCEL.getExchange())
                .durable(true)
                .build();
    }

    /**
     * 订单延迟队列队列所绑定的交换机
     */
    @Bean
    DirectExchange orderTtlDirect() {
        return (DirectExchange) ExchangeBuilder
                .directExchange(QueueEnum.QUEUE_TTL_ORDER_CANCEL.getExchange())
                .durable(true)
                .build();
    }

    /**
     * 订单实际消费队列
     */
    @Bean
    public Queue orderQueue() {
        return new Queue(QueueEnum.QUEUE_ORDER_CANCEL.getName());
    }

    /**
     * 订单延迟队列（死信队列）
     */
    @Bean
    public Queue orderTtlQueue() {
        return QueueBuilder
                .durable(QueueEnum.QUEUE_TTL_ORDER_CANCEL.getName())
                .withArgument("x-dead-letter-exchange", QueueEnum.QUEUE_ORDER_CANCEL.getExchange())//到期后转发的交换机
                .withArgument("x-dead-letter-routing-key", QueueEnum.QUEUE_ORDER_CANCEL.getRouteKey())//到期后转发的路由键
                .build();
    }

    /**
     * 将订单队列绑定到交换机
     */
    @Bean
    Binding orderBinding(DirectExchange orderDirect,Queue orderQueue){
        return BindingBuilder
                .bind(orderQueue)
                .to(orderDirect)
                .with(QueueEnum.QUEUE_ORDER_CANCEL.getRouteKey());
    }

    /**
     * 将订单延迟队列绑定到交换机
     */
    @Bean
    Binding orderTtlBinding(DirectExchange orderTtlDirect,Queue orderTtlQueue){
        return BindingBuilder
                .bind(orderTtlQueue)
                .to(orderTtlDirect)
                .with(QueueEnum.QUEUE_TTL_ORDER_CANCEL.getRouteKey());
    }

}
```

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E5%9C%A8rabbitmq%E7%AE%A1%E7%90%86%E9%A1%B5%E9%9D%A2%E5%8F%AF%E4%BB%A5%E7%9C%8B%E5%88%B0%E4%BB%A5%E4%B8%8B%E4%BA%A4%E6%8D%A2%E6%9C%BA%E5%92%8C%E9%98%9F%E5%88%97)在RabbitMQ管理页面可以看到以下交换机和队列

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:07:44_arch_screen_63-56651c53.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:07:44_arch_screen_65-f8f39713.png)

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E4%BA%A4%E6%8D%A2%E6%9C%BA%E5%8F%8A%E9%98%9F%E5%88%97%E8%AF%B4%E6%98%8E)交换机及队列说明

- mall.order.direct（取消订单消息队列所绑定的交换机）:绑定的队列为mall.order.cancel，一旦有消息以mall.order.cancel为路由键发过来，会发送到此队列。
- mall.order.direct.ttl（订单延迟消息队列所绑定的交换机）:绑定的队列为mall.order.cancel.ttl，一旦有消息以mall.order.cancel.ttl为路由键发送过来，会转发到此队列，并在此队列保存一定时间，等到超时后会自动将消息发送到mall.order.cancel（取消订单消息消费队列）。

### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E6%B7%BB%E5%8A%A0%E5%BB%B6%E8%BF%9F%E6%B6%88%E6%81%AF%E7%9A%84%E5%8F%91%E9%80%81%E8%80%85cancelordersender)添加延迟消息的发送者CancelOrderSender

> 用于向订单延迟消息队列（mall.order.cancel.ttl）里发送消息。

```
package com.macro.mall.tiny.component;

import com.macro.mall.tiny.dto.QueueEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 取消订单消息的发出者
 * Created by macro on 2018/9/14.
 */
@Component
public class CancelOrderSender {
    private static Logger LOGGER =LoggerFactory.getLogger(CancelOrderSender.class);
    @Autowired
    private AmqpTemplate amqpTemplate;

    public void sendMessage(Long orderId,final long delayTimes){
        //给延迟队列发送消息
        amqpTemplate.convertAndSend(QueueEnum.QUEUE_TTL_ORDER_CANCEL.getExchange(), QueueEnum.QUEUE_TTL_ORDER_CANCEL.getRouteKey(), orderId, new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws AmqpException {
                //给消息设置延迟毫秒值
                message.getMessageProperties().setExpiration(String.valueOf(delayTimes));
                return message;
            }
        });
        LOGGER.info("send delay message orderId:{}",orderId);
    }
}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E6%B7%BB%E5%8A%A0%E5%8F%96%E6%B6%88%E8%AE%A2%E5%8D%95%E6%B6%88%E6%81%AF%E7%9A%84%E6%8E%A5%E6%94%B6%E8%80%85cancelorderreceiver)添加取消订单消息的接收者CancelOrderReceiver

> 用于从取消订单的消息队列（mall.order.cancel）里接收消息。

```
package com.macro.mall.tiny.component;

import com.macro.mall.tiny.service.OmsPortalOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 取消订单消息的处理者
 * Created by macro on 2018/9/14.
 */
@Component
@RabbitListener(queues = "mall.order.cancel")
public class CancelOrderReceiver {
    private static Logger LOGGER =LoggerFactory.getLogger(CancelOrderReceiver.class);
    @Autowired
    private OmsPortalOrderService portalOrderService;
    @RabbitHandler
    public void handle(Long orderId){
        LOGGER.info("receive delay message orderId:{}",orderId);
        portalOrderService.cancelOrder(orderId);
    }
}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E6%B7%BB%E5%8A%A0omsportalorderservice%E6%8E%A5%E5%8F%A3)添加OmsPortalOrderService接口

```
package com.macro.mall.tiny.service;

import com.macro.mall.tiny.common.api.CommonResult;
import com.macro.mall.tiny.dto.OrderParam;
import org.springframework.transaction.annotation.Transactional;

/**
 * 前台订单管理Service
 * Created by macro on 2018/8/30.
 */
public interface OmsPortalOrderService {

    /**
     * 根据提交信息生成订单
     */
    @Transactional
    CommonResult generateOrder(OrderParam orderParam);

    /**
     * 取消单个超时订单
     */
    @Transactional
    void cancelOrder(Long orderId);
}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E6%B7%BB%E5%8A%A0omsportalorderservice%E7%9A%84%E5%AE%9E%E7%8E%B0%E7%B1%BBomsportalorderserviceimpl)添加OmsPortalOrderService的实现类OmsPortalOrderServiceImpl

```
package com.macro.mall.tiny.service.impl;

import com.macro.mall.tiny.common.api.CommonResult;
import com.macro.mall.tiny.component.CancelOrderSender;
import com.macro.mall.tiny.dto.OrderParam;
import com.macro.mall.tiny.service.OmsPortalOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 前台订单管理Service
 * Created by macro on 2018/8/30.
 */
@Service
public class OmsPortalOrderServiceImpl implements OmsPortalOrderService {
    private static Logger LOGGER = LoggerFactory.getLogger(OmsPortalOrderServiceImpl.class);
    @Autowired
    private CancelOrderSender cancelOrderSender;

    @Override
    public CommonResult generateOrder(OrderParam orderParam) {
        //todo 执行一系类下单操作，具体参考mall项目
        LOGGER.info("process generateOrder");
        //下单完成后开启一个延迟消息，用于当用户没有付款时取消订单（orderId应该在下单后生成）
        sendDelayMessageCancelOrder(11L);
        return CommonResult.success(null, "下单成功");
    }

    @Override
    public void cancelOrder(Long orderId) {
        //todo 执行一系类取消订单操作，具体参考mall项目
        LOGGER.info("process cancelOrder orderId:{}",orderId);
    }

    private void sendDelayMessageCancelOrder(Long orderId) {
        //获取订单超时时间，假设为60分钟
        long delayTimes = 30 * 1000;
        //发送延迟消息
        cancelOrderSender.sendMessage(orderId, delayTimes);
    }

}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E6%B7%BB%E5%8A%A0omsportalordercontroller%E5%AE%9A%E4%B9%89%E6%8E%A5%E5%8F%A3)添加OmsPortalOrderController定义接口

```
package com.macro.mall.tiny.controller;

import com.macro.mall.tiny.dto.OrderParam;
import com.macro.mall.tiny.service.OmsPortalOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 订单管理Controller
 * Created by macro on 2018/8/30.
 */
@Controller
@Api(tags = "OmsPortalOrderController", description = "订单管理")
@RequestMapping("/order")
public class OmsPortalOrderController {
    @Autowired
    private OmsPortalOrderService portalOrderService;

    @ApiOperation("根据购物车信息生成订单")
    @RequestMapping(value = "/generateOrder", method = RequestMethod.POST)
    @ResponseBody
    public Object generateOrder(@RequestBody OrderParam orderParam) {
        return portalOrderService.generateOrder(orderParam);
    }
}

```

## [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E8%BF%9B%E8%A1%8C%E6%8E%A5%E5%8F%A3%E6%B5%8B%E8%AF%95)进行接口测试

### [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E8%B0%83%E7%94%A8%E4%B8%8B%E5%8D%95%E6%8E%A5%E5%8F%A3)调用下单接口

注意：已经将延迟消息时间设置为30秒

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:07:44_arch_screen_49-75e568d7.png)

![](undefined)

![](undefined)

## [#](https://www.macrozheng.com/mall/architect/mall_arch_09.html#%E9%A1%B9%E7%9B%AE%E6%BA%90%E7%A0%81%E5%9C%B0%E5%9D%80)项目源码地址

[https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-08](https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-08)
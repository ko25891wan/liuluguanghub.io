---
categories:
  - 技术教程
  - mall
  - 架构篇
title: mall整合Swagger-UI实现在线API文档
date: 2024-01-21 17:26:52
tags:
---
本文主要讲解 mall 是如何通过整合 Swagger-UI 来实现一份相当完善的在线 API 文档的。

## [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#%F0%9F%91%8D-%E7%9B%B8%E5%85%B3%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B)👍 相关视频教程

[mall整合Swagger实现在线API文档open in new window](https://t.zsxq.com/0el8bde0E)

## [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#%E9%A1%B9%E7%9B%AE%E4%BD%BF%E7%94%A8%E6%A1%86%E6%9E%B6%E4%BB%8B%E7%BB%8D)项目使用框架介绍

### [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#swagger-ui)Swagger-UI

> Swagger-UI是HTML, Javascript, CSS的一个集合，可以动态地根据注解生成在线API文档。

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#%E5%B8%B8%E7%94%A8%E6%B3%A8%E8%A7%A3)常用注解

- @Api：用于修饰Controller类，生成Controller相关文档信息
- @ApiOperation：用于修饰Controller类中的方法，生成接口方法相关文档信息
- @ApiParam：用于修饰接口中的参数，生成接口参数相关文档信息
- @ApiModelProperty：用于修饰实体类的属性，当实体类是请求参数或返回结果时，直接生成相关文档信息

## [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#%E6%95%B4%E5%90%88swagger-ui)整合Swagger-UI

### [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#%E6%B7%BB%E5%8A%A0%E9%A1%B9%E7%9B%AE%E4%BE%9D%E8%B5%96)添加项目依赖

> 在pom.xml中新增Swagger-UI相关依赖

```
<!--Swagger-UI API文档生产工具-->
<dependency>
  <groupId>io.springfox</groupId>
  <artifactId>springfox-swagger2</artifactId>
  <version>2.7.0</version>
</dependency>
<dependency>
  <groupId>io.springfox</groupId>
  <artifactId>springfox-swagger-ui</artifactId>
  <version>2.7.0</version>
</dependency>
```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#%E6%B7%BB%E5%8A%A0swagger-ui%E7%9A%84%E9%85%8D%E7%BD%AE)添加Swagger-UI的配置

> 添加Swagger-UI的Java配置文件

注意：Swagger对生成API文档的范围有三种不同的选择

- 生成指定包下面的类的API文档
- 生成有指定注解的类的API文档
- 生成有指定注解的方法的API文档

```
package com.macro.mall.tiny.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger2API文档的配置
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {
    @Bean
    public Docket createRestApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                //为当前包下controller生成API文档
                .apis(RequestHandlerSelectors.basePackage("com.macro.mall.tiny.controller"))
                //为有@Api注解的Controller生成API文档
//                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                //为有@ApiOperation注解的方法生成API文档
//                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("SwaggerUI演示")
                .description("mall-tiny")
                .contact("macro")
                .version("1.0")
                .build();
    }
}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#%E7%BB%99pmsbrandcontroller%E6%B7%BB%E5%8A%A0swagger%E6%B3%A8%E8%A7%A3)给PmsBrandController添加Swagger注解

> 给原有的品牌管理Controller添加上Swagger注解

```
package com.macro.mall.tiny.controller;

import com.macro.mall.tiny.common.api.CommonPage;
import com.macro.mall.tiny.common.api.CommonResult;
import com.macro.mall.tiny.mbg.model.PmsBrand;
import com.macro.mall.tiny.service.PmsBrandService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 品牌管理Controller
 * Created by macro on 2019/4/19.
 */
@Api(tags = "PmsBrandController", description = "商品品牌管理")
@Controller
@RequestMapping("/brand")
public class PmsBrandController {
    @Autowired
    private PmsBrandService brandService;

    private static final Logger LOGGER = LoggerFactory.getLogger(PmsBrandController.class);

    @ApiOperation("获取所有品牌列表")
    @RequestMapping(value = "listAll", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<List<PmsBrand>> getBrandList() {
        return CommonResult.success(brandService.listAllBrand());
    }

    @ApiOperation("添加品牌")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult createBrand(@RequestBody PmsBrand pmsBrand) {
        CommonResult commonResult;
        int count = brandService.createBrand(pmsBrand);
        if (count == 1) {
            commonResult = CommonResult.success(pmsBrand);
            LOGGER.debug("createBrand success:{}", pmsBrand);
        } else {
            commonResult = CommonResult.failed("操作失败");
            LOGGER.debug("createBrand failed:{}", pmsBrand);
        }
        return commonResult;
    }

    @ApiOperation("更新指定id品牌信息")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult updateBrand(@PathVariable("id") Long id, @RequestBody PmsBrand pmsBrandDto, BindingResult result) {
        CommonResult commonResult;
        int count = brandService.updateBrand(id, pmsBrandDto);
        if (count == 1) {
            commonResult = CommonResult.success(pmsBrandDto);
            LOGGER.debug("updateBrand success:{}", pmsBrandDto);
        } else {
            commonResult = CommonResult.failed("操作失败");
            LOGGER.debug("updateBrand failed:{}", pmsBrandDto);
        }
        return commonResult;
    }

    @ApiOperation("删除指定id的品牌")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult deleteBrand(@PathVariable("id") Long id) {
        int count = brandService.deleteBrand(id);
        if (count == 1) {
            LOGGER.debug("deleteBrand success :id={}", id);
            return CommonResult.success(null);
        } else {
            LOGGER.debug("deleteBrand failed :id={}", id);
            return CommonResult.failed("操作失败");
        }
    }

    @ApiOperation("分页查询品牌列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<CommonPage<PmsBrand>> listBrand(@RequestParam(value = "pageNum", defaultValue = "1")
                                                        @ApiParam("页码") Integer pageNum,
                                                        @RequestParam(value = "pageSize", defaultValue = "3")
                                                        @ApiParam("每页数量") Integer pageSize) {
        List<PmsBrand> brandList = brandService.listBrand(pageNum, pageSize);
        return CommonResult.success(CommonPage.restPage(brandList));
    }

    @ApiOperation("获取指定id的品牌详情")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<PmsBrand> brand(@PathVariable("id") Long id) {
        return CommonResult.success(brandService.getBrand(id));
    }
}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#%E4%BF%AE%E6%94%B9mybatis-generator%E6%B3%A8%E9%87%8A%E7%9A%84%E7%94%9F%E6%88%90%E8%A7%84%E5%88%99)修改MyBatis Generator注释的生成规则

> CommentGenerator为MyBatis Generator的自定义注释生成器，修改addFieldComment方法使其生成Swagger的@ApiModelProperty注解来取代原来的方法注释，添加addJavaFileComment方法，使其能在import中导入@ApiModelProperty，否则需要手动导入该类，在需要生成大量实体类时，是一件非常麻烦的事。

```
package com.macro.mall.tiny.mbg;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.CompilationUnit;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.internal.DefaultCommentGenerator;
import org.mybatis.generator.internal.util.StringUtility;

import java.util.Properties;

/**
 * 自定义注释生成器
 * Created by macro on 2018/4/26.
 */
public class CommentGenerator extends DefaultCommentGenerator {
    private boolean addRemarkComments = false;
    private static final String EXAMPLE_SUFFIX="Example";
    private static final String API_MODEL_PROPERTY_FULL_CLASS_NAME="io.swagger.annotations.ApiModelProperty";

    /**
     * 设置用户配置的参数
     */
    @Override
    public void addConfigurationProperties(Properties properties) {
        super.addConfigurationProperties(properties);
        this.addRemarkComments = StringUtility.isTrue(properties.getProperty("addRemarkComments"));
    }

    /**
     * 给字段添加注释
     */
    @Override
    public void addFieldComment(Field field, IntrospectedTable introspectedTable,
                                IntrospectedColumn introspectedColumn) {
        String remarks = introspectedColumn.getRemarks();
        //根据参数和备注信息判断是否添加备注信息
        if(addRemarkComments&&StringUtility.stringHasValue(remarks)){
//            addFieldJavaDoc(field, remarks);
            //数据库中特殊字符需要转义
            if(remarks.contains("\"")){
                remarks = remarks.replace("\"","'");
            }
            //给model的字段添加swagger注解
            field.addJavaDocLine("@ApiModelProperty(value = \""+remarks+"\")");
        }
    }

    /**
     * 给model的字段添加注释
     */
    private void addFieldJavaDoc(Field field, String remarks) {
        //文档注释开始
        field.addJavaDocLine("/**");
        //获取数据库字段的备注信息
        String[] remarkLines = remarks.split(System.getProperty("line.separator"));
        for(String remarkLine:remarkLines){
            field.addJavaDocLine(" * "+remarkLine);
        }
        addJavadocTag(field, false);
        field.addJavaDocLine(" */");
    }

    @Override
    public void addJavaFileComment(CompilationUnit compilationUnit) {
        super.addJavaFileComment(compilationUnit);
        //只在model中添加swagger注解类的导入
        if(!compilationUnit.isJavaInterface()&&!compilationUnit.getType().getFullyQualifiedName().contains(EXAMPLE_SUFFIX)){
            compilationUnit.addImportedType(new FullyQualifiedJavaType(API_MODEL_PROPERTY_FULL_CLASS_NAME));
        }
    }
}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#%E8%BF%90%E8%A1%8C%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90%E5%99%A8%E9%87%8D%E6%96%B0%E7%94%9F%E6%88%90mbg%E5%8C%85%E4%B8%AD%E7%9A%84%E4%BB%A3%E7%A0%81)运行代码生成器重新生成mbg包中的代码

> 运行com.macro.mall.tiny.mbg.Generator的main方法，重新生成mbg中的代码，可以看到PmsBrand类中已经自动根据数据库注释添加了@ApiModelProperty注解

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:12_arch_screen_03-d7630b01.png)

### [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#%E8%BF%90%E8%A1%8C%E9%A1%B9%E7%9B%AE-%E6%9F%A5%E7%9C%8B%E7%BB%93%E6%9E%9C)运行项目，查看结果

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#%E8%AE%BF%E9%97%AEswagger-ui%E6%8E%A5%E5%8F%A3%E6%96%87%E6%A1%A3%E5%9C%B0%E5%9D%80)访问Swagger-UI接口文档地址

接口地址：http://localhost:8080/swagger-ui.html

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:12_arch_screen_04-4ae6d6d5.png)

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#%E5%AF%B9%E8%AF%B7%E6%B1%82%E5%8F%82%E6%95%B0%E5%B7%B2%E7%BB%8F%E6%B7%BB%E5%8A%A0%E8%AF%B4%E6%98%8E)对请求参数已经添加说明

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:12_arch_screen_05-998e8ad6.png)

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#%E5%AF%B9%E8%BF%94%E5%9B%9E%E7%BB%93%E6%9E%9C%E5%B7%B2%E7%BB%8F%E6%B7%BB%E5%8A%A0%E8%AF%B4%E6%98%8E)对返回结果已经添加说明

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:12_arch_screen_06-dc73ab5c.png)

### [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#%E7%9B%B4%E6%8E%A5%E5%9C%A8%E5%9C%A8%E7%BA%BF%E6%96%87%E6%A1%A3%E4%B8%8A%E9%9D%A2%E8%BF%9B%E8%A1%8C%E6%8E%A5%E5%8F%A3%E6%B5%8B%E8%AF%95)直接在在线文档上面进行接口测试

![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA+MAAAA+CAYAAABAzMq8AAAPuklEQVR4nO3df0hdZ57H8c8MMxMwhd2kqVabtGnYrJlpxBrdrYt/TBIwMWmKHQIhM4WpFlyEhM4fRc1MWYIMbYwSSocUwgqNG0i3BMpEJrZWITGwMpbGccV0p5Il61Sj1Zg6O5PIdveP2ec595x7z7m/vNeYo6bvFxS9554fzznmj37O9/nxrb8YAgAAAAAAofn2cjcAAAAAAIBvGsI4AAAAAAAhI4wDAAAAABAywjgAAAAAACEjjAMAAAAAEDLCOAAAAAAAISOMAwAAAAAQMsI4AAAAAAAhI4wDAAAAABCy7yTbePmz6bDbAQAAAABA6HY/k7cs100axq0bE3/UO12/19zd/w2zPUtm3SPf05Hnv68Df79puZsCAAAAAFiBRif/vGzXTtlNfTUHccu23d4DAAAAAAArTcowvpqDuOdhuAcAAAAAwMOHCdwAAAAAAAgZYRwAAAAAgJARxgEAAAAACBlhHAAAAACAkBHGF2mwba02rm/V4HI3JJXJC6pbv1Z1Hyx2zfhpXaw191h7QbcX2HPFPwsAAAAAq9qd3l51jCRuHzl3Ken21SDlOuNIdPuDn6ikTjp9/T2t/tXLP9Gv1u9W688va6LhOWdL5P461dhzT6+WLfK011q1cU/z/Z0DAAAAWCozQzp2VmpoKtGjzoZZfXxySKqt1N7c2G421NUNJzm+uFADP92a5gI31PHajEpPVago8Lv/+1Gd0Ro1NwWv6bFB8/mpzQtcJ1v2Pgc0tf+Aaop8m0f6VT6Um+JaXlvT8D8Pc65jXxaqpXg87hmnvqeRc72arPI9B9ueD3PU5Ts2bRvt3/PkLfX5tw1f8rV5ndoDz3/lIoxnbFr9lzolE15fLNDqrwJfu6pWVev0S5Egbivpv3BeNNxz7k/K04tnze/ZnresURM9MoG8VRVfNap0aVsNAAAALChZsO577VZww8lLOu797gTMA2o3xw2WuOHVDYRd+WMqNwE1KHWwluZU99qluH0PaCDpvhGPVm5WvblGx8jWYHCOioRk1RxI8X2S8Dszrit6Qg2B/U1A/3BezbVpQn/uE8Fg7Oc8k9SHei8Ajs94n0ejz25nVblaKjeoqGqDzp/sV4EXmIsq1PVlr9p6Z53vY+E8sY2ff9ivj2sr1HKqJNYk/99slSGMZ2ryqrpMFm888txyt2RJDF5plqrPqqLA3TA5po/0rOoL0h6WmbIfqlFva3xSKl2K8wEAAABZKDLBesD7kGFlPBUnKHf7q932+FEV5MaFfhPAd1Y9oW2pKrPRaq9SV587LsVtTxf6U3OCeffXzu/eS4h6G+TzTECf+Vp9/hcRS15Jds/nf+5OBd39OrdELTXmswnfDRqKtlPmL1be7Z1jTs877bbnKtSked7aYbfP68rwDel3o77Ar2BlfMGeDCvHAwvjbW8c0uH82Of//LcL2vVu5Pd/bPyR/mnbd317/5/6359Q3uGn9TdJzvWnz4f1TGv826hwDZ6v1UcmvL6Z0PXa7e7tfTT7DJ09pMe849rWqvrEcXX2mK/2mABs/tl3uhVjr1u4Z1/7TbUfzHM/2THbW3RU5nxHxlTiHBuRrAt45DreJ3ON65tT38zkBZ05Yc8Ta2ci3/V99+OMRd9unoWvzfVJj+/UFyaMizAOAACA0N1QxzmpxoYyG/6a/N9t0F4TcJ1wfFIJQfSMPxAX2xS8Tvm5vuTnVJxzTJA0Qd0J/XHd1LtnNWl2L4oL0He+nNfO/HWyQTNt9TnKfWng2KqaU9kFTK8S7VzbhPPfmJ8j3be0zYTylugNR9oeMHPLDcIpFC/0ZiDYM8B7GbCzyrdLUYXbhkoNVMY2O+18vDKuyn1Dg6aJNlpu21+p0qF+qSn2soXKeECh3j9drIr//i9teuXT6Na2N36k9/f8Wod73A1T5vvXPw0e2uN+fqVK4zv+R7882qd/XvoGLsIn6j9hg+cP48Jrs6rX23B9TxP2oxtUS9qeio7Dju73jgm1Zr/o8dda9Qu9pYmv3ot+3rhni371VFzQ7jTn+8Fls98956MTuvf8RE9ef8/tTu6G5s5YyPdeENjAvC/J3dz+7UXz3XHVZzum2x0PbgP4hPvSwHmhcGKB4wAAAIBQmfBaYquv69Ty+KjKO/yhOxaeB5rs+OOhwFjn+hp/N3W7ZYMK8odiAXt6Xn35uWpJcd3S4lHVBSrPHlvl3eCcr6Yp8chE7kuDaJvjuqnb9nXMRc/dXJXkFH4mxNaZrDawUGjNspt6X3esot13ziYfX2W8+xG12BciXmU82uYsKv4zd/V5bo7JOfOaMh+LSqTywDAAxY0Z9/0NV7glD+Ntb7hBPC5oN7z+66W+VGhuf/B2ZHz1P+TFfWO2XfeNiy44pPaeMRNY39bFl7yw7O73RlwVuqxR7f4wXPayTlc36+iVT0wY9wd5E7J9wb70pbPad6JWXb+d1os2EF/7FxPEbZXbPz77Ob3ac1ytvmp6zCf6V1uN//nPshzPbUL/O5Gu7W8ejD2Hxw6+pdOXOp02AAAAACtGXo7UYYKi7bbcdFfHTIDr83/vD3S9m6JV5MTKuPnf/Hzp/PCs9pp9RobmVF9SkXC5yd5eDdqqrr+LfBL+LuSZiFS44zY6oXbeGYvuBFpnUjMTcovTnMi0eSAv8eVD0Nb0LwqKKhLCvL8C73Xhd9iXFnok8fhT/op/sgnj4rqcl0RefjSYcw9Gz5GmjavIEofxv1NZvu1y/unCu64awYnbgp7VpvhtBZu1T81xXbST7Bfg7+r+B902YToa3Ks368nA+Z9y3uh5s/c7Y79NYK+Ir3I77UgifuI2/3nMPaYM6O6Y+cTeAXna9APZXuk+ps3VUtcfpqWy+BcYAAAAwIPkTiKmJ9R1qlDbTt7VHdtV3Zv0ywmus9qVojKbWBmXHi02YfPsuO5UzmlweJ1Kfxrbf+RcJEzulAmlRfETmPl4FefKYNfs2HnSdbeeC+47NOeE4Gj7zf01VM2qbyq2j79iHbmvyH4t+819vdafOE48UGmP8F4EJN5TunHm/m7qc74J3JLtG+l+X+N+St5NfavzAuBO75j7Oc0z1uqpilsPeAI3t8t6TuSTf9y48p/W+LtPux/+pPdf6XbGXaw4XuU5y4nbRhYIosHx4raL+U2ddsZoZ+ajm2OyQdgRH9hTilW3K3wvB5yu7/9hx4YvfI9FT2USru1M7Dclcz918o+DBwAAAB402737gPYqEu7O2PHG5r9ocJ2eN6FUuuJWujOSu0m7NKRrvfM6U5zrVr69pcMKVT88o1LnXLNK3gX7hjpO3g2e01sWLNM2RM1q0oTubSXpj0s2ZtxhK8s1Jnifu+FMJqfcnFgN0Tf5mT2mLXo2/z0Fx5lHQ7/7ssF59gmT5i215N3cA/e5CjzgMD6qw0ftm5BIKA9EsmRjxlcgb9bxH2c5vjptaHWWEetMmLBtfLGN7BzTF+ZH6snYvOu6M8LHTdxW2nBPnSaQJ451T7TQS4aIyDj2rgMEcQAAACwPZ6Zzk0O6qsbUFg3ekaW9dtVu1pRT6d6QJCz6K6+2sjsWCX61G3Ts5LzaoxOpRUK/E04Dx3+t48nGjJuw+oK/fUNzCYH6TPxs6mnGb3/+pQn+RbHjJ6cy7/oe625+I/NjknAmpbOh311n3HKe+1Sk3TLh+JhKUrxwSFXhzmT8d4pnLLcHwCqxxGH8U12belr7ny2UepZ39vMl4c46ntg12/PvCct3ZTQ5mrOMmAnFgcBuArUtlFdn18Qnt1SnaUdwAjdnRvgUbSvdZf4p77mqQRPGk3ZVd7vHt/or8o7I5HZBkXspOkIQBwAAQPhsIDyfX64BEwJj3ZvtF6O6ssOEw1yzfYdvbWv5likbHoqsCz7tLUVmw7etBt8y4XiN6pyKcrqZzTOojM8M6by5Vp/ZXlMUO1dmXaw3qGzHGh3vHtWIXbfbaXx/pO3pxownYydHMz9eWHDH5JwXANEVtOb1GxOQzzjh2j8je6y7fOL9Bbu7J++mngyV8aQars5o/+FifdaoZV+O7H55wbozZXW3U0dfv6AKb+mva61O1/N97W+lnxzNWYfbBNvoZG22kuxbHi0Ljx38mRrrduvo9lZt8mZTdyvvQd6M8C9nOXGb5zn9uL1areZadVtiFe/BtsW1GwAAAHhQ7Drj3mzn0cBox0R/mKOupkj4frSyRLtODqjj8UhALIqfeG3a/emOpa53lwSzob38nNIE8oUq47P6+KwJqlWFav7dqMpNSHeqyFmw487bpy7Fxmabc7dXzatuoTHj8ZxJ1nJiw4WHR6NjvK3YOO/4ezIh2ukuv0a7quzztH18c/RCU0V0/LcV6eqerDJuzzfgPNO9Gd91/PFUxhP19OmZnki39PF3fa9m5mf0y3d9+wXGjMeNJ18RvInb0s06boK6XQN8/drolmDX81S82c53a6NbVW7suanTynzMeOBcX12W1u9W9Xp39nS71vn1s9L22thu3sRtCTPCZ+6xg+9pSHas+xZtrPPafU+d8q9xDgAAACw3fxfodaq31dkpO6u6P0DbbublZj8TrocKk4drG06H18RmLVcktLe7gbxdo25FutAX5NNXxkfODejKDnc8d+VWldnZ1b1QHbdEl8Mdxx2/znjCywMp8DnVmPFoDwBXfU1FrBt8NmPG7Xrr+ZvV4szmnvjoUpvT1Ey6CeAy8XBUxr/1FyN+4+XPpvXyqavL0Z4lN3DqwOIOdNfUtmHz1SzHi6887lrkspO0HUre5d65X/nWKr8fdnb4t31roQMAAADA4sW6sLtLo9VW+pZ1u+UuW5fF+uWu0ck/a/czyzO89gFP4LZaxWYdz3bithUpuhZ5iiBuOUuhXUwYe76469kq/LPqJIgDAAAAWAK2a36N85vt0eBbG86/bN0qQxhPyi7NdU8vLnczlkpZoya+aky/T8Ehvdl+USXb1+qL++kN4OtRcP8VdgAAAAB4OBHGEWXHhE8cvM+TZBL8AQAAAOAb7tvL3QAAAAAAAL5pCOMAAAAAAIQsZRhf98j3wmzHA/Ew3AMAAAAA4OGTMowfef77qzrM2rbbewAAAAAAYKVJuc44AAAAAAAPs7/6rlT6tytsnfHtm/46zHYAAAAAABCq6+N/XLZrM4EbAAAAAAAhI4wDAAAAABAywjgAAAAAACEjjAMAAAAAEDLCOAAAAAAAISOMAwAAAAAQMsI4AAAAAAAhI4wDAAAAABAywjgAAAAAACEjjAMAAAAAEDLCOAAAAAAAISOMAwAAAAAQMsI4AAAAAAAhI4wDAAAAABAywjgAAAAAACEjjAMAAAAAEDLCOAAAAAAAISOMAwAAAAAQMsI4AAAAAAAhI4wDAAAAABAywjgAAAAAACH7fyzoNBDxZQfIAAAAAElFTkSuQmCC)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:05:12_arch_screen_08-ec7a037e.png)

## [#](https://www.macrozheng.com/mall/architect/mall_arch_02.html#%E9%A1%B9%E7%9B%AE%E6%BA%90%E7%A0%81%E5%9C%B0%E5%9D%80)项目源码地址

[https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-02](https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-02)
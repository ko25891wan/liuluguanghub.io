---
categories:
  - 技术教程
  - mall
  - 架构篇
title: mall整合Elasticsearch实现商品搜索
date: 2024-01-21 17:27:18
tags:
---
> 本文主要讲解 mall 整合 Elasticsearch 的过程，以实现商品信息在 Elasticsearch 中的导入、查询、修改、删除为例。

## [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%F0%9F%91%8D-%E7%9B%B8%E5%85%B3%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B)👍 相关视频教程

[mall整合Elasticsearch实现商品搜索open in new window](https://t.zsxq.com/0einxu90G)

## [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E9%A1%B9%E7%9B%AE%E4%BD%BF%E7%94%A8%E6%A1%86%E6%9E%B6%E4%BB%8B%E7%BB%8D)项目使用框架介绍

### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#elasticsearch)Elasticsearch

> Elasticsearch 是一个分布式、可扩展、实时的搜索与数据分析引擎。 它能从项目一开始就赋予你的数据以搜索、分析和探索的能力，可用于实现全文搜索和实时数据统计。

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#elasticsearch%E7%9A%84%E5%AE%89%E8%A3%85%E5%92%8C%E4%BD%BF%E7%94%A8)Elasticsearch的安装和使用

1. 下载Elasticsearch6.2.2的zip包，并解压到指定目录，下载地址：[https://www.elastic.co/cn/downloads/past-releases/elasticsearch-6-2-2open in new window](https://www.elastic.co/cn/downloads/past-releases/elasticsearch-6-2-2)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:06:12_arch_screen_25-48daf958.png)

2. 安装中文分词插件，在elasticsearch-6.2.2\bin目录下执行以下命令：elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v6.2.2/elasticsearch-analysis-ik-6.2.2.zip

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:06:12_arch_screen_26-3dcb5bb5.png)

3. 运行bin目录下的elasticsearch.bat启动Elasticsearch

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:06:12_arch_screen_27-ba3cb8e0.png)

4. 下载Kibana,作为访问Elasticsearch的客户端，请下载6.2.2版本的zip包，并解压到指定目录，下载地址：[https://artifacts.elastic.co/downloads/kibana/kibana-6.2.2-windows-x86_64.zipopen in new window](https://artifacts.elastic.co/downloads/kibana/kibana-6.2.2-windows-x86_64.zip)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:06:12_arch_screen_28-021b8895.png)

5. 运行bin目录下的kibana.bat，启动Kibana的用户界面

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:06:12_arch_screen_29-0d7fe0a1.png)

6. 访问[http://localhost:5601open in new window](http://localhost:5601/) 即可打开Kibana的用户界面

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:06:12_arch_screen_30-362cad7b.png)

### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#spring-data-elasticsearch)Spring Data Elasticsearch

> Spring Data Elasticsearch是Spring提供的一种以Spring Data风格来操作数据存储的方式，它可以避免编写大量的样板代码。

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E5%B8%B8%E7%94%A8%E6%B3%A8%E8%A7%A3)常用注解

##### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#document)@Document

```
//标示映射到Elasticsearch文档上的领域对象
public @interface Document {
  //索引库名次，mysql中数据库的概念
	String indexName();
  //文档类型，mysql中表的概念
	String type() default "";
  //默认分片数
	short shards() default 5;
  //默认副本数量
	short replicas() default 1;

}
```

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#id)@Id

```
//表示是文档的id，文档可以认为是mysql中表行的概念
public @interface Id {
}
```

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#field)@Field

```
public @interface Field {
  //文档中字段的类型
	FieldType type() default FieldType.Auto;
  //是否建立倒排索引
	boolean index() default true;
  //是否进行存储
	boolean store() default false;
  //分词器名次
	String analyzer() default "";
}
```

```
//为文档自动指定元数据类型
public enum FieldType {
	Text,//会进行分词并建了索引的字符类型
	Integer,
	Long,
	Date,
	Float,
	Double,
	Boolean,
	Object,
	Auto,//自动判断字段类型
	Nested,//嵌套对象类型
	Ip,
	Attachment,
	Keyword//不会进行分词建立索引的类型
}
```

#### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#sping-data%E6%96%B9%E5%BC%8F%E7%9A%84%E6%95%B0%E6%8D%AE%E6%93%8D%E4%BD%9C)Sping Data方式的数据操作

##### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E7%BB%A7%E6%89%BFelasticsearchrepository%E6%8E%A5%E5%8F%A3%E5%8F%AF%E4%BB%A5%E8%8E%B7%E5%BE%97%E5%B8%B8%E7%94%A8%E7%9A%84%E6%95%B0%E6%8D%AE%E6%93%8D%E4%BD%9C%E6%96%B9%E6%B3%95)继承ElasticsearchRepository接口可以获得常用的数据操作方法

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:06:12_arch_screen_31-17948c33.png)

##### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E5%8F%AF%E4%BB%A5%E4%BD%BF%E7%94%A8%E8%A1%8D%E7%94%9F%E6%9F%A5%E8%AF%A2)可以使用衍生查询

> 在接口中直接指定查询方法名称便可查询，无需进行实现，如商品表中有商品名称、标题和关键字，直接定义以下查询，就可以对这三个字段进行全文搜索。

```
    /**
     * 搜索查询
     *
     * @param name              商品名称
     * @param subTitle          商品标题
     * @param keywords          商品关键字
     * @param page              分页信息
     * @return
     */
    Page<EsProduct> findByNameOrSubTitleOrKeywords(String name, String subTitle, String keywords, Pageable page);
```

> 在idea中直接会提示对应字段

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:06:12_arch_screen_32-d55bbbce.png)

##### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E4%BD%BF%E7%94%A8-query%E6%B3%A8%E8%A7%A3%E5%8F%AF%E4%BB%A5%E7%94%A8elasticsearch%E7%9A%84dsl%E8%AF%AD%E5%8F%A5%E8%BF%9B%E8%A1%8C%E6%9F%A5%E8%AF%A2)使用@Query注解可以用Elasticsearch的DSL语句进行查询

```
@Query("{"bool" : {"must" : {"field" : {"name" : "?0"}}}}")
Page<EsProduct> findByName(String name,Pageable pageable);
```

## [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E9%A1%B9%E7%9B%AE%E4%BD%BF%E7%94%A8%E8%A1%A8%E8%AF%B4%E6%98%8E)项目使用表说明

- `pms_product`：商品信息表
- `pms_product_attribute`：商品属性参数表
- `pms_product_attribute_value`：存储产品参数值的表

## [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E6%95%B4%E5%90%88elasticsearch%E5%AE%9E%E7%8E%B0%E5%95%86%E5%93%81%E6%90%9C%E7%B4%A2)整合Elasticsearch实现商品搜索

### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E5%9C%A8pom-xml%E4%B8%AD%E6%B7%BB%E5%8A%A0%E7%9B%B8%E5%85%B3%E4%BE%9D%E8%B5%96)在pom.xml中添加相关依赖

```
<!--Elasticsearch相关依赖-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-elasticsearch<artifactId>
</dependency>
```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E4%BF%AE%E6%94%B9springboot%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6)修改SpringBoot配置文件

> 修改application.yml文件，在spring节点下添加Elasticsearch相关配置。

```
data:
  elasticsearch:
    repositories:
      enabled: true
    cluster-nodes: 127.0.0.1:9300 # es的连接地址及端口号
    cluster-name: elasticsearch # es集群的名称
```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E6%B7%BB%E5%8A%A0%E5%95%86%E5%93%81%E6%96%87%E6%A1%A3%E5%AF%B9%E8%B1%A1esproduct)添加商品文档对象EsProduct

> 不需要中文分词的字段设置成@Field(type = FieldType.Keyword)类型，需要中文分词的设置成@Field(analyzer = "ik_max_word",type = FieldType.Text)类型。

```
package com.macro.mall.tiny.nosql.elasticsearch.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 搜索中的商品信息
 * Created by macro on 2018/6/19.
 */
@Document(indexName = "pms", type = "product",shards = 1,replicas = 0)
public class EsProduct implements Serializable {
    private static final long serialVersionUID = -1L;
    @Id
    private Long id;
    @Field(type = FieldType.Keyword)
    private String productSn;
    private Long brandId;
    @Field(type = FieldType.Keyword)
    private String brandName;
    private Long productCategoryId;
    @Field(type = FieldType.Keyword)
    private String productCategoryName;
    private String pic;
    @Field(analyzer = "ik_max_word",type = FieldType.Text)
    private String name;
    @Field(analyzer = "ik_max_word",type = FieldType.Text)
    private String subTitle;
    @Field(analyzer = "ik_max_word",type = FieldType.Text)
    private String keywords;
    private BigDecimal price;
    private Integer sale;
    private Integer newStatus;
    private Integer recommandStatus;
    private Integer stock;
    private Integer promotionType;
    private Integer sort;
    @Field(type =FieldType.Nested)
    private List<EsProductAttributeValue> attrValueList;

    //省略了所有getter和setter方法
}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E6%B7%BB%E5%8A%A0esproductrepository%E6%8E%A5%E5%8F%A3%E7%94%A8%E4%BA%8E%E6%93%8D%E4%BD%9Celasticsearch)添加EsProductRepository接口用于操作Elasticsearch

> 继承ElasticsearchRepository接口，这样就拥有了一些基本的Elasticsearch数据操作方法，同时定义了一个衍生查询方法。

```
package com.macro.mall.tiny.nosql.elasticsearch.repository;

import com.macro.mall.tiny.nosql.elasticsearch.document.EsProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * 商品ES操作类
 * Created by macro on 2018/6/19.
 */
public interface EsProductRepository extends ElasticsearchRepository<EsProduct, Long> {
    /**
     * 搜索查询
     *
     * @param name              商品名称
     * @param subTitle          商品标题
     * @param keywords          商品关键字
     * @param page              分页信息
     * @return
     */
    Page<EsProduct> findByNameOrSubTitleOrKeywords(String name, String subTitle, String keywords, Pageable page);

}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E6%B7%BB%E5%8A%A0esproductservice%E6%8E%A5%E5%8F%A3)添加EsProductService接口

```
package com.macro.mall.tiny.service;

import com.macro.mall.tiny.nosql.elasticsearch.document.EsProduct;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 商品搜索管理Service
 * Created by macro on 2018/6/19.
 */
public interface EsProductService {
    /**
     * 从数据库中导入所有商品到ES
     */
    int importAll();

    /**
     * 根据id删除商品
     */
    void delete(Long id);

    /**
     * 根据id创建商品
     */
    EsProduct create(Long id);

    /**
     * 批量删除商品
     */
    void delete(List<Long> ids);

    /**
     * 根据关键字搜索名称或者副标题
     */
    Page<EsProduct> search(String keyword, Integer pageNum, Integer pageSize);

}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E6%B7%BB%E5%8A%A0esproductservice%E6%8E%A5%E5%8F%A3%E7%9A%84%E5%AE%9E%E7%8E%B0%E7%B1%BBesproductserviceimpl)添加EsProductService接口的实现类EsProductServiceImpl

```
package com.macro.mall.tiny.service.impl;

import com.macro.mall.tiny.dao.EsProductDao;
import com.macro.mall.tiny.nosql.elasticsearch.document.EsProduct;
import com.macro.mall.tiny.nosql.elasticsearch.repository.EsProductRepository;
import com.macro.mall.tiny.service.EsProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * 商品搜索管理Service实现类
 * Created by macro on 2018/6/19.
 */
@Service
public class EsProductServiceImpl implements EsProductService {
    private static final Logger LOGGER = LoggerFactory.getLogger(EsProductServiceImpl.class);
    @Autowired
    private EsProductDao productDao;
    @Autowired
    private EsProductRepository productRepository;

    @Override
    public int importAll() {
        List<EsProduct> esProductList = productDao.getAllEsProductList(null);
        Iterable<EsProduct> esProductIterable = productRepository.saveAll(esProductList);
        Iterator<EsProduct> iterator = esProductIterable.iterator();
        int result = 0;
        while (iterator.hasNext()) {
            result++;
            iterator.next();
        }
        return result;
    }

    @Override
    public void delete(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public EsProduct create(Long id) {
        EsProduct result = null;
        List<EsProduct> esProductList = productDao.getAllEsProductList(id);
        if (esProductList.size() > 0) {
            EsProduct esProduct = esProductList.get(0);
            result = productRepository.save(esProduct);
        }
        return result;
    }

    @Override
    public void delete(List<Long> ids) {
        if (!CollectionUtils.isEmpty(ids)) {
            List<EsProduct> esProductList = new ArrayList<>();
            for (Long id : ids) {
                EsProduct esProduct = new EsProduct();
                esProduct.setId(id);
                esProductList.add(esProduct);
            }
            productRepository.deleteAll(esProductList);
        }
    }

    @Override
    public Page<EsProduct> search(String keyword, Integer pageNum, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNum, pageSize);
        return productRepository.findByNameOrSubTitleOrKeywords(keyword, keyword, keyword, pageable);
    }

}

```

### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E6%B7%BB%E5%8A%A0esproductcontroller%E5%AE%9A%E4%B9%89%E6%8E%A5%E5%8F%A3)添加EsProductController定义接口

```
package com.macro.mall.tiny.controller;

import com.macro.mall.tiny.common.api.CommonPage;
import com.macro.mall.tiny.common.api.CommonResult;
import com.macro.mall.tiny.nosql.elasticsearch.document.EsProduct;
import com.macro.mall.tiny.service.EsProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 搜索商品管理Controller
 * Created by macro on 2018/6/19.
 */
@Controller
@Api(tags = "EsProductController", description = "搜索商品管理")
@RequestMapping("/esProduct")
public class EsProductController {
    @Autowired
    private EsProductService esProductService;

    @ApiOperation(value = "导入所有数据库中商品到ES")
    @RequestMapping(value = "/importAll", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<Integer> importAllList() {
        int count = esProductService.importAll();
        return CommonResult.success(count);
    }

    @ApiOperation(value = "根据id删除商品")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<Object> delete(@PathVariable Long id) {
        esProductService.delete(id);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "根据id批量删除商品")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<Object> delete(@RequestParam("ids") List<Long> ids) {
        esProductService.delete(ids);
        return CommonResult.success(null);
    }

    @ApiOperation(value = "根据id创建商品")
    @RequestMapping(value = "/create/{id}", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<EsProduct> create(@PathVariable Long id) {
        EsProduct esProduct = esProductService.create(id);
        if (esProduct != null) {
            return CommonResult.success(esProduct);
        } else {
            return CommonResult.failed();
        }
    }

    @ApiOperation(value = "简单搜索")
    @RequestMapping(value = "/search/simple", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<CommonPage<EsProduct>> search(@RequestParam(required = false) String keyword,
                                                      @RequestParam(required = false, defaultValue = "0") Integer pageNum,
                                                      @RequestParam(required = false, defaultValue = "5") Integer pageSize) {
        Page<EsProduct> esProductPage = esProductService.search(keyword, pageNum, pageSize);
        return CommonResult.success(CommonPage.restPage(esProductPage));
    }

}

```

## [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E8%BF%9B%E8%A1%8C%E6%8E%A5%E5%8F%A3%E6%B5%8B%E8%AF%95)进行接口测试

### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E5%B0%86%E6%95%B0%E6%8D%AE%E5%BA%93%E4%B8%AD%E6%95%B0%E6%8D%AE%E5%AF%BC%E5%85%A5%E5%88%B0elasticsearch)将数据库中数据导入到Elasticsearch

![](undefined)

### [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E8%BF%9B%E8%A1%8C%E5%95%86%E5%93%81%E6%90%9C%E7%B4%A2)进行商品搜索

![](undefined)

## [#](https://www.macrozheng.com/mall/architect/mall_arch_07.html#%E9%A1%B9%E7%9B%AE%E6%BA%90%E7%A0%81%E5%9C%B0%E5%9D%80)项目源码地址

[https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-06](https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-06)
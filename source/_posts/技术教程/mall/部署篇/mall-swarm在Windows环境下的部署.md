---
categories:
  - 技术教程
  - mall
  - 部署篇
title: mall-swarm在Windows环境下的部署
date: 2024-01-21 17:30:27
tags:
---
> 本文主要以图文的形式讲解 mall-swarm 项目在 Windows 下的开发环境搭建。

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_windows.html#%F0%9F%91%8D-%E7%9B%B8%E5%85%B3%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B)👍 相关视频教程

[mall-swarm项目后端开发环境搭建open in new window](https://t.zsxq.com/14UohaEtf)

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_windows.html#%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA)开发环境搭建

> `mall-swarm`中使用到的环境和`mall`项目中大致相同，具体可以查看[mall在Windows环境下的部署](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html)。

### [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_windows.html#%E7%AE%80%E6%98%93%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA%E6%B5%81%E7%A8%8B)简易环境搭建流程

- 安装IDEA并导入项目源码；
    
- 安装MySQL，创建一个`mall`数据库，并导入`/document/sql/mall.sql`文件；
    
- 安装Redis、Elasticsearch、MongoDB、RabbitMQ等环境。
    

### [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_windows.html#nacos%E6%B3%A8%E5%86%8C%E4%B8%AD%E5%BF%83%E6%90%AD%E5%BB%BA)Nacos注册中心搭建

- 由于使用了Nacos注册中心，我们需要先搭建Nacos注册中心，下载地址：https://github.com/alibaba/nacos/releases

![](https://www.macrozheng.com/assets/swarm_deploy_windows_new_01-aec615b5.png)

- 下载完成后解压到指定文件夹，在bin目录下使用`startup.cmd -m standalone`启动Nacos服务，默认账号密码为`nacos:nacos`,访问地址：http://localhost:8848/nacos/

![](https://www.macrozheng.com/assets/swarm_deploy_windows_new_02-4894bbd0.png)

- 将项目`config`目录下的配置文件添加到`Nacos`中，只要添加包含`dev`的配置即可，配置文件的文件名称需要和Nacos中的`Data Id`一一对应；

![](https://www.macrozheng.com/assets/swarm_deploy_windows_new_03-771dcba3.png)

- 索引配置添加完成后Nacos中配置列表显示如下。

![](https://www.macrozheng.com/assets/swarm_deploy_windows_new_04-0ce06ac8.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_windows.html#%E9%A1%B9%E7%9B%AE%E9%83%A8%E7%BD%B2)项目部署

> `mall-swarm`项目启动有先后顺序，大家可以按照以下顺序启动。

- 启动网关服务`mall-gateway`，直接运行`MallGatewayApplication`的main函数即可；
    
- 启动认证中心`mall-auth`，直接运行`MallAuthApplication`的main函数即可；
    
- 启动后台管理服务`mall-admin`，直接运行`MallAdminApplication`的main函数即可；
    
- 启动前台服务`mall-portal`，直接运行`MallPortalApplication`的main函数即可；
    
- 启动搜索服务`mall-search`，直接运行`MallSearchApplication`的main函数即可；
    
- 启动监控中心`mall-monitor`，直接运行`MallMonitorApplication`的main函数即可；
    
- 运行完成后可以通过监控中心查看监控信息，账号密码为`macro:123456`：http://localhost:8101
    
- 运行完成后可以直接通过如下地址访问API文档：http://localhost:8201/doc.html
    

![](https://www.macrozheng.com/assets/swarm_deploy_windows_04-3d298205.png)

- 如何访问需要登录的接口，先调用认证中心接口获取token，后台管理`client_id`和`client_secret`为`admin-app:123456`，前台系统为`portal-app:123456`；

![](https://www.macrozheng.com/assets/swarm_deploy_windows_05-2123d535.png)

- 然后将token添加到请求头中，即可访问需要权限的接口了。

![](https://www.macrozheng.com/assets/swarm_deploy_windows_06-1f8d5354.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_windows.html#%E6%95%88%E6%9E%9C%E5%B1%95%E7%A4%BA)效果展示

- 注册中心服务信息，访问地址：http://localhost:8848/nacos/

![](https://www.macrozheng.com/assets/mall_swarm_run_new_04-62d4e505.png)

- 监控中心服务概览信息，访问地址：http://localhost:8101

![](https://www.macrozheng.com/assets/mall_swarm_run_new_05-d8c0b90d.png)

![](https://www.macrozheng.com/assets/mall_swarm_run_new_06-6083362c.png)

![](https://www.macrozheng.com/assets/mall_swarm_run_new_07-b8115d1b.png)

- 日志收集系统信息，访问地址：http://localhost:5601

![](https://www.macrozheng.com/assets/mall_swarm_run_new_08-cd0506e4.png)
---
categories:
  - 技术教程
  - mall
  - 部署篇
title: mall-swarm使用Jenkins实现自动化部署
date: 2024-01-21 17:30:19
tags:
---
> 之前对 `mall-swarm` 项目做了升级，注册中心和配置中心都改为使用 Nacos，但是 Jenkins 的自动化部署文档一直都没更新。有些朋友参考原来的文档部署有点小问题，这次对 `mall-swarm` 的自动化部署文档做个升级，希望对大家有所帮助！

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_jenkins.html#jenkins%E7%9A%84%E5%9F%BA%E6%9C%AC%E4%BD%BF%E7%94%A8)Jenkins的基本使用

使用该部署方案需要对Jenkins有所了解，关于Jenkins的基本使用可以参考：[《使用Jenkins一键打包部署SpringBoot应用，就是这么6！》](https://www.macrozheng.com/mall/reference/jenkins.html)

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_jenkins.html#%E9%83%A8%E7%BD%B2%E5%87%86%E5%A4%87)部署准备

> 部署之前需要先安装`mall-swarm`需要的依赖服务，并打包好所有应用的Docker镜像。由于之前已经写过相关教程，这里只提示下关键的步骤，具体可以参考《mall-swarm在Linux环境下的部署（基于Docker容器）》，文档地址：http://www.macrozheng.com/#/deploy/mall_swarm_deploy_docker 。

- 需要安装好项目所需的依赖服务，直接使用`Docker Compose`安装即可，具体服务和版本信息如下；

|组件|版本号|
|---|---|
|Mysql|5.7|
|Redis|5.0|
|MongoDb|4.3.5|
|RabbitMq|3.7.15|
|Nginx|1.10|
|Elasticsearch|7.6.2|
|Logstash|7.6.2|
|Kibana|7.6.2|
|Nacos|1.3.0|

- 打包好所有SpringBoot应用的Docker镜像，具体应用服务信息如下；

|应用|说明|
|---|---|
|mall-monitor|监控中心|
|mall-gateway|微服务网关|
|mall-auth|认证中心|
|mall-admin|商城后台服务|
|mall-portal|商城前台服务|
|mall-search|商城搜索服务|

- 将应用所有配置添加到Nacos注册中心中去，具体配置文件如下。

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_01-e5c200bc.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_jenkins.html#%E6%89%A7%E8%A1%8C%E8%84%9A%E6%9C%AC%E5%87%86%E5%A4%87)执行脚本准备

> Jenkins自动化部署是需要依赖Linux执行脚本的，我们先把需要执行的脚本准备好。

- 脚本文件都存放在了`mall-swarm`项目的`/document/sh`目录下：

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_02-f059d340.png)

- 上传脚本前在IDEA中修改所有脚本文件的换行符格式为`LF`，否则脚本会无法执行；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_03-d212b31b.png)

- 将所有脚本文件上传到指定目录，这里我们上传到`/mydata/sh`目录下；

![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAxMAAAChCAMAAACLbhKBAAAAwFBMVEUAAADl5eVcAAAzAADl5aPF5eUAM4AAAFzlxYAzgMWj5eVco+WjXADFgDMAADOAxeXlo1zl5cXlxaOAMwAAXKMAM1yjXDOjxeVco8WAo8XFgFyAXDNcMwAzXKPFo4AzgKPFo1zF5cWjgDOjgFyjxaNcgIDFxcVcgKPFxYBcMzMAMzPFxaNcgMWAo4AzXIDFxeWAo+VcXIAAXICjxcWAxaOjXFwzM1wzMzOAxcXFo6OAXICAM1zlxcWj5cXF5aOAgFxrbVRHAAAMu0lEQVR42uydiXqaQBSFGY1VBBUENU1Ms3Tf9315/8fqLNgTmYEJwX5aev6vtTcyd8B2DuMApzcghBBCCCGEkLbkQhz1bpeITH+7mvemsyS4FcPxxP8xsDdEhNSR33CMRKN+EZ29FmJ+qqLwRrnuduGONGF3OPyZTWdx1d5CaoLsWBPTaJ4F0xci3pUmQHtNbD7RncuMmiCtNXFnsDwW6ZWKVmYiQDQcC4VqasaUObebeCtjcVdGmWqhppPna48mTNeJCU6OhbjX12N9I8JcxOrI5AuYymbpRR8ZpcEeJUE+6lMTZAeaeLQMpg97MjjpB2ciDhBhnlDjdbgS89Woj/GFdsOxHKyLl3Lbu/t9OXp1A/93J9NzupQ9TbY0EURHPbUdqA1rqbgrZKBDdSjCkFATpL0mEnxL0q+IoIk8zVR4ZwBNWBlAnuEbaCIxMTRhNpayzdatDAUEepkNx3HAeYLsQBNxMUQnxdhBBE3IH3TDCJqwMjTTxwMhubkmoARE5sSPAW6ap/c/B1sZWx3mUrGXGTVBdqIJjDT9PiJbEyE0YWVowvTUmif0NxuZ2EATcqtpCKZ3hRAXlZoI9XKCmiCHNk/ICH22mSfC9HViJT87FrFDE3rFbkioCbILTTRbTyC3lIERHoq46npvXqkJrSf5IiN9VOrAALq3NYHlRPXecmqCNNaE+7pTmGbWdSd5+da67pQU152io3XwSq8F0A7gPVsTJjcUMjLboq1hPPzwNAjO08ypCbOc+NVz7Q0RIc00Yd+f2HyLl02NNjacD0SaVdyfUMFb1SfaAbwXCc3k2ghXokuvCnX0iutL4NPK7MKtiSgxax3n3hAR4tFEA8J02Q8OEC4UyN6eAfz0Xog0Dg6LnA/3EUIIIYQQQgghhBBC/nmcPuZnxwPjGjI3o8WhXZ4lpCHwlDrf898Si+an0oSaZmbzjwE1QbrB7TXxsH/tYaWf36gJ0gXgs3Z4r+s81SBK9OvkDjVBOoKZE1zea6+nGk+z5kc9aoJ0hWL8W16JOk812Dy4GgfUBOkKxfi3PXV+T3VhGtJGVGqCdIZi/Fvea5en2uJMxMbaFlATpDNUzxN+T3UuloFuITSjg/RVELKr9YTfU52LySbkPEG6g/GSurzXPk91Li4CaoJ0D+OzdnmvPZ7q6WzzjYmaIIQQQgghhBBCCCGEEEIIIeTfY3F3L/+XZYjbjIuVEBcygK8cmK1P8J4DPDl8r1/1ifDeVO5knjlzEYGWdb7tm6iwRPpryZK9MHxw+qKhJuDqaJphO2hNmdX+YpaUfeUYGtpN5evvXLmutIfE/kR4T1fTWKsaBM5cRF5w/CE10T3CvWkCUa6UAF85CFUmxgf6qxh9vpLHOTa5cxHtXhOAmmgA6mNvym+pP+2t9bktNNGgQnebDOw0SgJjEMTPBfjwUYKKGuivsSbQu18TGJvPB+nHlThxueIRga12ZwMx/zaI0YurDnldzXE+9b9VHztK8E9kb63PbakJf4Xuthmh0P/2ZtRD+Dg/4iczW6Did+U8sZjNex5NTGdqpM3XReebdshFhDrH01mayfnF5YpHBNBOP8O8GIv4Wi92HfLamuOcLFAf2zHbY6s/t6Um/BW622acL9Xy2dSRlL/wjalUH2y88VGhD1d/5vT7pBd4NKFGml4xQBPIRVRSZYSqxpazy0Q2KMCpfC/oxa5D7qk5TszfJmquT1AAC1s9ue014a+82i4DWdAEfOVOTWAmaTFPqJFmzUW+eWIShHo0u1zxiADawR+JXhx1A+tqjv/XlklUrEZ97FhVNY0Sa6ud4d/aSBP+Ct3tMnABBt+d4Ct3fneSUbUm0Nqjic3eEmcuIlsTg9jtirdrjqOdyjUReqnRBGqOUxOgVAt4Ep58+S7PlF2dJ+TgwBobvnLnGhvzhKc/zxobmrByETk14XLFIwJoh8ijCavmODVRMa7D+fss/HqZ7UMTvtVB2wxk4VosfOX2tVj00WqeCMIE7RrOEy5XPCKAdlhPeDRhdUBNVIyvXE7FOa45ttQEVpV+TfgrdN82A7W1zRob9+yMr9zu2NyzQ8Vvd38PZH/P3hx51hOqF2uNjVxELk24XPGIANrhupNPE+ZYUHOcmnCOLz0K9O+/pQmYuXF+blChu1XGq/fFtVg82wFfuf1sByp+O/szG8W9tfMT4T3dn3UtFrmIHJpwueIRAbTD/Qn0gjrkliZQc5zrCUIIIYQQQgghhBBCCCGEkP+Kffixtyt5v3qt7qLBDd01P7b14AbddYfNvvzYqOSdi2XJDd0xPzaOmZr4V9iDHxuVvPW2kvOzY35saoJ+7BtloJJ3aGmiM37sP88s4ZjpwqYf252BSt6qncyUDeBy64wfG5owf9CFTT92dQYqeZv5ZHTdDd0ZP3ZZE3Rh049dk4FK3vnlUp9VcZbujB+7rAm6sOnHrspAJW99Uiy73Drmx4Ym6MKmH7sqA5W89Vgvu6E748cua4IubPqxHRmgep7ojB9bqUO+/NEEXdj0Y9dkIClMl/p/ToYbujt+bBWFYrQ5Zrqw6ceuy0DS2UC223JDd8aPPVyJ9EqOcBwzXdiEEEJ+s3dGu03DYBhdVRBMpYOySoNtnbjhErjiboj3fyuS/HVOm3mj2k1j+xwJbQz/wbHrdkl9+omIiIiIiIiIiIjMxccmHzvzGco1+9gjZqHOlTnlY6d5r9rHdk2UwGzyseOvm+uqfWzXhD72yfnYbNQu3ce+2EbqBBX0eX8ej3cXB/NLO/YRa2s352Pn87HjbEv3scd0Iiroc+zv3fXbbJlf2m33u2O1tdvzsZ/Lx169Kd/HHlPsqKDPjAjzS7tYDt3C0NZuz8fO5mPHrJeej41nRwV97v81VgLzS7uh2dVHbe0Gfex8PvbwDFl8Pjap2FQcKhOL/aUF80u74Zmw/0ZbuzkfO5uPHZ5mlfnYk9eJ4/mlXfzv/RG1tdv0sSf52GGalp+PzfUEFfSZO1/ML+36L5tfXQtt7eZ87Fw+dlxhV5CPPdx3uunaUUGf4/7Z1e5hyfxOjtw31NZuz8d+ko/NFXbxPna8P9E9/1NBn/tvuoKHe+aXdrzMaGtLdWw3175ZLRK8+3HP3YJXXd6JVMbPm1fvwNguNh8uRERERERERERqYEb52HtXuu587NjS5FsXM2ZO+djJla7Txz5aE5yHa2KOzCYfO+2mq9PHPsI1oY99Sj522jFauo9NBWOAST10v+8E50E7RtL07PZ87Ew+dnKlS/exqWAMMKlzvzvRjpHUx27Ox36aj40rXbiPTQVnj0mdWxO0SyNpenaDPvbTfGxc6cJ9bCoYA3Z559YE7caR1MduzsfO5GPjSlfgY0cFY/DymqBdOr4+dns+djYfe7WuIh+bite/TuhjN+pjT/OxLz9Vk48dFZz9C2uCr9yV1sdu0MfO5GMnV7r4fGwqGIPcmuA8aLcfFH3sFn3sXD52uNLF52NTwRgcPJpXi7EvnAdjNZTpY4uIiIiIiIiIiIiIiDTFufOxScVOn6ZcvY/9zPG062bC+fOx07zzqfu1+9j0Hi5dE3PicgZr4nJzndJZas3Hdk3oY5+Wj832bFK8ivexb7/G74AZz5re546nhd2yj53ysXkYkvZYvI+96POx13nPmt5njmcqdss+dsrHJhWbVODifez3y/hxzrOm99PjaWE37mOnKmadNVGBjz3MXtafo/fT42lht+1jj5cR5Fzxu1MFPnasCcYgtyamx9PCbtvHHtcEqdhcY1fgY5/yOjE9nha2PvZQRSo292KL97Ez1xOZNTE9nhZ22z42+dikYqf37ErPx87dd2KE6P30eFrYjfvY5GPvT5Gk7PJ97Ns4j6xnTe8zx9PClirxfWcR14SIa0JEREREREREZO4+drep520ya3Zv2/SxuztV3seaD+f3sVef74bP2k9J2fX72K6J2XNmH/v7MvaipnSWP9X72K4Jfez/+tixGzyleK0L97ExqaPPj3cX1DJW4xlhXOtj62N3M85zYfenDh8bkzr28u66CaSWseKMknGtj62P3e8VR8hOyXbl+9jp4cvZUzsZqyPjWh9bHzuqklgT19hfFqX72GFSx3fJPaV2MlbHxrU+tj52PPnFb1hxL3bzu3gf+9CkXqRLH2oPx2q6JvSx9bH7x2a8PgRV+NiY1PSZ2hdfJ/Sx9bGHqm04cejNhfvYHIS7XNQej9VkTehj62P387xdfEs//Lvs/OR18T72aFLHnaWr3cNyrGWsOCMe9frY/9q5dxMAQBgIoE7hNO4/l4kgt4CN8F4rir8mxUUeu545vfZPPbHm+D2P3UnqbLQGe8E7N3eVE+XXy2MDAAAAD22nMjxtWvH04AAAAABJRU5ErkJggg==)

- 将所有脚本文件都修改为可执行文件:

```
chmod +x ./mall-*
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_05-05ae8fe3.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_jenkins.html#jenkins%E4%B8%AD%E5%88%9B%E5%BB%BA%E4%BB%BB%E5%8A%A1)Jenkins中创建任务

> 接下来我们将通过在Jenkins中创建任务来实现自动化部署。由于我们的`mall-swarm`是个多模块的项目，部署上面和曾经的单模块项目还是有所区别的。

### [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_jenkins.html#mall-admin)mall-admin

> 由于各个模块执行任务的创建都大同小异，下面将详细讲解`mall-admin`模块任务的创建，其他模块将简略讲解。

- 首先我们选择`构建一个自由风格的软件项目`，然后输入任务名称为`mall-admin`，配置其Git仓库地址，这里我直接使用了Gitee上面的地址：

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_06-bb29e111.png)

- 之后我们创建一个构建，构建`mall-swarm`项目中的依赖模块，否则当构建可运行的服务模块时会因为无法找到这些模块而构建失败；

```
# 只install mall-common,mall-mbg两个模块
clean install -pl mall-common,mall-mbg -am
```

- 依赖模块构建示意图：

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_07-24d511ed.png)

- 再创建一个构建，单独构建并打包`mall-admin`模块：

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_08-362b5409.png)

- 再创建一个构建，通过SSH去执行`sh`脚本，这里执行的是`mall-admin`的运行脚本：

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_09-9c1b9f5c.png)

- 点击保存，完成mall-admin的执行任务创建。

### [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_jenkins.html#mall-gateway)mall-gateway

> `mall-gateway`和其他模块与`mall-admin`的创建任务方式基本一致，只需修改构建模块时的`pom.xml`文件位置和`执行脚本`位置即可。

- 我们可以直接从`mall-admin`模块的任务复制一个过来创建：

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_10-e94b0ef2.png)

- 修改第二个构建中的`pom.xml`文件位置，改为：`${WORKSPACE}/mall-gateway/pom.xml`

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_11-69f91cba.png)

- 修改第三个构建中的SSH执行脚本文件位置，改为：`/mydata/sh/mall-gateway.sh`

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_12-bac74573.png)

- 点击保存，完成mall-gateway的执行任务创建。

### [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_jenkins.html#%E5%85%B6%E4%BB%96%E6%A8%A1%E5%9D%97)其他模块

其他模块的执行任务创建，参考`mall-admin`和`mall-gateway`的创建即可。

### [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_jenkins.html#%E4%BB%BB%E5%8A%A1%E5%88%9B%E5%BB%BA%E5%AE%8C%E6%88%90)任务创建完成

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_21-3dcf5d60.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_jenkins.html#docker%E7%BD%91%E7%BB%9C%E9%97%AE%E9%A2%98)Docker网络问题

> 如果之前使用的是`Docker Compose`启动所有依赖服务，会默认创建一个网络，所有的依赖服务都会在此网络之中，不同网络内的服务无法互相访问。我这里创建的网络是`deploy_default`，所以需要指定`sh`脚本中服务运行的的网络，否则启动的应用服务会无法连接到依赖服务。

- 可以使用`docker inspect mysql`命令来查看mysql服务所在的网络；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_19-028c4e49.png)

- 也可以通过`docker network ls`来查看所有网络；

```
[root@local-linux ~]# docker network ls
NETWORK ID          NAME                     DRIVER              SCOPE
59b309a5c12f        bridge                   bridge              local
3a6f76a8920d        deploy_default           bridge              local
ef34fe69992b        host                     host                local
a65be030c632        none     
```

- 修改所有`sh`脚本，修改服务运行的网络，添加一行`--network deploy_default \`即可。

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_20-64968d13.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_jenkins.html#%E6%A8%A1%E5%9D%97%E5%90%AF%E5%8A%A8%E9%A1%BA%E5%BA%8F%E9%97%AE%E9%A2%98)模块启动顺序问题

> 由于作为注册中心和配置中心的Nacos已经启动了，其他模块基本没有启动顺序的限制，但是最好还是按照下面的顺序启动。

推荐启动顺序：

- mall-auth
- mall-gateway
- mall-monitor
- mall-admin
- mall-portal
- mall-search

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_jenkins.html#%E8%BF%90%E8%A1%8C%E5%AE%8C%E6%88%90%E6%95%88%E6%9E%9C%E5%B1%95%E7%A4%BA)运行完成效果展示

- 查看API文档信息，访问地址：http://192.168.3.101:8201/doc.html

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_13-fed37937.png)

- 查看注册中心注册服务信息，访问地址：http://192.168.3.101:8848/nacos/

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_14-d9888132.png)

- 监控中心应用信息，访问地址：http://192.168.3.101:8101

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_15-ec4b9178.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_16-4da79bd3.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_17-0b991594.png)

- 日志收集系统信息，访问地址：http://192.168.3.101:5601

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_jks_18-9e0da61c.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_jenkins.html#%E6%80%BB%E7%BB%93)总结

我们通过在Jenkins中创建任务，完成了`mall-swarm`项目的自动化部署工作，这样当我们每次修改完代码后，只需点击启动任务，就可以实现一键打包部署，省去了频繁打包部署的麻烦。
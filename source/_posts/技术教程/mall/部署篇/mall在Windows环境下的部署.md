---
categories:
  - 技术教程
  - mall
  - 部署篇
title: mall在Windows环境下的部署
date: 2024-01-21 17:30:43
tags:
---
> 本文主要以图文的形式讲解 mall 项目所需环境在 windows 下的安装，主要包括 IDEA、Mysql、Redis、Mongodb、RabbitMQ、Elasticsearch、Logstash、Kibana、OSS。

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#%F0%9F%91%8D-%E7%9B%B8%E5%85%B3%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B)👍 相关视频教程

[mall项目后端开发环境搭建open in new window](https://t.zsxq.com/0epBUTyox)

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#idea)IDEA

- 关于IDEA的安装与使用请参考：https://github.com/judasn/IntelliJ-IDEA-Tutorial
    
- 搜索插件仓库，安装插件`Lombok`；
    

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_01-a5fa6444.png)

- 将项目下载到本地，然后直接打开。

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_02-b59aa688.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_03-848541df.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#mysql)Mysql

- 下载并安装mysql`5.7`版本，下载地址：https://dev.mysql.com/downloads/installer/
    
- 设置数据库帐号密码：root root
    
- 下载并安装客户端连接工具Navicat，下载地址：http://www.formysql.com/xiazai.html
    
- 创建数据库`mall`，导入项目`document/sql`文件夹下的`mall.sql`文件，初始化数据。
    

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#redis)Redis

- 由于Redis官方并没有提供Windows版本，第三方提供的最新版本为`5.0`，下载地址：https://github.com/tporadowski/redis/releases/

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_04-c813849e.png)

- 下载完后解压到指定目录；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_05-524c83dd.png)

- 在当前地址栏输入cmd命令后，使用如下命令可以启动Redis服务；

```
redis-server.exe redis.windows.conf
```

- 如果你想把Redis注册为系统服务来使用的话可以试试下面的命令。

```
# 安装为服务
redis-server --service-install redis.windows.conf
# 启动服务
redis-server --service-start 
# 停止服务
redis-server --service-stop
# 卸载服务
redis-server --service-uninstall
```

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#elasticsearch)Elasticsearch

- 下载Elasticsearch`7.17.3`版本的zip包，并解压到指定目录，下载地址：https://www.elastic.co/cn/downloads/past-releases/elasticsearch-7-17-3

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_07-3f749d80.png)

- 安装中文分词器，注意下载与Elasticsearch对应的版本，下载地址：https://github.com/medcl/elasticsearch-analysis-ik/releases

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_08-a3e22f93.png)

- 下载完成后解压到Elasticsearch的`plugins`目录下；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_08_2-943c675d.png)

- 运行bin目录下的`elasticsearch.bat`启动Elasticsearch服务。

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_10-154a4b8a.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#kibana)Kibana

- 下载Kibana，作为访问Elasticsearch的客户端，请下载`7.17.3`版本的zip包，并解压到指定目录，下载地址：https://www.elastic.co/cn/downloads/past-releases/kibana-7-17-3

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_11-0a25cd76.png)

- 运行bin目录下的`kibana.bat`，启动Kibana服务；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_12-5c098822.png)

- 打开Kibana的用户界面，访问地址：http://localhost:5601

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_13-e272f8a6.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#logstash)Logstash

- 下载Logstash，用于收集日志，请下载`7.17.3`版本的zip包，并解压到指定目录，下载地址：https://www.elastic.co/cn/downloads/past-releases/logstash-7-17-3

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_14-e09f51ab.png)

- 将Logstash的配置文件`logstash.conf`拷贝到安装目录的`bin`目录下，配置文件地址：https://github.com/macrozheng/mall/blob/master/document/elk/logstash.conf

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_15-584dfa2f.png)

- 注意该版本最低要求`JDK 11`，需要自行安装，然后在环境变量中进行如下配置；

```
LS_JAVA_HOME=D:\developer\env\Java\jdk-11.0.14.1
```

- Logstash需要安装`json_lines`插件；

```
logstash-plugin install logstash-codec-json_lines
```

- 运行bin目录下的`logstash.bat`，启动Logstash服务，启动命令如下。

```
logstash -f logstash.conf
```

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#mongodb)MongoDB

- 下载MongoDB安装包，选择`Windows`社区版安装，下载地址：https://www.mongodb.com/download-center/community

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_16-b4b5dd6c.png)

- 运行MongoDB安装包并选择自定义安装，设置好安装路径；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_17-b392d371.png)

- 配置MongoDB，让MongoDB作为服务运行，并配置好数据目录和日志目录；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_18-f405b192.png)

- 取消MongoDB Compass的安装选项（不取消安装极慢），需要可自行安装；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_19-36f57f33.png)

- 双击`mongo.exe`可以运行MongoDB自带客户端，操作MongoDB；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_20-d5fa931b.png)

- 连接成功后会显示如下信息；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_25_0-72489cba.png)

- 如果需要移除MongoDB服务，只需使用管理员权限运行`cmd`工具，并输入如下命令。

```
sc.exe delete MongoDB
```

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#rabbitmq)RabbitMQ

- 下载Erlang的`OPT 25`，下载地址：https://erlang.org/download/otp_versions_tree.html

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_25-5052d54b.png)

- 安装Erlang，直接双击安装包安装即可；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_26_0-41382028.png)

- 下载RabbitMQ的`3.10.5`版本，下载地址：https://github.com/rabbitmq/rabbitmq-server/releases

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_26-90f0c03c.png)

- 下载文件为`rabbitmq-server-3.10.5.exe`，直接双击安装包安装即可；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_27-aecf7049.png)

- 安装完成后，进入RabbitMQ安装目录下的sbin目录；

![](https://qiniu.ko25891wan.top/日记软件/obsition/re_rabbitmq_start_06-b6308839.png)

- 在地址栏输入cmd并回车启动命令行，然后输入以下命令启动管理功能：

```
rabbitmq-plugins enable rabbitmq_management
```

- 访问RabbitMQ管理页面地址，查看是否安装成功，默认账号密码为`guest:guest`，访问地址：http://localhost:15672/

![](https://qiniu.ko25891wan.top/日记软件/obsition/re_rabbitmq_start_07-568a9b20.png)

- 创建一个我们需要使用的帐号`mall:mall`，并设置其角色为管理员；

![](https://qiniu.ko25891wan.top/日记软件/obsition/re_rabbitmq_start_08-53701273.png)

- 创建一个新的虚拟host为，名称为`/mall`；

![](https://qiniu.ko25891wan.top/日记软件/obsition/re_rabbitmq_start_09-3d6932ed.png)

- 点击mall用户进入用户配置页面；

![](https://qiniu.ko25891wan.top/日记软件/obsition/re_rabbitmq_start_10-9a412dbb.png)

- 给mall用户配置该虚拟host的权限，至此，RabbitMQ的配置完成。

![](https://qiniu.ko25891wan.top/日记软件/obsition/re_minio_start_01-ccbb9f35.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#minio)MinIO

- 下载MinIO在Windows下的安装包，下载地址：https://dl.min.io/server/minio/release/windows-amd64/minio.exe

![](https://qiniu.ko25891wan.top/日记软件/obsition/re_minio_start_02-acfaad82.png)

- 下载完成后创建MinIO的数据存储目录，并使用如下启动命令MinIO服务；

```
minio.exe server D:\developer\env\minio\data --console-address ":9001"
```

- 此时MinIO的API将运行在`9000`端口，MinIO Console管理页面将运行在`9001`端口；

![](https://qiniu.ko25891wan.top/日记软件/obsition/re_minio_start_03-4ebc1425.png)

- MinIO服务运行成功后就可访问MinIO Console的管理界面了，输入账号密码`minioadmin:minioadmin`即可登录，访问地址：http://localhost:9001

![](https://qiniu.ko25891wan.top/日记软件/obsition/re_minio_start_04-df874596.png)

- 看下MinIO Console的左侧菜单，不仅支持了存储桶、文件的管理，还增加了用户、权限、监控等管理功能；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_35-5d57712f.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#oss)OSS

> 注意：mall项目能同时支持OSS和MinIO两种对象存储，如果已经安装了MinIO，可以不使用OSS。

### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#%E5%BC%80%E9%80%9Aoss%E6%9C%8D%E5%8A%A1)开通OSS服务

- 登录阿里云官网；
- 将鼠标移至产品标签页，单击对象存储 OSS，打开OSS 产品详情页面；
- 在OSS产品详情页，单击立即开通。

### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#%E5%88%9B%E5%BB%BA%E5%AD%98%E5%82%A8%E7%A9%BA%E9%97%B4)创建存储空间

- 点击网页右上角控制台按钮进入控制台；

![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAxQAAABACAMAAAC0qsHfAAAAmVBMVEUbISQ3PD89QkX///8jKSzZ2tumqarz9PRucnTn5+jDxcX7ODhWWl2PkpTGxsaam5y1tba9vr5PU1WGiIleYmT6+/t1eHoeJCerrKzV1tbi4uI5PkHDw8Pf4OCVmJnR0tMxNjlCRkm5urvMzs6Eh4iho6Tu7+9+goNobG7Iycqxs7RHTE8nLC9iZmiNj5FaXmFRVljd3d3Z2tp92od5AAAJaElEQVR42uzTIQEAIAAEsad/afwpPFuH7cX51kAKKZBCCqSQAimkYFJIQUghBSGFFIQUUhBSSEFIIQUhhRSEFFIQUkhBSCEFIYUUhBSXXXNhTxOGwnBslgsZiMhF8FKmzkvd2nX7/z9uHJIYCNrIamefrd9TSxKFkPK9OTmxH1D8kT6/Uuh/0uC/Fbql8sV0upqgv6dXuvoDiv9D6Jba5Xm0+IRe0gcUPfQBhVPLfRiV6+j+cTk4L3RLTSafkundpCtk9AZQ+J5AIO7xq0IRpBT9Qxq8oGwedxvzeVYf4/ylU8ejcHArzX5EZVFrvfJng3NCp0Xx2edrTIU4e7ULZtNN/LWt+Ospr9I0aPdPPB81JKDaD4rdlpi3CLFLAE0vKPiU8c79BCxAF4kz0W0U7HJuKeMnnyFh/C2hCIeVYlmQNOyiHGpK8+xYDm8MxSxbFw9htt/vNutC8LNUuKEguCUiH9W1oPi0mHa1aAKIQVQyYKCwPQTVXlAY0CRgXSg4Y2HndLevDRT+NFBQUIKcImlw6oqkFxT6D8Z4VfB8Gb/E9SNFPM+OUIzGSRRXTcMwiUKFTAjNshxlUFY0xFFyQyiSdTEaz2TIEMXD82kq3FAYj5garBOuBcVknS/vLD2XCFkAUOO3YLvzhD2HQrUXFJwZzGszE6JGBCVZrE1FiRsKgi2RGgp4ARTKn1rua1B4AG3Ry6EQqnN1K+rCjF8TirADRTgMJQ2q7R1C8W1eHL4dK6tidC6v6A+F+d2FYjZr15ffnVBE3e2nuzYUlAWMas8wTj3uCZgQlYQsGnn+i1AEKa60YtSMSByvZaAIUjnGENjQpzscaRIL6UuADaCAN91MMK4uAB22T+GMXsiUACjgJTCtYwStjlt+7UiRW1A8jeTaKIkUC+8xUmTlcGyWUklZ8MFpdeM1bopxRAg1dUqI/gTVHzb8LIv2KnzDcicU+UMHnOdfHSjWqdDeqx3MVowHWy4AEB0oekYKisoACWAAvGtHCooJkoJp3gkFnGjoUOClQVWpoHAu7QBx6nv1+kkoqDWYF4oQ6Nn3AAoo1Wz6HkVEID71/OtCMQYGlEKVOehqlHShGErdGApa3M9MbbYxVUtnHi7F8sC4cos5vDCB+b8wi5HW91Gajr67oAjXX+y2fRcKzLj0Krv3aNUGPVNIBKqyXsxDW5+cQlQmPIB3QuLbUBA1fj01uHMKItqBFXIgcGUFRTB1eFIlNwR7B4yFosQOfz2gMGORguTmqlAk0XA0bi+f8iGoas7mVd79LiPFsMhaWXeRXw0KaJTPvGvD5QNON6bMNu6cIi7v7LagCwVVfVG22nJoI/WtEFHZTjqwLxRgRDVQgo9aSSh0izY44w4oVIRopls9d59AAqKLvcOre78cCuhSP0qBpdLgmlDE8/soa0HRRsaCYgAwQOW2UBTFvgnFvjzcDU7oMijMkgrma3hYylfGht3o4BO8jZEbiqwY222H4QkoqEx72QMxncsfaZ9+UOgRajasSGGJe6ETCs5EcyeWwuQuGpm3S/BHFvWBmAbPl7tS/aCgmOpQI9T9BOyaUIxHo3EetqAIoyRbJdU7uYLCbMm+GyhEwa1I4YLCHSn4AQ7Sk3wbnLHhPcPlEqFsi6ePyK3Jnvh2069NGwpvB1aVSybGoQ0mPrle19OhkbgACoy9460L4oTCnWibZEJdtpw2SSDEkVI0d4eogUiYtVufRFscgyB6EyiyeSxtbqDIogxYgHc6kSKbKz7yW+cUT82cIrxGTkHTAA56VqJndgt3W7xInhiGCODW5Pmnzc6XRdyCQgAUQKHsF5xC6jfgiUN7J8Nx7j4JAxKYjuoykVBYa3pXom0S8mNKzcnaO2DhgsKI2vuv6h6qsdtcuHMKuBU4/a2WT3lleOVt/eVdnVbkMs2GsqW4PiO/9e7TfGmYGJflK3afTDLh+XVNMC6kP09a8XGBWZpGX9BFuvuZ2ZgsHrtQUIJqEtkqlck1kRY0U2DfnEIx32SfNKAwAUc4oDD9Tz2iz4F1CyU9oGBcf06tCAF9NTyCaS8oYE0o4X+TSBEPY/P9HaCgiRglYPi6ep8NwtD8x0f4HqD4FpWbIxVL6vie4sJE219LKAIWeuI8FGhc4pR+R5fpy8+nib2gasWY5sxNhdzTEQAmvN4TFIgeoyb3OJxFsVZfKJTM8PpBUQ/DY+uUvgEUSZTreGGgANv/ZucMeBOFoQAsO+ntSFCRBsTiIRAykdu8/f8/d32jvcoLuV2rC2R7X7JslamN9KOv7xXjKI6SXordKo4ha9vkemvHDLJP3qkumk4IzxOiY1m7vaWiHVwF7HDaHmUKKBxIgThW/38KUn/nHQeERYnmrgAqWhAQ+FoKeZIhfleFC0TgLgW0XKVg8tNRoz+s1LNcwye4CKAp21KKt/QE+1HfPXzabiJu5JDNGCyA4Q6CqIRtxJN8jWYVHk2/96nI8pCX5eUlyrJz6Y3iWNGGQApJ4U51wLTH4TWUmd07Wgq9p0f3AvVkAinYN1W1C9Cz3MInvfvquym8WUgB5RxITNxvoY2dAJIcUk1VohcR+nesnNAbQpQ+SR5PvkuWn7OeYlNnTeeN4SqFDzXlO0nRhT8RzylKdcL4R1LAdRn17lYpfLicwlEHKXy1Ggct4M/bpYAZ8i2/Jl/Qt8o+FXJZyGTB8EOkSPKIDxqwhOZRk680scd3W+mLkegRMriQfAJfVtIYbzJEyddtXbfrS3kqMjaaknXZJasS6guUfXImXS7TK6CJt4nCzKQN6Qc1xE7yB8IKJyl0eGIiC9/vD0HbNvvk6+lBvbzaEAj9U1iGT70Rfwczey/8Md0PlM4fVLwzg12N+HwDGVcpxpC1+TceVfLwbBDioetKITxxaovdWMccZwrgbuHT8Sm5XpQvn19EOvp+/fhUUhjspNBVjnewmSkwzuET4LTQHu85minozjvMZVWcRx6ewxcXsMPraWG4HPbDbC7djmoD3Y5qAa/YyKNzkGKzf00GVY59QFLMAe/Lspichya8luDXE/udmiZJYQFJ8Vmk+BckhQ0kBUlBUiBICpKCpECQFCQFSYEgKUgKkgJBUpAUJAWCpCAp6FvHESQFSUEMICn+tE/HJgCEAADERNx/5q//ChEsTXaIFIQUUhBSSEFIIQUhhRSEFFIQUkhBSCEFIYUUhBRSEFJIQUghBTfWgfmsxYPGBH4GAGx9/hv+JNJrrDoAAAAASUVORK5CYII=)

- 选择我的云产品中的对象存储OSS；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_37-76280073.png)

- 点击左侧存储空间的加号新建存储空间；

![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANwAAAEZCAMAAADL1rsAAAAC+lBMVEX09PX////i4uIyNzrz8/T7ODgeutiDg4O4uLjJ9PX09MtqrOPJjjpqbnAyN46pbTqp3/X0yI6SlZf09OPP//8yjsv036wybaz/zqPirHC35v+Dg6OS3v+JNzrnuJODg5Pi9PWTuOewwNiJyPWDk7fn/////+e3k4Pq09pqm8rBwMPe9PX///LPpYP09OD//89qg7XIm3Cxg3CJN47esoVm0/Gjg4NqN3Cjzv9qbpuJN3BqNzrI9PWWyfUeyer04LWgn6GWbnCx3/WAsuFmutwyN3AjIyOOyu7/7OP06siAbnDI6vWGye7n8fX08u708ejtyYfCg2lyI1as4O3/3t//5rf05Kzq///t9PXW19mnqq7t4K30yZtqN45vU4pqboWKU3BgYGDOydiDpc9pg8L//usewOP159DUwrqTg4PH3fjA0+3NwLq/ubjr1af1/f///viHw+3O6+e6yd/07d2Su9fBxtK4u8+5usLrz6f9///h7ffn7fXY7fVmzuaHt+P//96Jx91mwNy6w9bt4M/23MfRxMK4ubzjzLvHubijg5MkWZBkYobJimtuRUx2OiPY9f/U6fjO5vW74+zo/+t9tej//ef+5Mzi9MtqrMvbx7r0ybW3oq5qrKznwJbLw5CTiI6jroUya3WIZ19WSFclKlSOX0KsiTyQZy3i/v+w7P/l8vyS3u6VzO7N2+rX4uj78eXi9OP05OHK0t7h29l0ptjZ0tWfwM/My8y4ucjt3LpCi7fJr7FIfK+UhKSbnqEvbpziyI7FtYtkZ4iwmYeBa4ZIY3vntHmpbXAvJ15BREpnI0UrIy5mRiY+KiXP5v/J9OOsyOGWsuC8vduFstv/8dT/7dSTpc/BrL2mp7281Lx9oryFmrfI37XJ36ypyKyJbax9eaesnqHny5znxZvespt1jJvPpZNMcpFpUoppkIijk4POk32mgXzJrHCQh3BqbXDCj22KUm3TmmdBUl8uSl+whFjBi1KRXk67hkxqWUMjI0FfUUB3SyluizVcAAAJwElEQVR42uydachMURjHzximmOsdE3emW97i9WWSb5oUhbKHscYbERPeD0oRSrasKVu2QpbsEglJ2fediA+27AnZ97X873PMPaO5M7wzlnvG84u598ydD+fX/znPub4comoRIxiGYRiGYRiGYZgipXYoKCRGSXXxAzVK8UgRaVKLrmH8ThMcOcsvbxThNrBRWNFD0XopubApvEnYTwSVHLl9B1N3Rgq4GEFhRLeFaDiptIHwMJBSV1WTFtJyD64GfPBXJmd4NThR+2QtmquRiiW6uJSuKjyyNt2LFHKWd5dd7dlYO5iik1yk30o5MpxFRTJkTwTxTMmtxK1nQShoD/Vq3K5Hco5PpBnu5QDJyuDo65Lq+GizCpJqMXp20UWaLUebn7Yx6MhllKUhZ2//wG9rWqub1KLfQtuzCy7V91FZtHOp5GRRygFsISfd6KZG01tSDk887oYpQ4B0pBylGX2GL2nvpmgnhfyK1a0iJAdXwrsdhcrQpDwoDYoSa7AUH/YCq4I9jbo+cORJbknINEyPv6Wgo1h+NXlYBe36Qy5OJu5yuPe8HPYp2rbV5GmNIb+SCxjnlvN4WUaaNaCSTE2eGiLtBDBAKDnkrAYeTw4dI3OAGzRLGNg7YFY5v+n1ssT0CFliUKG7VPuk90wphw9VgJDDH++XZWaZ0oJz5my5vV6RmdCgoTAMwzAMwzAMwzAMwzBMMeArYlhOV1hOV1hOV1hOV1hOV1hOV1hOV1hOV1hOV9zlGpXVbd86EJhQFgBxjHyx5j77ohc55OrE4j5fz+YYSbmeASLu0wZXuVgAqdlysbiS0y+4LMm1b01lGU/WjTlysSFVAkN6+3TCVQ4VKMsyXa5sky0d0Ck+d7lkINADchPKNFtkvyCHqCZvHiW1knEaY0BoFZy7XPsJ1C1xN3gUbJxNQLfdwFUuudmWGzG5ty+G9KQT1mGdopDbsfD7Pjd41GRHLoZWiRvsDdqQ6w1lyKjJzWNxWZ/2Fk5asWKQ6xnY1KhsSG9s40jM3tbrSq2kRs0zq5wPHs0psDjMbCN7Ww9otY+7ygVy4tOG//CfPEUCy+kKy+kKy+kKy+kKy+kKy+kKy+lKccsxDMMwzN+gWnaE9mRXYDlvw3K6wnK6wnK6wnK6ohRGXhlfuFyX6Q0Vo8eJykIHQDqEoxsaCDpiOwUdlagOpw6rk3Rzy/Xq3s7tydSjn04fvzFfZLB7/3hR8bR+uUin65EDLR3WdBKVIfN0dysY6Vfd8INJoaBSkmxfXBpMeTbOT67F3LcPWj4/t+4ySaTTt39H0WH96YnlP8gN6qxS7FZJOcufjkknclsmJWeY9pGJwImKIi5ILjGuS3KsHd+7dcNd5V6tmqPslFzFev/5mSMqK6fOc5QgNroaS5AbjZQNyRkkauYnl3i953sQI0MzO7rJDehxH3Yuya0YOCsfOYSkMEpW+aOHUlndhByMyF/KyV9becr1nXtqi5AMO7PIXU7cuzM+U27FrGvHIJfHijOkC51wXFI9jP6hjn6nk4LlqeLiV+VGUos7fOY6tbixzpMuH9u23ScnW3H03MNd5a5yDkoObgsq9ldaDtM2005FN5Y3JrmUWxjCKUlT/GpZnjjfVvF5kfMksXTjwSnjZHPY0HbdRPFzuQ6XyvE5ZtaCPBoKOuELM+0/zti7mOSkAa74Uv0yWHBZJt7PnJcqy7nR8UKRcJNzimDruLy6pRG0TKiourTlNuwJoRapPNUzU4hC5ZCps+YST9Kfdviws9xVjva5NcNFPnI00fS2QnI4RF2qqg5KFNwtRcXGg4jOhWFzdg7LtuYSs3+j3OpmSyg5d7lgQZv4mLnHL5bjsmZRht3jR39Dzh/dlj05tSHmIwet9Wff1Pxydt/8jOy+zvwzcnhfpPZIcpZ9xrhMLv0NBb9w5MJUmT+Ri7jIgcSMuy37DBWZ7L46VoxcJqWV3NqBAweunfQSn7i2E/8WJZf4xq4drEYNhHEAZ3rJnMVDiJASSLIXh94kR1/A4yKeF1HXXbog7q71Iui66qliaUEEey2FWrTU9ik8qY/gwZfof75kdzZlKQk0SzJ8f0qZ2WRLfvvNTCZlT+/Z+8hj9fMc45oVxjU1jGtqGNfUMK6pYVxTYzfO5u+hcFaUtWalJE40KYxjXA3DOMbVMIyrO+745PTBt00bcYM3fUkZHlqH230sZ5l2t+zCPYRtfLQpBt/fgte1C/dOymfZbHvUl+PtVeLcde+Ko3GrI0x8Z91r73uiTH73yZZmD52ta8f5DhJGJXGwOVkS4ad/wO+ULBwVy/TOt68fd+um2LhjdEtx5tQ0eEeyWD83jPBShi0843K12sOsqwJHv0rh6BAZAzHDKWoWH5uv3sunl7oV4XBxLhrZ1boOphAJ2k+osHrc0UBMeb7uoek6iRmh5ItbgSiO664AB0IiFnAKqnikcTRecRQANPL1VeRJ41MLOFE8cS+n+dOvAucgHWFwhEGA+6RJQuGoluZwOHM+akGnyiYqwLlFM/gsR5HpTuT0VxWVo1oZHDXIsw/bbNTlcb4ziya6Tvg1wNtUQB9MwUwWl5DdHqjV4GBagnNu6CufTyODU87zVobY+ODhjLvhzpf/gVD4CaOiOHjGH00ZIa0QB8mlYenpitL15nEQL+L88EUY4SMBju4RRYPVf3pEd4NjbMRGBxXhFK2JHV2TgHrZghK3oHP1a+2/pF2K++e54Q7qC1y5W/lEgjc8O+tJndf3q1lQqCQuGj/pVqDmtwJMuASlpIURHToxv0EhsFt6tZxvKbMMe1pXj41zrnKE015ASz/QASXHPw6xYdG6WuAqCHTYSFuK07rzA1tx4vbJS1uHpW3/Q2Ec4xhXnzCOcTXMGn9Vg8PhcDgcDofDuWCvjlEbBoIoDG9tmEa3EAIjBTWBCGG78w0sDDlYcpRUgVwgfcrcI29nzU4SHFJaxf8VuxpLhn3MagUAAAAAQHFammY51XLoX836bjS7f0hpe2f2uE/lotXYDnbwQlNV6vpUmo9mU1qBU+Nqurz4zc5aDVrmp4INSjkfVTzttfgp+Z0yFFEPfaenNjsFfenS7S2NW6JzXcmTRl1l89vef/XO5HnMd9NZHXVRq7H+h+Mq2ibNxY9wY80wmEzbu7IJy+qHGmG0vIW/1zmx+tuvoW//hNvsfEv+Gc5F7bkUT9friBfb8ko433pnm2JbtrENNcrv2l+4smlvLw6UK+HUDa3WJrXiUA6UNg4QDyFRb5+7PM/vawkXn4Jr71z+InwoodJdPgX16I9DI+qz5oO/qOvIBgAAAAAAAAAAAAAAAADA10bBKBgFIwMAANqGfwn0ynAcAAAAAElFTkSuQmCC)

- 新建存储空间并设置读写权限为公共读。

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_38-51f50bb5.png)

### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#%E8%B7%A8%E5%9F%9F%E8%B5%84%E6%BA%90%E5%85%B1%E4%BA%AB-cors-%E7%9A%84%E8%AE%BE%E7%BD%AE)跨域资源共享（CORS）的设置

- 选择一个存储空间，打开其基础设置；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_windows_deploy_41-65656de0.png)

- 点击跨越设置的设置按钮；

![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAtcAAAC3CAMAAADuO4IcAAAC9FBMVEX////5+fkjIyPS09T7ODgeutji4uL//+rq///OiCOqYiOGy/9jq+ojYqr/y4YjiM7/56r//86q5//Gw8bqq2P/7ONNUE3O//8eyeqGIyPN//8jI4ZjIyP/3t////GwwNiNj42S3v8ewOPq09rNydhmut+w7P9jI4YjI2Nm0/GGI2OSutpmwOMeut9mutn54qaSutgjhcnlqWHx7/OqYmPO/+weutojI0IjIypCIyPJ+fmm4fmFyfn5+cnl+fnq/+37++Wwwdv5x4RjI2MqJCRgIyP4/f//+/hfqOXc3NzFw9AjKE42Sk3GgyPj+P+c0P5hYKaGI4YjI4SmYCOEIyPi8fCGy+rO6a5jYqojJW4jI2EjNEZNTDgjLTiMSyS66P96tOnO/87/886ErcwjXaX/3aH/1pUjLmWIfmNtWVgjI1g4MCSGQSNsIyNUIyPx+//E9v/O8/+48P+j3//R5Pv///iNzviz7u2S2+jt7eaw1+GTxt7/+dNSmdGp5s2cx8mIisfo1MbOw8b/9sT77LtShLGNqq0nhaTGwaGhj4gjiIYlSYbOyIQjLYHvuXjApWwjWGzMjGOEI2FSL08qO01NRjGcUyVGNCNjMyNNIyMzIyPx///c7v/q5/+V1v+qy/+S3vH89+6j1u6GxO7N7Or/+OPN3t//8NzN09oewNpfo9k/jtLq9M6Fyczcw8a/w8YmgL+8ubjjz67Ox66Dj6BmfqAmaKBNXZzVy5nuyZktY5VjgY3xyYrGtorOqIrqy4aEI4RhI4SnY3+Nj3FNUHHOq2PSnFS1k1CcUE3Ok0aug0LVlTt/IyPj///O//iq5/jc3OoeyeRm0+Nxq+Njq+Pq3t/x59xmwNiqvdXc7s74486xwM6x1sZjqLgqcLiGsrHx5Kr/46r4y6rOq6qNmqpjiKqGppgxTI2quYZ/c4Zcc4ZjYoYqO3hNenHjr2NhI2Gcc1RUU1S4ek1GUE2GaUY4O0Z4TCqqiCOGiCO4cCNjYiONLSOqpNxAAAAMN0lEQVR42uzUu0pcURSA4c2aIhczSXTCCZKQ2n4KE0hhIRYiWonIeCu8gIKdVoJgKygWXmrB1l4fQNHWTl/B3tY9R2XwDQ76fcWCteqflQAAAAAAAAAAAICqqdVqhvEWBgAAAAAAAAAAAAAAAAAAAMA7t7B6P5uyD5/jSyp9jV8/z+byZe8mb7eN0vBoSidbjY7yBJXS3RtZfbzZvIj5PJtDr7qeOOpfX7uK/v8p/YhSX09K339HR3mCSnnu+iFedLW7/nPayHZjcvP6/G50YyaWetKnmC6KqcGnrvvae1cu/1tedE3VdPcuH+9E/bJYidZhtIpirN11WXvnGy/+254dOIj6xxzxctn135HiMEaK/Wjl1HVN1Tyya28vTYdhAMdfni6kTSOd/EDmxS66ELqbyDa6KGTNctquRESnNvM8dXPTiBo2t5nDZsGmuRmCSQR64flQaRclnbTDRURBZzpQUHSG6Kb3fX/atvoH3ovnczN4d/vdy8PzLj0rJ2MbLXbZoOusM9TvIudnKyHw+KZEVRhpvtJqFenY0ka0RtjHJ5CING4LaqhrrXBaw8w5CUJCSc/SFw1A2kkDeHKPXAdzzQs2OysJxSeMAhPhaqPQ5yCEjiQAY1c08a6ZmmGCkEjoxJHsRGMzRLrYfb1YSrvut3q28tDzw52EutCtKixRQBI2oCAkmExIrQ3PaZjjS2GXPF9bpWZ1aUvOvWK9kned8vWHtMFC61ZRvS2woGLOEoTE4m+spl3vmY7vouWu03Yr1Aez4HlLyDHqX+dbkk1KtgFk4gcIiWV7dmomRD5mx7cfCV3/hi8wwWcVZYf31OIk7f4G6EZY101H98uWWrFrJBzeNVyaXNlrrkm8r/umK9V5CoAml9x1wXKMPc747DBlwvsaiY53PZ5LV9RlXfKs/Hd/rb5bDDBh4l3vvHpYAeaxY4MQchDWNfsVcCtR7BoJh3edSogWZErW9YNR9t64Y3gNytpvzTtp10bdiM0O+mb2os67ZpttrsKIXSPh8K4DKrd3Y8OhtAWHAKYedvcGG3qGIJT3KyeD3d5mC3nJhnBPm9y17rJG1mDArpFwPhkSRmTfutmV/J8m/dPBsvbaKNSfm/kcA8Y6/+gAdo0EtwZMuYVov0nSGTp3+N/3FJbkQ/kqDXbhNgAcuuOz1z+xs8gDsz/ph+6tNJA0hxRJFoKQSPo/vC4sYW/ldeziNlcTjr+ea985bZWgp0evqki+0frGTb+Y+R57dlH6B3aNRFWQS/1/6nUTLuW+afOojSCEEEIIIYQQQgghhBBCf9iDQwIAAAAAQf9fe8MAAAAAAAAAALATe2eQGkUQheFBGJJKt10mtEybkBknM4lJNJKFcRuGnMBNdOvSExgjiCsFcaEHEF24SECvYEDIAVyIB3AjeAFx5f/+rpfn9HSaEeldP9LV9aqrqjP41Z+/S+hJvRQXJJY6oW3m1sL/vsjTn3chkpOLmTnc1MtZCmuemdufQ7c01pcFhKveXhAaFdOUH+Bl+StA9f1z9l0T1zoysU6FuHyz3b69JmcUkq8u8ry88nUFsw02WijaEn3Uhm0LuTJzfY19Z3Wus8harW6vz0saOn0TdQWpIVrzmwvyj53EZVwnAL48FM54Wq4TIkesEC5qJc+VOVwxrj9sLmCSAPbeAlvfOA5mh6iYpp7TcjVUhcN9hHMuYhejGri+MdsdHRJFsmtcvx9kuMX2RoB7SK43iDJoRVV6HRvKmcylqA+May6HrcWG6/qDPOOHvMlBOZ2eazIldL2u1HcFVHgiwI4gx+TaC1qBLhersgp34PT+FVNZQm8gF9I0jEG9ehVHvA9++OHlMK5JKn5y8JZNkpl3e+R6Uq9Jrul1Kdcm3qzZAmqijsgJw0tjoxSgURsTA6uaaxNTxJRcR7KETjifU65RMmOhOq5h/FZxrXq9M1f5rkSXf6gkmBfnwTlFX7lGSYRZGKN5DCG25JpBjLW2tXjmPdA4xjVBvn0ofSn0jV7XH0Qo9VQxBv+MqysZ45owiLb6ACpzUcDIqKGXuHh6wUs3HCndLDGKjetXHEKuZQ73gCJ+uv6Dsqy6HWOYL3CdhAVHri01vd5BzZZFMVL1S4nnEvB5Zv768OY3QfNg+0uP6hp0e2unB+89EKxpsrPBuOZ2e+AaLAN0HmX+moNhZ/rk3K42UU+4HA0XJ+pWy7mef5o/waWoS4PmMA5REGVMcOltywk8OddScyC1qNe4keq1i2jpDd2UQhrzz0BKrgO8uMih8osp15aacqPxXK7zO6tgo6vYeAR1+dGN3RHhU0Gd2Yb6PlpdHGaqvhoFvR4cr45xPelDcKiaI6NR57nhuq6gXl+7m28NANPPnXKuGc6LPvOsOT0zmgg823AOXAMjMj3hrwGsOg97V77PufZgM/Xsod0IbzXXwVo8n5Jr6vV+fmdtBI3BUUOchWuQubyye/2hAIo6MA8XabBNcruj96vmQ8h2geuDx6oZSnlfzHrDdT1hXP/yulPx89PEN+aQawDNlnx7LbJc0MXZWEc9cA0iWZvUa3RCHY+nDkMv3Zst6HXyAgtLuY6m4TosRtRg2Kfk+upSB2NUfDOQ2Vf3bHq9O3oM+EgpmoRUgdb6DTJzy9TsSa6HbbPfmTA9VAPTcF1PmA8hbsFeo1wv7ofQm5JZJODDct1i/ieu0TtwHZmDDnrN+aSU+vddbs0I+tX+OvHkGiI8vQ+RtWBcg0oCalyTzCH1mXUYEjXKptfK9XADc4S52hZ4bnzX6+vEA3IdRjR6XV8ALej13u8g0IGivSd3OuM8CJ50FoB0H8BajpDMfIhxnSMalfvrM66DEaErwXOj3O5knbuP6YsrXtYUx6hAh7HFNPXynzR43lx6yakquU5j0esHuoNoXOdECroQWG5bv+tlwA9cPxytdXsrR6S8RK/RNQPaCPJf9Nd/6/XBmWU5ariuKebvdMSHqF4nutshTRrE0lGjUYA2gV5zKKnC61F5a1zziZLoI5vkGlAL18/2iBW3/CC9Ym9iOfCLYf6Yos8x1ft8WBIfN/m13kzp/c/n2kV/2Luf1KaiKADjvos4EZ4gDhQHgmaSgZRkmKb+CQiZBCQGlI5dR7cguojO3INuwY24Bc85N/GQGoo3wXju7feDlrzmkd7Bl8PNe4Vq13/M69c5MwlcPuYN8wZCv54NtUu70KFZ75zXmq+Ncn0w3O6aeX10FrJ3LXVZG3krYPo8Sm0aWrP2jB/3NvLyT6TzrXmdr/xtXsM/N64vfOuhj9ycnqV9t3/8/oPuwO3V8o7EQ7Yctw9lvF888s2Tv6DLi9q8meXoStd+k8S2x+tbjIOnL58MB/k63UA+Mkrfw53zWl7g+aXdpNFzr53X3rW8YfBPfL6vPd5bj0TvWVuSvvfmGTmf1/IrtfTNdZYH69g9Wtu+r/fVWr8mayHbCvXsK4e9HPXJ71Vau5mftglfT7ZbNN61zViL2fsW+Ra6dKrh59xllP/Qk9xDqfRSnrUnJW3djljX9l6xqb9rXg+43Vgl7fr/6fn3x8ia6howdA0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOIrb4dwCDhYuo3ALQo3CZRRuQahRuIzCLQg1CpdRuAWhRuEyCrcg1ChcRuEWhBqFyyjcglCjcBmFWxBqFC6jcAtCjcJlFG5BqNG1Gd05DF2jwBG77g5B1/hbdA3QNW4oukaL6Botomu0iK7RIrpGi+gaLaJrtIiu0SK6RovKuj77NpFvqyQ+vui6kzfpt+WErhFG4bweS74W98lb7fr7pBuPulfvpvKQrhFH6T5kMe9mS+uaeY24SrvWmOdd7trotGZ/jWCKup6lNDpbSda2yT6/WCUnG266RhSl++uRP2ZeI6zyrscp08J9g31+SteIY495vZjbPmTqu2w5/ELXCGSPrk9+nsrmY94xrxFWcdezUTdLX1fSN/MaYRV2vUhpqt8la+Y14irqWrcf2vJIyl5OmNcIq6hrK1sGtpql5aekfGLTNaLg7/nQIrpGi+gaLaJrtIiu0SK6RovoGi2ia7SIrtEiukaL6Botomu0iK7RIv7PHVoULqNwC0KNwmUUbkGoUbiMwi0INQqX0a926ZgGAACGYdiO8cc8aShayUaQI3FBNIrbKC6IRhtnAAAAAAAAAAAA4B01Agndr+a42wAAAABJRU5ErkJggg==)

- 点击创建规则；

![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAlUAAABUCAMAAACcGzIMAAABnlBMVEX////5+fnw8PAeutji4uIjIyP7ODiEx/nF+/+mYCMjYKbb5dgeuur5x4T5+cnl+fnlqGEjI4Ujhcn5+eXJhSOm4vnJ+flhqOUjYqr54qb//87q//+Gy///y4aEI2HO///S09Tqq2OGIyPb//5hI4SEIyOq5/8euvDF2djOiCMjI2L/9PHx7eqo9P//+/Ue2fUe5fn//+pjq+qqYiOD7f2DutgjiM7/56qEI4RgIyOoutjl+eVjI2PJ+eXx//////nx//mGI2NjIyP59uxhYKbKyISoyagnXI0rIyRDJSPb9P/c8fj5+PLC1O3b5eyi1+z58OP77NTJ+clhqKaEhYTKn3BWLiPx//3T7/nF6fmD3/fx7fW94fKv3vKLxOmGy85hqMnp27DJ4qZEdaY1ZZvlvpXry4/Xs4spU37XqXnJqGGmYGFhI2G5jlysfEqqiCPy9vmouuqDuurl+cmEx8mDnMUjiL6Gq6rlx6YjhaaEYKYjhYSmYIQjQnkjQHEjQWzOiGOqiGNjYmOmqGEjMl2GiCO6hSNjYiOEYCNpQiOGErPBAAAHXklEQVR42uzSMQqAQAxFwV0Me/8j21goiFWMEWa6lPm8AQAAAAAAAAAA9La+NSgS6Z6qulzbm24+XFEsbeXxNzGT9atqHtasFVkrq0pVqlLVmapUpaq+dvbMw0lpIArjcZhMBAUhgoiCioLSzjsRQey99957772Xset/7ft237IJOuMlQHTGfHOX3WyW6G5+fO/lXTBULQipCqkKqQqpCqkKqfrHFFKlFVIVUhVS9e8qpErr/6VqzsK87khV24XU/JCqkCrPqpokYFQEQP1UNYDVsuV5MWxCdI4POTV7rieqNl9/+iCkKjCq4rEZf4GqJlFznohh5V1UiebgB7NJVG1cTH2ijC9I3uhC0RtVm4/uvLKb+4mVs7g3M2qmZiXKppY9XKrG1zVEO33qNDTc1Z0liwyhSmRCdqbxBQ9UpS3R5EpYFSvpfqi5rq0v1TJi4aSsfPpJXjpGIRoSFzxQhT1UH01mxc5a6grGAqQqT+3suZ01i9mrli03ewJGIEheLQzqVQeO7tx+Rm+sTc/ChCyse15GXxkmVeDj9KI6KKGf8XU0Z99UObfeo6pCPRwnlq6I8ExvVM180bWS/d+KRNlyQXW4nJUWwtMuRC3MYapsXjpQEEP48UYV9hAfXR21klmmM3WoTAewHixVhdpiFQS1VxXbeh6wY6pY4MxzXnXg7o7tJ917kJbbPFqqxoDU+ocKlBaTE2nV2aX2LaqMb1jFVLXq3qmCEq9mKK8CQaaSeqRxMqWEwsoW7kb8wM8EjbFbNu0GU0WbMThViAa4GRooWKo6m5oCo9pimT6BK2dsozlyDJefKSfTdjZJqgDVFQdUMGk3Vexc2SFTNQaKCCEGZWL9qpahNI29alCqdACCahmGyxHq07WMCEkCE+CU2zKDGlgVe9XAVOkIKKgC4mujAq7gs/XzCz9KPLaabeFVBYEWUTVnYbMIbIg3gzWAVx28t+Pyif5BcARlJVUWhoZLFUOl4hqgwhl7VWRYVKU5xgAnbrCi3J2MeKrxXqqYRLwnpFbfzoAfLJWpGrZXqTh7WLJmBedVhdlzq013iQGBjhqiSkTGX+sLRVdaNVmqABUn6jpFl16FfRwZVeMbdhlQQ7HiyKt+T1XDB1XwCX56nC4rfLrALU1fWaUL0dhX7m7/bsHC3FRx0PJLFfYSh162DqrRBlivKmgs+qiiY7XNyVRVQ+SOjcXJU3Wt+/aseyRXotU+KVviXWlEVEFjDXHogSKOzAwl76TTiyqEE1M15ocqbRXMSBxksTnZjlCoxQ89XssIw059sumUqcqVfFK1rSvutfbvUYV6QQFxTqdIDqo6a7Ztyhu6iCCQa/v2KhQVbpzpp4qeQ6Kc2oqv86ipYlAInSWL9nIlAcGQBvDb8k+VIiSbKPesCgGRhRS9n6p0L/3Bq4n6TZQHp6q0Fp8GVeq/k7pUCpwq02w7ciSdVxFV3/SLH8VCrpX69CpgdXPnjbN9VGHTkHOMNltvgB0Ghc5U9UDVstavIs6mGnVJ1aOpFc9UsVE5vArgKNky3iuJiJhV6Q81NiZj0XFK8yVVNOaLqpWXFFXaq/BaELxXudjQXkUWljeYKjIrmlLAgG+vAlbH3u13UXWonKUNjT0um9ZIvWrMMJaukKAgz2q0DFYddYZTBuXwxi7qQxMYhbxRBQocXjXFXRJ1e5Wmir0qUT4XjdGjfw0sJYM12fNCFfi56KQqXtsfPUJkAtTAqVJlz7ag6rk0LhptYwKoKqLmPvsaiuy+vQrac/3YewdWydTW2C3Tlt1Zo6pXgSrBSINBaSHuMTMwMYB2al2jhaJWRZqaUfFRW7d1XjV5qo7Au0RembbSqUN0A2IA5zBwz1QlbQGU+GeTWcrYYHsvo9bfoEr/iQaAwZo6m8zZ901ZcEc8bIqhvN93QI3V1WOfz6mTNPIpV61Fa/hUIbwBFJGf110VUkTBuiyJVvC3HS5GeKMKORGo4i/Fn6jSeRUJ5QdR0MINkrasUuS6lleqZgqbIlJFBOYIiFsET9V9YMQl0Q7y86JgpVjbtmZxp4T3P5pAg6kvGPbvVYzVj+0neAvezHJWoLOj86oJA6IqFNLz8eOVCHqs3RtEwt6ISNpwlWnDZA9UkTFIquJYjFqZKoriVMtyRUAGICnyLUxO96Ds4laTpgoJlEqlunQfWbyZSXWxYKlayJV0CH+60ar+bO8OUhCGgQCKhiCeQhcewS56Cw/hTTy6qRVHXLW4SYb3NiIILubTlBLSdnt+mduV7PqYlt+1r+2jBdauWT9OG6oK95udMLl3wizpvJxjMYsn58tyOK0r3hwP42f7q1S1k72gquqXqoKqVKWqfqkqqEpVquqXqoKqVKWqfjlpKKhKVarql6qCqlSlqn4dVk7G/nAy9mCOZYdk/85bsrmqKidVUZLNVVU5qYqSbK6qyklVlGRzVVVOqqIkm6uqclIVJdlcVZWTqvjmHfHABhX+oiqqqhiAqqiqYgCqoqqKAaiKqioGoCqqqhiAqqiqYgBPJb2WvG4yyQ0AAAAASUVORK5CYII=)

- 进行跨域规则设置；

![](https://qiniu.ko25891wan.top/日记软件/obsition/re_mall_windows_deploy_01-3d024788.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#mall-admin)mall-admin

- 启动项目：直接运行`com.macro.mall.MallAdminApplication`的main方法即可；
- 接口文档地址：http://localhost:8080/swagger-ui/

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#mall-search)mall-search

- 启动项目：直接运行`com.macro.mall.search.MallSearchApplication`的main方法即可；
- 接口文档地址：http://localhost:8081/swagger-ui/
- 使用前需要先调用接口导入数据；http://localhost:8081/esProduct/importAll
- 如出现无法启动的问题，可以先删除Elasticsearch里面的数据再启动

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#mall-portal)mall-portal

- 启动mall-portal项目：直接运行`com.macro.mall.portal.MallPortalApplication`的main方法即可；
- 接口文档地址：http://localhost:8085/swagger-ui/

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html#%E6%8E%A5%E5%8F%A3%E6%96%87%E6%A1%A3)接口文档

> 由于mall项目中的大部分接口需要登录认证之后才能访问，这里以mall-admin为例，介绍下接口文档的访问。

- 成功启动`mall-admin`模块后，我们可以通过该地址来访问接口文档：http://localhost:8080/swagger-ui/

![](https://qiniu.ko25891wan.top/日记软件/obsition/re_mall_windows_deploy_02-195a3300.png)

- 接下来我们需要访问登录接口`/admin/login`来获取token；

![](https://qiniu.ko25891wan.top/日记软件/obsition/re_mall_windows_deploy_03-e33d7bcd.png)

- 登录成功后，返回结果中将返回token和tokenHead；

![](https://qiniu.ko25891wan.top/日记软件/obsition/re_mall_windows_deploy_04-88c44270.png)

- 然后点击Swagger文档的`Authorize`按钮，输入`tokenHead`+`token`拼接的认证请求头，注意`tokenHead`后面有个空格；

![](https://qiniu.ko25891wan.top/日记软件/obsition/re_mall_windows_deploy_05-d59b5125.png)

- 之后调用需要登录认证的接口，就可以正常访问了；

![](https://qiniu.ko25891wan.top/日记软件/obsition/re_mall_windows_deploy_06-83846569.png)

- 对于`mall-portal`模块的接口调用也是一样的，登录获取token的接口为`/sso/login`。
![image.png](https://qiniu.ko25891wan.top/日记软件/obsition/20240202175446.png)

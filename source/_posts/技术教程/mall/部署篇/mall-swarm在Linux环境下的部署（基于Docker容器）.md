---
categories:
  - 技术教程
  - mall
  - 部署篇
title: mall-swarm在Linux环境下的部署（基于Docker容器）
date: 2024-01-21 17:30:24
tags:
---
> 本文以 `mall-swarm` 项目为例，主要介绍一个微服务架构的电商项目如何在 Docker 容器下部署，涉及到大量系统组件的部署及多个 Spring Cloud 微服务应用的部署，基于 CentOS 7.6。

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_docker.html#%F0%9F%91%8D-%E7%9B%B8%E5%85%B3%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B)👍 相关视频教程

[mall-swarm在Linux环境下的部署（基于Docker容器）open in new window](https://t.zsxq.com/14hxqfcjA)

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_docker.html#%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA)环境搭建

### [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_docker.html#%E5%9F%BA%E7%A1%80%E7%8E%AF%E5%A2%83%E9%83%A8%E7%BD%B2)基础环境部署

> `mall-swarm`运行需要的系统组件如下，使用Docker Compose 批量安装更方便，Docker Compose使用请参考：[使用Docker Compose部署SpringBoot应用](https://www.macrozheng.com/mall/reference/docker_compose.html) 。

|组件|版本号|
|---|---|
|Mysql|5.7|
|Redis|7.0|
|MongoDb|4.x|
|RabbitMq|3.9|
|Nginx|1.22|
|Elasticsearch|7.17.3|
|Logstash|7.17.3|
|Kibana|7.17.3|
|Nacos|2.1.0|

- 本项目已经提供好了Docker Compose脚本，直接执行即可，脚本地址：https://github.com/macrozheng/mall-swarm/blob/master/document/docker/docker-compose-env.yml

```
docker-compose -f docker-compose-env.yml up -d
```

- 某些系统组件无法启动问题，具体参考：[mall在Linux环境下的部署（基于Docker Compose）](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html)
    
- 由于新增了`Logstash`组件，需要预先创建好Logstash的配置文件，再安装Logstash的JSON插件，配置文件地址：https://github.com/macrozheng/mall-swarm/tree/master/document/elk/logstash.conf
    

```
# 创建好配置文件目录
mkdir /mydata/logstash
# 进入容器使用如下命令安装插件
logstash-plugin install logstash-codec-json_lines
```

### [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_docker.html#%E9%95%9C%E5%83%8F%E6%89%93%E5%8C%85%E4%B8%8A%E4%BC%A0)镜像打包上传

> 一共6个应用服务需要打包成Docker镜像，具体如何打包可以参考[使用Maven插件为SpringBoot应用构建Docker镜像](https://www.macrozheng.com/mall/reference/docker_maven.html) 。需要注意的是如果打包过程中遇到找不到`mall-common`或`mall-mbg`模块，需要先按顺序将这些模块install到本地maven仓库再进行打包。

|应用|说明|
|---|---|
|mall-monitor|监控中心|
|mall-gateway|微服务网关|
|mall-auth|认证中心|
|mall-admin|商城后台服务|
|mall-portal|商城前台服务|
|mall-search|商城搜索服务|

镜像打包上传完成后，完整docker仓库镜像示意图：

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_run_09-31227372.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_docker.html#%E5%BA%94%E7%94%A8%E9%83%A8%E7%BD%B2)应用部署

### [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_docker.html#%E5%9C%A8nacos%E4%B8%AD%E6%B7%BB%E5%8A%A0%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6)在Nacos中添加配置文件

- 由于我们使用Nacos作为配置中心，统一管理配置，所以我们需要将项目`config`目录下的所有配置都添加到Nacos中，Nacos访问地址：http://192.168.3.101:8848/nacos/

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_run_new_01-370dadd6.png)

- 注意，配置文件的文件名称需要和Nacos中的`Data Id`一一对应；

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_run_new_02-4718f843.png)

### [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_docker.html#%E4%BD%BF%E7%94%A8docker-compose%E6%89%B9%E9%87%8F%E9%83%A8%E7%BD%B2%E6%89%80%E6%9C%89%E5%BA%94%E7%94%A8)使用Docker Compose批量部署所有应用

- 直接使用提供好的Docker Compose脚本，启动所有应用即可，脚本地址：https://github.com/macrozheng/mall-swarm/blob/master/document/docker/docker-compose-app.yml

```
docker-compose -f docker-compose-app.yml up -d
```

- 启动成功后，可以查看API文档信息，访问地址：http://192.168.3.101:8201/doc.html

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_run_05-d51bc5da.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_docker.html#%E8%BF%90%E8%A1%8C%E5%AE%8C%E6%88%90%E6%95%88%E6%9E%9C%E5%B1%95%E7%A4%BA)运行完成效果展示

- 查看注册中心注册服务信息，访问地址：http://192.168.3.101:8848/nacos/

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_run_new_04-62d4e505.png)

- 监控中心应用信息，访问地址：http://192.168.3.101:8101

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_run_new_05-d8c0b90d.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_run_new_06-6083362c.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_run_new_07-b8115d1b.png)

- 日志收集系统信息，访问地址：http://192.168.3.101:5601

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_run_new_08-cd0506e4.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_swarm_deploy_docker.html#%E5%8F%AF%E8%A7%86%E5%8C%96%E7%AE%A1%E7%90%86%E5%B7%A5%E5%85%B7)可视化管理工具

> Portainer 是一款轻量级的应用，它提供了图形化界面，用于方便的管理Docker环境，包括单机环境和集群环境，下面我们将用Portainer来管理Docker容器中的应用。

- 官网地址：https://github.com/portainer/portainer
    
- 获取Docker镜像文件：
    

```
docker pull portainer/portainer
```

- 使用docker容器运行Portainer：

```
docker run -p 9000:9000 -p 8000:8000 --name portainer \
--restart=always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v /mydata/portainer/data:/data \
-d portainer/portainer
```

- 查看Portainer的DashBoard信息，访问地址：http://192.168.3.101:9000

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_run_new_09-5ed7108d.png)

- 查看所有运行中的容器信息：

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_run_new_10-9a140de4.png)

- 查看所有已经下载的Docker镜像：

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_run_new_11-2bb62979.png)

- 查看`mall-portal`应用的统计信息：

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_run_new_12-a686daf5.png)

- 查看`mall-portal`应用的运行过程中打印的日志信息：

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_run_new_13-8f3d9869.png)

- 进入`mall-portal`应用的容器内部来操作容器内部系统：

![](https://qiniu.ko25891wan.top/日记软件/obsition/mall_swarm_run_new_14-051e33ce.png)
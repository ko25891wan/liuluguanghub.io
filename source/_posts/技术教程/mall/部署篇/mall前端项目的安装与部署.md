---
categories:
  - 技术教程
  - mall
  - 部署篇
title: mall前端项目的安装与部署
date: 2024-01-21 17:30:30
tags:
---
> 本文主要讲解 mall 前端项目 mall-admin-web 的在 Windows 和 Linux 环境下的安装及部署。

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_web.html#%F0%9F%91%8D-%E7%9B%B8%E5%85%B3%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B)👍 相关视频教程

- [mall项目前端开发环境搭建_上篇open in new window](https://t.zsxq.com/0eRDjsod5)
- [mall项目前端开发环境搭建_下篇open in new window](https://t.zsxq.com/0enEtKLI5)
- [mall项目前端在Linux环境下的部署open in new window](https://t.zsxq.com/10iG2uRqr)

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_web.html#windows%E4%B8%8B%E7%9A%84%E5%AE%89%E8%A3%85%E5%8F%8A%E9%83%A8%E7%BD%B2)Windows下的安装及部署

- 下载nodejs并安装，最好使用`v12.14.0`版本，版本不对会导致`npm install`出错，下载地址：https://nodejs.org/dist/v12.14.0/node-v12.14.0-x64.msi
    
- 下载mall-admin-web的代码；
    
    - Github：https://github.com/macrozheng/mall-admin-web
    - Gitee：https://gitee.com/macrozheng/mall-admin-web
- 从IDEA中打开mall-admin-web项目；
    

![](https://www.macrozheng.com/assets/tech_screen_12-b52a6b69.png)

- 切换至淘宝镜像源加速访问；

```
# 设置为淘宝的镜像源
npm config set registry https://registry.npm.taobao.org
# 设置为官方镜像源
npm config set registry https://registry.npmjs.org
```

- 打开控制台输入命令安装相关依赖；

```
npm install
```

![](https://www.macrozheng.com/assets/tech_screen_13-e44020a2.png)

![](https://www.macrozheng.com/assets/tech_screen_14-ba050135.png)

- node-sass无法下载导致构建失败时可使用如下命令下载。

```
# linux
SASS_BINARY_SITE=https://npm.taobao.org/mirrors/node-sass/ npm install node-sass
# window
set SASS_BINARY_SITE=https://npm.taobao.org/mirrors/node-sass&& npm install node-sass
```

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_web.html#%E5%B7%B2%E6%90%AD%E5%BB%BAmall%E5%90%8E%E5%8F%B0%E7%8E%AF%E5%A2%83%E7%9A%84%E5%90%AF%E5%8A%A8)已搭建mall后台环境的启动

- 可以直接运行mall-admin服务；

![](https://www.macrozheng.com/assets/tech_screen_15-35c1016f.png)

- 使用命令启动mall-admin-web，在IDEA控制台中输入如下命令：

```
npm run dev
```

![](https://www.macrozheng.com/assets/tech_screen_16-06b14787.png)

- 访问地址查看效果：http://localhost:8090

![](https://www.macrozheng.com/assets/tech_screen_17-9bdf6fb6.png)

- 进行登录操作，发现调用的是本地接口：

![](https://www.macrozheng.com/assets/tech_screen_18-1080d372.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_web.html#%E6%9C%AA%E6%90%AD%E5%BB%BAmall%E5%90%8E%E5%8F%B0%E7%8E%AF%E5%A2%83%E7%9A%84%E5%90%AF%E5%8A%A8)未搭建mall后台环境的启动

> 未搭建mall后台的需要使用线上api进行访问，线上API地址：`https://admin-api.macrozheng.com` 。

- 修改`dev.env.js`文件中的`BASE_API`为线上地址；

![](https://www.macrozheng.com/assets/tech_screen_19-7f691f63.png)

- 使用命令启动mall-admin-web，在IDEA控制台中输入如下命令：

```
npm run dev
```

![](https://www.macrozheng.com/assets/tech_screen_16-06b14787.png)

- 访问地址http://localhost:8090 查看效果：

![](https://www.macrozheng.com/assets/tech_screen_17-9bdf6fb6.png)

- 进行登录操作，发现调用的是线上接口：

![](https://www.macrozheng.com/assets/tech_screen_20-ea2f33cd.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_web.html#linux%E4%B8%8B%E7%9A%84%E9%83%A8%E7%BD%B2)Linux下的部署

- 修改`prod.env.js`文件的配置

![](https://www.macrozheng.com/assets/tech_screen_21-14b39673.png)

- 使用命令进行打包；

```
npm run build
```

![](https://www.macrozheng.com/assets/tech_screen_22-4e706126.png)

- 打包后的代码位置

![](https://www.macrozheng.com/assets/tech_screen_23-308b1c38.png)

- 将dist目录打包为dist.tar.gz文件

![](https://www.macrozheng.com/assets/tech_screen_24-013248c4.png)

- Linux上Nginx的安装可以参考[mall在Linux环境下的部署（基于Docker容器）](https://www.macrozheng.com/mall/deploy/mall_deploy_windows.html) 中的Nginx部分；
    
- 将`dist.tar.gz`上传到linux服务器（nginx相关目录）；
    

![](https://www.macrozheng.com/assets/tech_screen_27-2cb62da6.png)

- 使用该命令进行解压操作；

```
tar -zxvf dist.tar.gz
```

- 删除nginx的html文件夹；

```
rm -rf html
```

- 移动dist文件夹到html文件夹；

```
mv dist html
```

- 运行mall-admin服务；

```
docker start mall-admin
```

- 重启nginx；

```
docker restart nginx
```

- 访问首页并登录：http://192.168.3.101

![](https://www.macrozheng.com/assets/tech_screen_25-b690d449.png)

- 发现调用的是Linux服务器地址。

![](https://www.macrozheng.com/assets/tech_screen_26-88e34e96.png)

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_web.html#%E9%A1%B9%E7%9B%AE%E6%BA%90%E7%A0%81%E5%9C%B0%E5%9D%80)项目源码地址

[https://github.com/macrozheng/mall-admin-web](https://github.com/macrozheng/https://github.com/macrozheng/mall-admin-web)
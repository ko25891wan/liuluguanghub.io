---
categories:
  - 技术教程
  - mall
  - 部署篇
title: mall在Linux环境下的部署（基于Docker Compose）
date: 2024-01-21 17:30:37
tags:
---
> 最简单的 mall 在 Linux 下部署方式，使用两个 Docker Compose 脚本即可完成部署。第一个脚本用于部署 mall 运行所依赖的服务（MySQL、Redis、Nginx、RabbitMQ、MongoDB、Elasticsearch、Logstash、Kibana），第二个脚本用于部署 mall 中的应用（mall-admin、mall-search、mall-portal）。

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#%F0%9F%91%8D-%E7%9B%B8%E5%85%B3%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B)👍 相关视频教程

[mall在Linux环境下的部署（基于Docker Compose）open in new window](https://t.zsxq.com/10J1Pxc0W)

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#docker%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA%E5%8F%8A%E4%BD%BF%E7%94%A8)docker环境搭建及使用

具体参考：[开发者必备Docker命令](https://www.macrozheng.com/mall/reference/docker_command.html)

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#docker-compose%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA%E5%8F%8A%E4%BD%BF%E7%94%A8)docker-compose环境搭建及使用

具体参考：[使用Docker Compose部署SpringBoot应用](https://www.macrozheng.com/mall/reference/docker_compose.html)

## [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#mall%E9%A1%B9%E7%9B%AE%E7%9A%84docker-compose%E9%83%A8%E7%BD%B2)mall项目的docker-compose部署

### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#%E8%BF%90%E8%A1%8C%E9%85%8D%E7%BD%AE%E8%A6%81%E6%B1%82)运行配置要求

CentOS 7.6版本，推荐6G以上内存。

### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#%E9%83%A8%E7%BD%B2%E7%9B%B8%E5%85%B3%E6%96%87%E4%BB%B6)部署相关文件

- 数据库脚本`mall.sql`：https://github.com/macrozheng/mall/blob/master/document/sql/mall.sql
- nginx配置文件`nginx.conf`：https://github.com/macrozheng/mall/blob/master/document/docker/nginx.conf
- Logstash配置文件`logstash.conf`：https://github.com/macrozheng/mall/blob/master/document/elk/logstash.conf
- 系统服务运行脚本`docker-compose-env.yml`：https://github.com/macrozheng/mall/tree/master/document/docker/docker-compose-env.yml
- 应用服务运行脚本`docker-compose-app.yml`：https://github.com/macrozheng/mall/tree/master/document/docker/docker-compose-app.yml

### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#%E9%83%A8%E7%BD%B2%E5%89%8D%E5%87%86%E5%A4%87)部署前准备

#### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#%E6%89%93%E5%8C%85%E5%B9%B6%E4%B8%8A%E4%BC%A0mall%E5%BA%94%E7%94%A8%E7%9A%84%E9%95%9C%E5%83%8F)打包并上传mall应用的镜像

需要打包mall-admin、mall-search、mall-portal的docker镜像，具体参考：[使用Maven插件为SpringBoot应用构建Docker镜像](https://www.macrozheng.com/mall/reference/docker_maven.html)

#### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#%E4%B8%8B%E8%BD%BD%E6%89%80%E6%9C%89%E9%9C%80%E8%A6%81%E5%AE%89%E8%A3%85%E7%9A%84docker%E9%95%9C%E5%83%8F)下载所有需要安装的Docker镜像

```
docker pull mysql:5.7
docker pull redis:7
docker pull nginx:1.22
docker pull rabbitmq:3.9-management
docker pull elasticsearch:7.17.3
docker pull kibana:7.17.3
docker pull logstash:7.17.3
docker pull mongo:4
docker pull minio/minio
```

#### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#elasticsearch)Elasticsearch

- 需要设置系统内核参数，否则会因为内存不足无法启动；

```
# 改变设置
sysctl -w vm.max_map_count=262144
# 使之立即生效
sysctl -p
```

- 需要创建`/mydata/elasticsearch/data`目录并设置权限，否则会因为无权限访问而启动失败。

```
# 创建目录
mkdir /mydata/elasticsearch/data/
# 创建并改变该目录权限
chmod 777 /mydata/elasticsearch/data
```

#### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#nginx)Nginx

需要拷贝nginx配置文件，否则挂载时会因为没有配置文件而启动失败。

```
# 创建目录之后将nginx.conf文件上传到该目录下面
mkdir /mydata/nginx/
```

#### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#logstash)Logstash

修改Logstash的配置文件`logstash.conf`中`output`节点下的Elasticsearch连接地址为`es:9200`。

```
output {
  elasticsearch {
    hosts => "es:9200"
    index => "mall-%{type}-%{+YYYY.MM.dd}"
  }
}
```

创建`/mydata/logstash`目录，并将Logstash的配置文件`logstash.conf`拷贝到该目录。

```
mkdir /mydata/logstash
```

### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#%E6%89%A7%E8%A1%8Cdocker-compose-env-yml%E8%84%9A%E6%9C%AC)执行docker-compose-env.yml脚本

> 将该文件上传的linux服务器上，执行docker-compose up命令即可启动mall所依赖的所有服务。

```
version: '3'
services:
  mysql:
    image: mysql:5.7
    container_name: mysql
    command: mysqld --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: root #设置root帐号密码
    ports:
      - 3306:3306
    volumes:
      - /mydata/mysql/data:/var/lib/mysql #数据文件挂载
      - /mydata/mysql/conf:/etc/mysql #配置文件挂载
      - /mydata/mysql/log:/var/log/mysql #日志文件挂载
  redis:
    image: redis:7
    container_name: redis
    command: redis-server --appendonly yes
    volumes:
      - /mydata/redis/data:/data #数据文件挂载
    ports:
      - 6379:6379
  nginx:
    image: nginx:1.22
    container_name: nginx
    volumes:
      - /mydata/nginx/conf:/etc/nginx #配置文件目录挂载
      - /mydata/nginx/html:/usr/share/nginx/html #静态资源根目录挂载
      - /mydata/nginx/logs:/var/log/nginx #日志文件挂载
    ports:
      - 80:80
  rabbitmq:
    image: rabbitmq:3.9-management
    container_name: rabbitmq
    volumes:
      - /mydata/rabbitmq/data:/var/lib/rabbitmq #数据文件挂载
    ports:
      - 5672:5672
      - 15672:15672
  elasticsearch:
    image: elasticsearch:7.17.3
    container_name: elasticsearch
    environment:
      - "cluster.name=elasticsearch" #设置集群名称为elasticsearch
      - "discovery.type=single-node" #以单一节点模式启动
      - "ES_JAVA_OPTS=-Xms512m -Xmx1024m" #设置使用jvm内存大小
    volumes:
      - /mydata/elasticsearch/plugins:/usr/share/elasticsearch/plugins #插件文件挂载
      - /mydata/elasticsearch/data:/usr/share/elasticsearch/data #数据文件挂载
    ports:
      - 9200:9200
      - 9300:9300
  logstash:
    image: logstash:7.17.3
    container_name: logstash
    environment:
      - TZ=Asia/Shanghai
    volumes:
      - /mydata/logstash/logstash.conf:/usr/share/logstash/pipeline/logstash.conf #挂载logstash的配置文件
    depends_on:
      - elasticsearch #kibana在elasticsearch启动之后再启动
    links:
      - elasticsearch:es #可以用es这个域名访问elasticsearch服务
    ports:
      - 4560:4560
      - 4561:4561
      - 4562:4562
      - 4563:4563
  kibana:
    image: kibana:7.17.3
    container_name: kibana
    links:
      - elasticsearch:es #可以用es这个域名访问elasticsearch服务
    depends_on:
      - elasticsearch #kibana在elasticsearch启动之后再启动
    environment:
      - "elasticsearch.hosts=http://es:9200" #设置访问elasticsearch的地址
    ports:
      - 5601:5601
  mongo:
    image: mongo:4
    container_name: mongo
    volumes:
      - /mydata/mongo/db:/data/db #数据文件挂载
    ports:
      - 27017:27017
  minio:
    image: minio/minio
    container_name: minio
    command: server /data --console-address ":9001" #指定数据目录及console运行端口启动
    volumes:
      - /mydata/minio/data:/data #数据目录挂载
    environment:
      - "MINIO_ROOT_USER=minioadmin"
      - "MINIO_ROOT_PASSWORD=minioadmin"
    ports:
      - 9090:9000
      - 9001:9001
```

上传完后在当前目录下执行如下命令：

```
docker-compose -f docker-compose-env.yml up -d
```

![](https://www.macrozheng.com/assets/re_mall_docker_compose_01-d51a532b.png)

### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#%E5%AF%B9%E4%BE%9D%E8%B5%96%E6%9C%8D%E5%8A%A1%E8%BF%9B%E8%A1%8C%E4%BB%A5%E4%B8%8B%E8%AE%BE%E7%BD%AE)对依赖服务进行以下设置

当所有依赖服务启动完成后，需要对以下服务进行一些设置。

#### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#mysql)mysql

> 需要创建mall数据库并创建一个可以远程访问的对象reader。

- 将mall.sql文件拷贝到mysql容器的/目录下：

```
docker cp /mydata/mall.sql mysql:/
```

- 进入mysql容器并执行如下操作：

```
#进入mysql容器
docker exec -it mysql /bin/bash
#连接到mysql服务
mysql -uroot -proot --default-character-set=utf8
#创建远程访问用户
grant all privileges on *.* to 'reader' @'%' identified by '123456';
#创建mall数据库
create database mall character set utf8;
#使用mall数据库
use mall;
#导入mall.sql脚本
source /mall.sql;
```

#### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#elasticsearch-1)elasticsearch

> 需要安装中文分词器IKAnalyzer，并重新启动。

- 注意下载与Elasticsearch对应的版本，下载地址：https://github.com/medcl/elasticsearch-analysis-ik/releases

![](https://www.macrozheng.com/assets/mall_linux_deploy_new_02-d402f993.png)

- 下载完成后解压到Elasticsearch的`/mydata/elasticsearch/plugins`目录下；

![](https://www.macrozheng.com/assets/mall_linux_deploy_new_03-d123512a.png)

- 重新启动服务：

```
docker restart elasticsearch
```

#### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#logstash-1)logstash

> 旧版本需要安装`json_lines`插件，并重新启动，新版本已内置无需安装。

```
docker exec -it logstash /bin/bash
logstash-plugin install logstash-codec-json_lines
docker restart logstash
```

#### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#rabbitmq)rabbitmq

> 需要创建一个mall用户并设置虚拟host为/mall。

- 访问管理页面地址：http://192.168.3.101:15672

![](https://www.macrozheng.com/assets/mall_linux_deploy_03-78dad8ac.png)

- 输入账号密码并登录：guest guest
    
- 创建帐号并设置其角色为管理员：mall mall
    

![](https://www.macrozheng.com/assets/mall_linux_deploy_04-2fd308c9.png)

- 创建一个新的虚拟host为：/mall

![](https://www.macrozheng.com/assets/mall_linux_deploy_05-9a7bf37b.png)

- 点击mall用户进入用户配置页面

![](https://www.macrozheng.com/assets/mall_linux_deploy_06-309462ae.png)

- 给mall用户配置该虚拟host的权限

![](https://www.macrozheng.com/assets/mall_linux_deploy_07-782ca72c.png)

### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#%E6%89%A7%E8%A1%8Cdocker-compose-app-yml%E8%84%9A%E6%9C%AC)执行docker-compose-app.yml脚本

> 将该文件上传的linux服务器上，执行docker-compose up命令即可启动mall所有的应用。

```
version: '3'
services:
  mall-admin:
    image: mall/mall-admin:1.0-SNAPSHOT
    container_name: mall-admin
    ports:
      - 8080:8080
    volumes:
      - /mydata/app/mall-admin/logs:/var/logs
      - /etc/localtime:/etc/localtime
    environment:
      - 'TZ="Asia/Shanghai"'
    external_links:
      - mysql:db #可以用db这个域名访问mysql服务
  mall-search:
    image: mall/mall-search:1.0-SNAPSHOT
    container_name: mall-search
    ports:
      - 8081:8081
    volumes:
      - /mydata/app/mall-search/logs:/var/logs
      - /etc/localtime:/etc/localtime
    environment:
      - 'TZ="Asia/Shanghai"'
    external_links:
      - elasticsearch:es #可以用es这个域名访问elasticsearch服务
      - mysql:db #可以用db这个域名访问mysql服务
  mall-portal:
    image: mall/mall-portal:1.0-SNAPSHOT
    container_name: mall-portal
    ports:
      - 8085:8085
    volumes:
      - /mydata/app/mall-portal/logs:/var/logs
      - /etc/localtime:/etc/localtime
    environment:
      - 'TZ="Asia/Shanghai"'
    external_links:
      - redis:redis #可以用redis这个域名访问redis服务
      - mongo:mongo #可以用mongo这个域名访问mongo服务
      - mysql:db #可以用db这个域名访问mysql服务
      - rabbitmq:rabbit #可以用rabbit这个域名访问rabbitmq服务
```

上传完后在当前目录下执行如下命令：

```
docker-compose -f docker-compose-app.yml up -d
```

![](https://www.macrozheng.com/assets/re_mall_docker_compose_02-00d5be04.png)

### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#%E5%BC%80%E5%90%AF%E9%98%B2%E7%81%AB%E5%A2%99%E5%8D%B3%E5%8F%AF%E5%9C%A8%E5%85%B6%E4%BB%96%E4%B8%BB%E6%9C%BA%E4%B8%8A%E8%AE%BF%E9%97%AE%E6%89%80%E6%9C%89%E6%9C%8D%E5%8A%A1)开启防火墙即可在其他主机上访问所有服务

```
systemctl stop firewalld
```

### [#](https://www.macrozheng.com/mall/deploy/mall_deploy_docker_compose.html#%E8%87%B3%E6%AD%A4%E6%89%80%E6%9C%89%E6%9C%8D%E5%8A%A1%E5%B7%B2%E7%BB%8F%E6%AD%A3%E5%B8%B8%E5%90%AF%E5%8A%A8)至此所有服务已经正常启动

![](https://www.macrozheng.com/assets/re_mall_docker_compose_03-a5a11882.png)

![](https://www.macrozheng.com/assets/re_mall_docker_compose_04-2782346d.png)
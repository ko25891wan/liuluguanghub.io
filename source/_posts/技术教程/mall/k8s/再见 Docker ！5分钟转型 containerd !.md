---
categories:
  - 技术教程
  - mall
  - k8s
title: 再见 Docker ！5分钟转型 containerd !
date: 2024-01-21 17:20:12
tags:
---
> Docker 作为非常流行的容器技术，之前经常有文章说它被 K8S 弃用了，取而代之的是另一种容器技术 containerd！其实 containerd 只是从 Docker 中分离出来的底层容器运行时，使用起来和 Docker 并没有啥区别，本文主要介绍下 containerd 的使用，希望对大家有所帮助！

## [#](https://www.macrozheng.com/k8s/containerd_start.html#containerd%E7%AE%80%E4%BB%8B)containerd简介

containerd是一个工业级标准的容器运行时，它强调简单性、健壮性和可移植性。containerd可以在宿主机中管理完整的容器生命周期，包括容器镜像的传输和存储、容器的执行和管理、存储和网络等。

## [#](https://www.macrozheng.com/k8s/containerd_start.html#docker-vs-containerd)Docker vs containerd

containerd是从Docker中分离出来的一个项目，可以作为一个底层容器运行时，现在它成了Kubernete容器运行时更好的选择。

不仅仅是Docker，还有很多云平台也支持containerd作为底层容器运行时，具体参考下图。

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:22_16f577a566cf3f03004eee77c47e9c69.png)

## [#](https://www.macrozheng.com/k8s/containerd_start.html#k8s-cri)K8S CRI

K8S发布CRI（Container Runtime Interface），统一了容器运行时接口，凡是支持CRI的容器运行时，皆可作为K8S的底层容器运行时。

K8S为什么要放弃使用Docker作为容器运行时，而使用containerd呢？

如果你使用Docker作为K8S容器运行时的话，kubelet需要先要通过`dockershim`去调用Docker，再通过Docker去调用containerd。

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:22_93a6c4acb7809dc74e9c639daa56e6a8.png)

如果你使用containerd作为K8S容器运行时的话，由于containerd内置了`CRI`插件，kubelet可以直接调用containerd。

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:22_f103799335508412bb352d950173213f.png)

使用containerd不仅性能提高了（调用链变短了），而且资源占用也会变小（Docker不是一个纯粹的容器运行时，具有大量其他功能）。

## [#](https://www.macrozheng.com/k8s/containerd_start.html#containerd%E4%BD%BF%E7%94%A8)containerd使用

> 如果你之前用过Docker，你只要稍微花5分钟就可以学会containerd了，接下来我们学习下containerd的使用。

- 在之前的文章[《据说只有高端机器才配运行K8S，网友：1G内存的渣渣跑起来了！》open in new window](https://mp.weixin.qq.com/s/iDG7Wzq9DQHLFPtnqokOxQ)中我们安装了K3S，由于K3S中默认使用containerd作为容器运行时，我们只要安装好K3S就可以使用它了；
    
- 其实只要把我们之前使用的`docker`命令改为`crictl`命令即可操作containerd，比如查看所有运行中的容器；
    

```
crictl ps
```

```
CONTAINER           IMAGE               CREATED                  STATE               NAME                ATTEMPT             POD ID
4ca73ded41bb6       3b0b04aa3473f       Less than a second ago   Running             helm                20                  21103f0058872
3bb5767a81954       296a6d5035e2d       About a minute ago       Running             coredns             1                   af887263bd869
a5e34c24be371       0346349a1a640       About a minute ago       Running             nginx               1                   89defc6008501
```

- 查看所有镜像；

```
crictl images
```

```
IMAGE                                      TAG                 IMAGE ID            SIZE
docker.io/library/nginx                    1.10                0346349a1a640       71.4MB
docker.io/rancher/coredns-coredns          1.8.0               296a6d5035e2d       12.9MB
docker.io/rancher/klipper-helm             v0.4.3              3b0b04aa3473f       50.7MB
docker.io/rancher/local-path-provisioner   v0.0.14             e422121c9c5f9       13.4MB
docker.io/rancher/metrics-server           v0.3.6              9dd718864ce61       10.5MB
docker.io/rancher/pause                    3.1                 da86e6ba6ca19       327kB
```

- 进入容器内部执行bash命令，这里需要注意的是只能使用容器ID，不支持使用容器名称；

```
crictl exec -it a5e34c24be371 /bin/bash
```

- 查看容器中应用资源占用情况，可以发现占用非常低。

```
crictl stats
```

```
CONTAINER           CPU %               MEM                 DISK                INODES
3bb5767a81954       0.54                14.27MB             254B                14
a5e34c24be371       0.00                2.441MB             339B                16
```

## [#](https://www.macrozheng.com/k8s/containerd_start.html#%E6%80%BB%E7%BB%93)总结

从Docker转型containerd非常简单，基本没有什么门槛。只要把之前Docker命令中的`docker`改为`crictl`基本就可以了，果然是同一个公司出品的东西，用法都一样。所以不管K8S到底弃用不弃用Docker，对我们开发者使用来说，基本没啥影响！
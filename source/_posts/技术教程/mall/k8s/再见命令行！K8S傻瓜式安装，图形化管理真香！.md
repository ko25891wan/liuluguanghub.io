---
categories:
  - 技术教程
  - mall
  - k8s
title: 再见命令行！K8S傻瓜式安装，图形化管理真香！
date: 2024-01-21 17:20:09
tags:
---
> 之前我们一直都是使用命令行来管理 K8S 的，这种做法虽然对程序员来说看起来很炫酷，但有时候用起来还是挺麻烦的。今天我们来介绍一个 K8S 可视化管理工具 Rancher，使用它可以大大减少我们管理 K8S 的工作量，希望对大家有所帮助！

## [#](https://www.macrozheng.com/k8s/rancher_start.html#rancher%E7%AE%80%E4%BB%8B)Rancher简介

Rancher是为使用容器的公司打造的容器管理平台。Rancher简化了使用K8S的流程，开发者可以随处运行K8S，满足IT需求规范，赋能DevOps团队。

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:32_9f645aa85f243a326a02d52cc868905e.png)

## [#](https://www.macrozheng.com/k8s/rancher_start.html#docker%E5%AE%89%E8%A3%85)Docker安装

> 虽然Rancher的安装方法有好几种，但是使用Docker来安装无疑是最简单！没有安装Docker的朋友可以先安装下。

- 安装`yum-utils`：

```
yum install -y yum-utils device-mapper-persistent-data lvm2
```

- 为yum源添加docker仓库位置：

```
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

- 安装Docker：

```
yum install docker-ce
```

- 启动Docker：

```
systemctl start docker
```

## [#](https://www.macrozheng.com/k8s/rancher_start.html#rancher%E5%AE%89%E8%A3%85)Rancher安装

> 安装完Docker之后，我们就可以开始安装Rancher了。Rancher已经内置K8S，无需再额外安装。就像我们安装好Minikube一样，K8S直接就内置了。

- 首先下载Rancher镜像；

```
docker pull rancher/rancher:v2.5-head
```

- 下载完成后运行Rancher容器，Rancher运行起来有点慢需要等待几分钟：

```
docker run -p 80:80 -p 443:443 --name rancher \
--privileged \
--restart=unless-stopped \
-d rancher/rancher:v2.5-head
```

- 运行完成后就可以访问Rancher的主页了，第一次需要设置管理员账号密码，访问地址：https://192.168.5.46

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:32_639776e0558d43198d24df4e12ac904c.png)

- 设置下Rancher的`Server URL`，一个其他Node都可以访问到的地址，如果我们要安装其他Node的话需要用到它；

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:32_de5fd3d1043c34fa3f1464380950489b.png)

## [#](https://www.macrozheng.com/k8s/rancher_start.html#rancher%E4%BD%BF%E7%94%A8)Rancher使用

> 我们首先来简单使用下Rancher。

- 在首页我们可以直接查看所有集群，当前我们只有安装了Rancher的集群；

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:32_3c1f1450ade3d8eb15dd39ca5754c694.png)

- 点击集群名称可以查看集群状态信息，也可以点击右上角的按钮来执行`kubectl`命令；

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:32_139cb5177cd1e49e7e996c7373725be8.png)

- 点击仪表盘按钮，我们可以查看集群的Dashboard，这里可以查看的内容就丰富多了，Deployment、Service、Pod信息都可以查看到了。

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:32_e0931fec242dd0d2ba101bf3ef25bac7.png)

## [#](https://www.macrozheng.com/k8s/rancher_start.html#rancher%E5%AE%9E%E6%88%98)Rancher实战

> 之前我们都是使用命令行的形式操作K8S，这次我们使用图形化界面试试。还是以部署SpringBoot应用为例，不过先得部署个MySQL。

### [#](https://www.macrozheng.com/k8s/rancher_start.html#%E9%83%A8%E7%BD%B2mysql)部署MySQL

- 首先我们以`yaml`的形式创建Deployment，操作路径为`Deployments->创建->以YAML文件编辑`；

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:32_5d84f9d6f62f24f44bf6be676506fdd6.png)

- Deployment的`yaml`内容如下，注意添加`namespace: default`这行，否则会无法创建；

```
apiVersion: apps/v1
kind: Deployment
metadata:
  # 指定Deployment的名称
  name: mysql-deployment
  # 指定Deployment的空间
  namespace: default
  # 指定Deployment的标签 
  labels:
    app: mysql
spec:
  # 指定创建的Pod副本数量 
  replicas: 1
  # 定义如何查找要管理的Pod
  selector:
    # 管理标签app为mysql的Pod
    matchLabels:
      app: mysql
  # 指定创建Pod的模板
  template:
    metadata:
      # 给Pod打上app:mysql标签
      labels:
        app: mysql
    # Pod的模板规约
    spec:
      containers:
        - name: mysql
          # 指定容器镜像
          image: mysql:5.7
          # 指定开放的端口
          ports:
            - containerPort: 3306
          # 设置环境变量
          env:
            - name: MYSQL_ROOT_PASSWORD
              value: root
          # 使用存储卷
          volumeMounts:
            # 将存储卷挂载到容器内部路径
            - mountPath: /var/log/mysql
              name: log-volume
            - mountPath: /var/lib/mysql
              name: data-volume
            - mountPath: /etc/mysql
              name: conf-volume
      # 定义存储卷
      volumes:
        - name: log-volume
          # hostPath类型存储卷在宿主机上的路径
          hostPath:
            path: /home/docker/mydata/mysql/log
            # 当目录不存在时创建
            type: DirectoryOrCreate
        - name: data-volume
          hostPath:
            path: /home/docker/mydata/mysql/data
            type: DirectoryOrCreate
        - name: conf-volume
          hostPath:
            path: /home/docker/mydata/mysql/conf
            type: DirectoryOrCreate
```

- 其实我们也可以通过页面来配置Deployment的属性，如果你对`yaml`中的配置不太熟悉，可以在页面中修改属性并对照下，比如`hostPath.type`这个属性，一看就知道有哪些了；

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:32_d8e31449dec1f38c7818fbe8f82ba9c1.png)

- 之后以`yaml`的形式创建Service，操作路径为`Services->创建->节点端口->以YAML文件编辑`；

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:32_a34674dbe2b7a86af647bc221dd0857f.png)

- Service的`yaml`内容如下，`namespace`属性不能少；

```
apiVersion: v1
kind: Service
metadata:
  # 定义空间
  namespace: default
  # 定义服务名称，其他Pod可以通过服务名称作为域名进行访问
  name: mysql-service
spec:
  # 指定服务类型，通过Node上的静态端口暴露服务
  type: NodePort
  # 管理标签app为mysql的Pod
  selector:
    app: mysql
  ports:
    - name: http
      protocol: TCP
      port: 3306
      targetPort: 3306
      # Node上的静态端口
      nodePort: 30306
```

- 部署完成后需要新建`mall`数据库，并导入相关表，表地址：https://github.com/macrozheng/mall-learning/blob/master/document/sql/mall.sql
    
- 这里有个比较简单的方法来导入数据库，通过Navicat创建连接，先配置一个SSH通道；
    

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:32_9152f7947c2a0f68aa28cf3304b00be3.png)

- 接下来要获得Rancher容器运行的IP地址（在Minikube中我们使用的使用Minikube的地址）；

```
[root@linux-local ~]# docker inspect rancher |grep IPAddress
            "SecondaryIPAddresses": null,
            "IPAddress": "172.17.0.3",
                    "IPAddress": "172.17.0.3",
```

- 之后我们就可以像在Linux服务器上访问数据库一样访问Rancher中的数据库了，直接添加Rancher的IP和数据库端口即可。

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:32_d78d0b1f38df4ed36d0780a8d43cbf3d.png)

### [#](https://www.macrozheng.com/k8s/rancher_start.html#%E9%83%A8%E7%BD%B2springboot%E5%BA%94%E7%94%A8)部署SpringBoot应用

- 以`yaml`的形式创建SpringBoot应用的Deployment，操作路径为`Deployments->创建->以YAML文件编辑`，配置信息如下；

```
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: default
  name: mall-tiny-fabric-deployment
  labels:
    app: mall-tiny-fabric
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mall-tiny-fabric
  template:
    metadata:
      labels:
        app: mall-tiny-fabric
    spec:
      containers:
        - name: mall-tiny-fabric
          # 指定Docker Hub中的镜像地址
          image: macrodocker/mall-tiny-fabric:0.0.1-SNAPSHOT
          ports:
            - containerPort: 8080
          env:
            # 指定数据库连接地址
            - name: spring.datasource.url
              value: jdbc:mysql://mysql-service:3306/mall?useUnicode=true&characterEncoding=utf-8&serverTimezone=Asia/Shanghai
            # 指定日志文件路径
            - name: logging.path
              value: /var/logs
          volumeMounts:
            - mountPath: /var/logs
              name: log-volume
      volumes:
        - name: log-volume
          hostPath:
            path: /home/docker/mydata/app/mall-tiny-fabric/logs
            type: DirectoryOrCreate
```

- 以`yaml`的形式创建Service，操作路径为`Services->创建->节点端口->以YAML文件编辑`，配置信息如下；

```
apiVersion: v1
kind: Service
metadata:
  namespace: default
  name: mall-tiny-fabric-service
spec:
  type: NodePort
  selector:
    app: mall-tiny-fabric
  ports:
    - name: http
      protocol: TCP
      port: 8080
      targetPort: 8080
      # Node上的静态端口
      nodePort: 30180
```

- 创建成功后，在Deployments标签中，我们可以发现实例已经就绪了。

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:32_0b6988e98ac4ec21fbe73ad42bf77f03.png)

### [#](https://www.macrozheng.com/k8s/rancher_start.html#%E5%A4%96%E9%83%A8%E8%AE%BF%E9%97%AE%E5%BA%94%E7%94%A8)外部访问应用

> 依然使用Nginx反向代理的方式来访问SpringBoot应用。

- 由于Rancher服务已经占用了`80`端口，Nginx服务只能重新换个端口了，这里运行在`2080`端口上；

```
docker run -p 2080:2080 --name nginx \
-v /mydata/nginx/html:/usr/share/nginx/html \
-v /mydata/nginx/logs:/var/log/nginx  \
-v /mydata/nginx/conf:/etc/nginx \
-d nginx:1.10
```

- 创建完Nginx容器后，添加配置文件`mall-tiny-rancher.conf`，将`mall-tiny.macrozheng.com`域名的访问反向代理到K8S中的SpringBoot应用中去；

```
server {
    listen       2080;
    server_name  mall-tiny.macrozheng.com; #修改域名

    location / {
        proxy_set_header Host $host:$server_port;
        proxy_pass   http://172.17.0.3:30180; #修改为代理服务地址
        index  index.html index.htm;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

}
```

- 再修改访问Linux服务器的本机host文件，添加如下记录；

```
192.168.5.46 mall-tiny.macrozheng.com
```

- 之后即可直接在本机上访问K8S上的SpringBoot应用了，访问地址：http://mall-tiny.macrozheng.com:2080/swagger-ui.html

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-03_10:45:32_a8189de870f960f5075fdb50eb9feff8.png)

## [#](https://www.macrozheng.com/k8s/rancher_start.html#%E6%80%BB%E7%BB%93)总结

使用Rancher可视化管理K8S还真是简单，大大降低了K8S的部署和管理难度。一个Docker命令即可完成部署，可视化界面可以查看应用运行的各种状态。K8S脚本轻松执行，不会写脚本的图形化界面设置下也能搞定。总结一句：真香！

## [#](https://www.macrozheng.com/k8s/rancher_start.html#%E5%8F%82%E8%80%83%E8%B5%84%E6%96%99)参考资料

Rancher官方文档：https://docs.rancher.cn/rancher2/

## [#](https://www.macrozheng.com/k8s/rancher_start.html#%E9%A1%B9%E7%9B%AE%E6%BA%90%E7%A0%81%E5%9C%B0%E5%9D%80)项目源码地址

https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-fabric
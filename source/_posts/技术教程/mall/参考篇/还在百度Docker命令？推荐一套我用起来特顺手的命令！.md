---
categories:
  - 技术教程
  - mall
  - 参考篇
title: 还在百度Docker命令？推荐一套我用起来特顺手的命令！
date: 2024-01-21 17:28:34
tags:
---
> 平时经常使用 Docker 来搭建各种环境，简单又好用！但是有时候往往会忘记命令，总结了一套非常实用的 Docker 命令，对于 Java 开发来说基本上够用了，希望对大家有所帮助！

## [#](https://www.macrozheng.com/mall/reference/docker_command.html#docker%E7%AE%80%E4%BB%8B)Docker简介

Docker是一个开源的应用容器引擎，让开发者可以打包应用及依赖包到一个可移植的镜像中，然后发布到任何流行的Linux或Windows机器上。使用Docker可以更方便地打包、测试以及部署应用程序。

## [#](https://www.macrozheng.com/mall/reference/docker_command.html#docker%E7%8E%AF%E5%A2%83%E5%AE%89%E8%A3%85)Docker环境安装

- 安装`yum-utils`；

```
yum install -y yum-utils device-mapper-persistent-data lvm2
```

- 为yum源添加docker仓库位置；

```
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

- 安装docker服务；

```
yum install docker-ce
```

- 启动docker服务。

```
systemctl start docker
```

## [#](https://www.macrozheng.com/mall/reference/docker_command.html#docker%E9%95%9C%E5%83%8F%E5%B8%B8%E7%94%A8%E5%91%BD%E4%BB%A4)Docker镜像常用命令

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E6%90%9C%E7%B4%A2%E9%95%9C%E5%83%8F)搜索镜像

```
docker search java
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_01-70575cac.png)

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E4%B8%8B%E8%BD%BD%E9%95%9C%E5%83%8F)下载镜像

```
docker pull java:8
```

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E6%9F%A5%E7%9C%8B%E9%95%9C%E5%83%8F%E7%89%88%E6%9C%AC)查看镜像版本

> 由于`docker search`命令只能查找出是否有该镜像，不能找到该镜像支持的版本，所以我们需要通过`Docker Hub`来搜索支持的版本。

- 进入`Docker Hub`的官网，地址：https://hub.docker.com
    
- 然后搜索需要的镜像：
    

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_02-946ae48a.png)

- 查看镜像支持的版本：

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_03-3012a340.png)

- 进行镜像的下载操作：

```
docker pull nginx:1.17.0
```

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E5%88%97%E5%87%BA%E9%95%9C%E5%83%8F)列出镜像

```
docker images
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_04-234ab2a5.png)

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E5%88%A0%E9%99%A4%E9%95%9C%E5%83%8F)删除镜像

- 指定名称删除镜像：

```
docker rmi java:8
```

- 指定名称删除镜像（强制）：

```
docker rmi -f java:8
```

- 删除所有没有引用的镜像：

```
docker rmi `docker images | grep none | awk '{print $3}'`
```

- 强制删除所有镜像：

```
docker rmi -f $(docker images)
```

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E6%89%93%E5%8C%85%E9%95%9C%E5%83%8F)打包镜像

```
# -t 表示指定镜像仓库名称/镜像名称:镜像标签 .表示使用当前目录下的Dockerfile文件
docker build -t mall/mall-admin:1.0-SNAPSHOT .
```

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E6%8E%A8%E9%80%81%E9%95%9C%E5%83%8F)推送镜像

```
# 登录Docker Hub
docker login
# 给本地镜像打标签为远程仓库名称
docker tag mall/mall-admin:1.0-SNAPSHOT macrodocker/mall-admin:1.0-SNAPSHOT
# 推送到远程仓库
docker push macrodocker/mall-admin:1.0-SNAPSHOT
```

## [#](https://www.macrozheng.com/mall/reference/docker_command.html#docker%E5%AE%B9%E5%99%A8%E5%B8%B8%E7%94%A8%E5%91%BD%E4%BB%A4)Docker容器常用命令

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E6%96%B0%E5%BB%BA%E5%B9%B6%E5%90%AF%E5%8A%A8%E5%AE%B9%E5%99%A8)新建并启动容器

```
docker run -p 80:80 --name nginx \
-e TZ="Asia/Shanghai" \
-v /mydata/nginx/html:/usr/share/nginx/html \
-d nginx:1.17.0
```

- -p：将宿主机和容器端口进行映射，格式为：宿主机端口:容器端口；
- --name：指定容器名称，之后可以通过容器名称来操作容器；
- -e：设置容器的环境变量，这里设置的是时区；
- -v：将宿主机上的文件挂载到宿主机上，格式为：宿主机文件目录:容器文件目录；
- -d：表示容器以后台方式运行。

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E5%88%97%E5%87%BA%E5%AE%B9%E5%99%A8)列出容器

- 列出运行中的容器：

```
docker ps
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_05-31fddcd5.png)

- 列出所有容器：

```
docker ps -a
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_06-4b5fa4fc.png)

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E5%81%9C%E6%AD%A2%E5%AE%B9%E5%99%A8)停止容器

注意：`$ContainerName`表示容器名称，`$ContainerId`表示容器ID，可以使用容器名称的命令，基本也支持使用容器ID，比如下面的停止容器命令。

```
docker stop $ContainerName(or $ContainerId)
```

例如：

```
docker stop nginx
#或者
docker stop c5f5d5125587
```

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E5%BC%BA%E5%88%B6%E5%81%9C%E6%AD%A2%E5%AE%B9%E5%99%A8)强制停止容器

```
docker kill $ContainerName
```

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E5%90%AF%E5%8A%A8%E5%AE%B9%E5%99%A8)启动容器

```
docker start $ContainerName
```

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E8%BF%9B%E5%85%A5%E5%AE%B9%E5%99%A8)进入容器

- 先查询出容器的`pid`：

```
docker inspect --format "{{.State.Pid}}" $ContainerName
```

- 根据容器的pid进入容器：

```
nsenter --target "$pid" --mount --uts --ipc --net --pid
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_07-319b580d.png)

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E5%88%A0%E9%99%A4%E5%AE%B9%E5%99%A8)删除容器

- 删除指定容器：

```
docker rm $ContainerName
```

- 按名称通配符删除容器，比如删除以名称`mall-`开头的容器：

```
docker rm `docker ps -a | grep mall-* | awk '{print $1}'`
```

- 强制删除所有容器；

```
docker rm -f $(docker ps -a -q)
```

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E6%9F%A5%E7%9C%8B%E5%AE%B9%E5%99%A8%E7%9A%84%E6%97%A5%E5%BF%97)查看容器的日志

- 查看容器产生的全部日志：

```
docker logs $ContainerName
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_08-775681ca.png)

- 动态查看容器产生的日志：

```
docker logs -f $ContainerName
```

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E6%9F%A5%E7%9C%8B%E5%AE%B9%E5%99%A8%E7%9A%84ip%E5%9C%B0%E5%9D%80)查看容器的IP地址

```
docker inspect --format '{{ .NetworkSettings.IPAddress }}' $ContainerName
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_09-40b8291b.png)

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E4%BF%AE%E6%94%B9%E5%AE%B9%E5%99%A8%E7%9A%84%E5%90%AF%E5%8A%A8%E6%96%B9%E5%BC%8F)修改容器的启动方式

```
# 将容器启动方式改为always
docker container update --restart=always $ContainerName
```

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E5%90%8C%E6%AD%A5%E5%AE%BF%E4%B8%BB%E6%9C%BA%E6%97%B6%E9%97%B4%E5%88%B0%E5%AE%B9%E5%99%A8)同步宿主机时间到容器

```
docker cp /etc/localtime $ContainerName:/etc/
```

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E6%8C%87%E5%AE%9A%E5%AE%B9%E5%99%A8%E6%97%B6%E5%8C%BA)指定容器时区

```
docker run -p 80:80 --name nginx \
-e TZ="Asia/Shanghai" \
-d nginx:1.17.0
```

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E6%9F%A5%E7%9C%8B%E5%AE%B9%E5%99%A8%E8%B5%84%E6%BA%90%E5%8D%A0%E7%94%A8%E7%8A%B6%E5%86%B5)查看容器资源占用状况

- 查看指定容器资源占用状况，比如cpu、内存、网络、io状态：

```
docker stats $ContainerName
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_10-3f2058d6.png)

- 查看所有容器资源占用情况：

```
docker stats -a
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_11-43f2f10b.png)

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E6%9F%A5%E7%9C%8B%E5%AE%B9%E5%99%A8%E7%A3%81%E7%9B%98%E4%BD%BF%E7%94%A8%E6%83%85%E5%86%B5)查看容器磁盘使用情况

```
docker system df
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_16-dcbbacfe.png)

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E6%89%A7%E8%A1%8C%E5%AE%B9%E5%99%A8%E5%86%85%E9%83%A8%E5%91%BD%E4%BB%A4)执行容器内部命令

```
docker exec -it $ContainerName /bin/bash
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_12-879b8c3a.png)

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E6%8C%87%E5%AE%9A%E8%B4%A6%E5%8F%B7%E8%BF%9B%E5%85%A5%E5%AE%B9%E5%99%A8%E5%86%85%E9%83%A8)指定账号进入容器内部

```
# 使用root账号进入容器内部
docker exec -it --user root $ContainerName /bin/bash
```

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E6%9F%A5%E7%9C%8B%E6%89%80%E6%9C%89%E7%BD%91%E7%BB%9C)查看所有网络

```
docker network ls
```

```
[root@local-linux ~]# docker network ls
NETWORK ID          NAME                     DRIVER              SCOPE
59b309a5c12f        bridge                   bridge              local
ef34fe69992b        host                     host                local
a65be030c632        none     
```

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E5%88%9B%E5%BB%BA%E5%A4%96%E9%83%A8%E7%BD%91%E7%BB%9C)创建外部网络

```
docker network create -d bridge my-bridge-network
```

### [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E6%8C%87%E5%AE%9A%E5%AE%B9%E5%99%A8%E7%BD%91%E7%BB%9C)指定容器网络

```
docker run -p 80:80 --name nginx \
--network my-bridge-network \
-d nginx:1.17.0
```

## [#](https://www.macrozheng.com/mall/reference/docker_command.html#%E4%BF%AE%E6%94%B9%E9%95%9C%E5%83%8F%E7%9A%84%E5%AD%98%E6%94%BE%E4%BD%8D%E7%BD%AE)修改镜像的存放位置

- 查看Docker镜像的存放位置：

```
docker info | grep "Docker Root Dir"
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_13-ad1c2e8f.png)

- 关闭Docker服务：

```
systemctl stop docker
```

- 先将原镜像目录移动到目标目录：

```
mv /var/lib/docker /mydata/docker
```

- 建立软连接：

```
ln -s /mydata/docker /var/lib/docker
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_14-7ed12cac.png)

- 再次查看可以发现镜像存放位置已经更改。

![](https://qiniu.ko25891wan.top/日记软件/obsition/docker_command_15-03abae7d.png)

## [#](https://www.macrozheng.com/mall/reference/docker_command.html#docker%E5%AE%B9%E5%99%A8%E6%B8%85%E7%90%86)Docker容器清理

- 查看Docker占用的磁盘空间情况：

```
docker system df
```

- 删除所有关闭的容器：

```
docker ps -a | grep Exit | cut -d ' ' -f 1 | xargs docker rm
```

- 删除所有`dangling`镜像(没有Tag的镜像)：

```
docker rmi $(docker images | grep "^<none>" | awk "{print $3}")
```

- 删除所有`dangling`数据卷(即无用的 volume)：

```
docker volume rm $(docker volume ls -qf dangling=true)
```
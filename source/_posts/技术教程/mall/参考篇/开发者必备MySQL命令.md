---
categories:
  - 技术教程
  - mall
  - 参考篇
title: 开发者必备MySQL命令
date: 2024-01-21 17:28:47
tags:
---
> 开发者必备 Mysql 常用命令，涵盖了数据定义语句、数据操纵语句及数据控制语句，基于 Mysql5.7。

## [#](https://www.macrozheng.com/mall/reference/mysql.html#%E6%95%B0%E6%8D%AE%E5%AE%9A%E4%B9%89%E8%AF%AD%E5%8F%A5-ddl)数据定义语句(DDL)

### [#](https://www.macrozheng.com/mall/reference/mysql.html#%E6%95%B0%E6%8D%AE%E5%BA%93%E6%93%8D%E4%BD%9C)数据库操作

- 登录数据库：

```
mysql -uroot -proot
```

- 创建数据库：

```
create database test
```

- 查看所有数据库：

```
show databases
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/refer_screen_41-f37d4d3a.png)

- 选择数据库并使用：

```
use test
```

- 查看所有数据表：

```
show tables
```

- 删除数据库：

```
drop database test
```

### [#](https://www.macrozheng.com/mall/reference/mysql.html#%E8%A1%A8%E6%93%8D%E4%BD%9C)表操作

- 创建表：

```
create table emp(ename varchar(10),hiredate date,sal decimal(10,2),deptno int(2))  
```

```
create table dept(deptno int(2),deptname varchar(10))
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/refer_screen_42-940b3b71.png)

- 查看表的定义：

```
desc emp
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/refer_screen_43-1ede5291.png)

- 查看表定义（详细）：

```
show create table emp \G
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/refer_screen_44-79ca2830.png)

- 删除表：

```
drop table emp
```

- 修改表字段：

```
alter table emp modify ename varchar(20)
```

- 添加表字段：

```
alter table emp add column age int(3)
```

- 删除表字段：

```
alter table emp drop column age
```

- 字段改名；

```
alter table emp change age age1 int(4)
```

- 修改表名：

```
alter table emp rename emp1
```

## [#](https://www.macrozheng.com/mall/reference/mysql.html#%E6%95%B0%E6%8D%AE%E6%93%8D%E7%BA%B5%E8%AF%AD%E5%8F%A5-dml)数据操纵语句(DML)

### [#](https://www.macrozheng.com/mall/reference/mysql.html#%E6%8F%92%E5%85%A5%E8%AE%B0%E5%BD%95)插入记录

- 指定名称插入：

```
insert into emp (ename,hiredate,sal,deptno) values ('zhangsan','2018-01-01','2000',1)
```

- 不指定名称插入：

```
insert into emp values ('lisi','2018-01-01','2000',1)
```

- 批量插入数据：

```
insert into dept values(1,'dept1'),(2,'dept2')
```

### [#](https://www.macrozheng.com/mall/reference/mysql.html#%E4%BF%AE%E6%94%B9%E8%AE%B0%E5%BD%95)修改记录

```
update emp set sal='4000',deptno=2 where ename='zhangsan'
```

### [#](https://www.macrozheng.com/mall/reference/mysql.html#%E5%88%A0%E9%99%A4%E8%AE%B0%E5%BD%95)删除记录

```
delete from emp where ename='zhangsan'
```

### [#](https://www.macrozheng.com/mall/reference/mysql.html#%E6%9F%A5%E8%AF%A2%E8%AE%B0%E5%BD%95)查询记录

- 查询所有记录：

```
select * from emp
```

- 查询不重复的记录：

```
select distinct deptno from emp
```

- 条件查询：

```
select * from emp where deptno=1 and sal<3000
```

- 排序和限制：

```
select * from emp order by deptno desc limit 2
```

- 分页查询(查询从第0条记录开始10条)：

```
select * from emp order by deptno desc limit 0,10
```

- 聚合(查询部门人数大于1的部门编号)：

```
select deptno,count(1) from emp group by deptno having count(1) > 1
```

- 连接查询：

```
select * from emp e left join dept d on e.deptno=d.deptno
```

- 子查询：

```
select * from emp where deptno in (select deptno from dept)
```

- 记录联合：

```
select deptno from emp union select deptno from dept
```

## [#](https://www.macrozheng.com/mall/reference/mysql.html#%E6%95%B0%E6%8D%AE%E6%8E%A7%E5%88%B6%E8%AF%AD%E5%8F%A5-dcl)数据控制语句(DCL)

### [#](https://www.macrozheng.com/mall/reference/mysql.html#%E6%9D%83%E9%99%90%E7%9B%B8%E5%85%B3)权限相关

- 授予操作权限(将test数据库中所有表的select和insert权限授予test用户)：

```
grant select,insert on test.* to 'test'@'localhost' identified by '123'
```

- 查看账号权限：

```
show grants for 'test'@'localhost'
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/refer_screen_45-072633db.png)

- 收回操作权限：

```
revoke insert on test.* from 'test'@'localhost'
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/refer_screen_46-9c6f36a5.png)

- 授予所有数据库的所有权限：

```
grant all privileges on *.* to 'test'@'localhost'
```

- 授予所有数据库的所有权限(包括grant)：

```
grant all privileges on *.* to 'test'@'localhost' with grant option
```

- 授予SUPER PROCESS FILE权限（系统权限不能指定数据库）：

```
grant super,process,file on *.* to 'test'@'localhost'
```

- 只授予登录权限：

```
grant usage on *.* to 'test'@'localhost'
```

### [#](https://www.macrozheng.com/mall/reference/mysql.html#%E5%B8%90%E5%8F%B7%E7%9B%B8%E5%85%B3)帐号相关

- 删除账号：

```
drop user 'test'@'localhost'
```

- 修改自己的密码：

```
set password = password('123')
```

- 管理员修改他人密码：

```
set password for 'test'@'localhost' = password('123')
```

## [#](https://www.macrozheng.com/mall/reference/mysql.html#%E5%85%B6%E4%BB%96)其他

### [#](https://www.macrozheng.com/mall/reference/mysql.html#%E5%AD%97%E7%AC%A6%E9%9B%86%E7%9B%B8%E5%85%B3)字符集相关

- 查看字符集：

```
show variables like 'character%'
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/refer_screen_47-495dee0a.png)

- 创建数据库时指定字符集：

```
create database test2 character set utf8
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/refer_screen_48-a08408d7.png)

### [#](https://www.macrozheng.com/mall/reference/mysql.html#%E6%97%B6%E5%8C%BA%E7%9B%B8%E5%85%B3)时区相关

- 查看当前时区（UTC为世界统一时间，中国为UTC+8）：

```
show variables like "%time_zone%"
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/refer_screen_49-3441424e.png)

- 修改mysql全局时区为北京时间，即我们所在的东8区：

```
set global time_zone = '+8:00';
```

- 修改当前会话时区：

```
set time_zone = '+8:00'
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/refer_screen_50-f4888b63.png)

- 立即生效：

```
flush privileges
```
---
categories:
  - 技术教程
  - mall
  - 参考篇
title: 使用Dockerfile为SpringBoot应用构建Docker镜像
date: 2024-01-21 17:28:57
tags:
---
> 上次写过一篇[使用Maven插件构建Docker镜像](https://www.macrozheng.com/mall/reference/docker_maven.html) ，讲述了通过 docker-maven-plugin 来构建 docker 镜像的方式，此种方式需要依赖自建的 Registry 镜像仓库。本文将讲述另一种方式，使用 Dockerfile 来构建 docker 镜像，此种方式不需要依赖自建的镜像仓库，只需要应用的 jar 包和一个 Dockerfile 文件即可。

## [#](https://www.macrozheng.com/mall/reference/docker_file.html#dockerfile%E5%B8%B8%E7%94%A8%E6%8C%87%E4%BB%A4)Dockerfile常用指令

### [#](https://www.macrozheng.com/mall/reference/docker_file.html#add)ADD

用于复制文件，格式：

```
ADD <src> <dest>
```

示例：

```
# 将当前目录下的mall-tiny-docker-file.jar包复制到docker容器的/目录下
ADD mall-tiny-docker-file.jar /mall-tiny-docker-file.jar
```

### [#](https://www.macrozheng.com/mall/reference/docker_file.html#entrypoint)ENTRYPOINT

指定docker容器启动时执行的命令，格式：

```
ENTRYPOINT ["executable", "param1","param2"...]
```

示例：

```
# 指定docker容器启动时运行jar包
ENTRYPOINT ["java", "-jar","/mall-tiny-docker-file.jar"]
```

### [#](https://www.macrozheng.com/mall/reference/docker_file.html#env)ENV

用于设置环境变量，格式：

```
ENV <key> <value>
```

示例：

```
# mysql运行时设置root密码
ENV MYSQL_ROOT_PASSWORD root
```

### [#](https://www.macrozheng.com/mall/reference/docker_file.html#expose)EXPOSE

声明需要暴露的端口(只声明不会打开端口)，格式：

```
EXPOSE <port1> <port2> ...
```

示例：

```
# 声明服务运行在8080端口
EXPOSE 8080
```

### [#](https://www.macrozheng.com/mall/reference/docker_file.html#from)FROM

指定所需依赖的基础镜像，格式：

```
FROM <image>:<tag>
```

示例：

```
# 该镜像需要依赖的java8的镜像
FROM java:8
```

### [#](https://www.macrozheng.com/mall/reference/docker_file.html#maintainer)MAINTAINER

指定维护者的名字，格式：

```
MAINTAINER <name>
```

示例：

```
MAINTAINER macrozheng
```

### [#](https://www.macrozheng.com/mall/reference/docker_file.html#run)RUN

在容器构建过程中执行的命令，我们可以用该命令自定义容器的行为，比如安装一些软件，创建一些文件等，格式：

```
RUN <command>
RUN ["executable", "param1","param2"...]
```

示例：

```
# 在容器构建过程中需要在/目录下创建一个mall-tiny-docker-file.jar文件
RUN bash -c 'touch /mall-tiny-docker-file.jar'
```

## [#](https://www.macrozheng.com/mall/reference/docker_file.html#%E4%BD%BF%E7%94%A8dockerfile%E6%9E%84%E5%BB%BAspringboot%E5%BA%94%E7%94%A8%E9%95%9C%E5%83%8F)使用Dockerfile构建SpringBoot应用镜像

### [#](https://www.macrozheng.com/mall/reference/docker_file.html#%E7%BC%96%E5%86%99dockerfile%E6%96%87%E4%BB%B6)编写Dockerfile文件

```
# 该镜像需要依赖的基础镜像
FROM java:8
# 将当前目录下的jar包复制到docker容器的/目录下
ADD mall-tiny-docker-file-0.0.1-SNAPSHOT.jar /mall-tiny-docker-file.jar
# 运行过程中创建一个mall-tiny-docker-file.jar文件
RUN bash -c 'touch /mall-tiny-docker-file.jar'
# 声明服务运行在8080端口
EXPOSE 8080
# 指定docker容器启动时运行jar包
ENTRYPOINT ["java", "-jar","/mall-tiny-docker-file.jar"]
# 指定维护者的名字
MAINTAINER macrozheng
```

### [#](https://www.macrozheng.com/mall/reference/docker_file.html#%E4%BD%BF%E7%94%A8maven%E6%89%93%E5%8C%85%E5%BA%94%E7%94%A8)使用maven打包应用

在IDEA中双击package命令进行打包:  
![](https://qiniu.ko25891wan.top/日记软件/obsition/refer_screen_91-589450b5.png)  
打包成功后展示：

```
[INFO] --- spring-boot-maven-plugin:2.1.3.RELEASE:repackage (repackage) @ mall-tiny-docker-file ---
[INFO] Replacing main artifact with repackaged archive
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 30.749 s
[INFO] Finished at: 2019-06-16T14:11:07+08:00
[INFO] Final Memory: 43M/306M
[INFO] ------------------------------------------------------------------------
```

将应用jar包及Dockerfile文件上传到linux服务器： ![](https://qiniu.ko25891wan.top/日记软件/obsition/refer_screen_93-b0799213.png)

### [#](https://www.macrozheng.com/mall/reference/docker_file.html#%E5%9C%A8linux%E4%B8%8A%E6%9E%84%E5%BB%BAdocker%E9%95%9C%E5%83%8F)在Linux上构建docker镜像

在Dockerfile所在目录执行以下命令：

```
# -t 表示指定镜像仓库名称/镜像名称:镜像标签 .表示使用当前目录下的Dockerfile
docker build -t mall-tiny/mall-tiny-docker-file:0.0.1-SNAPSHOT .
```

输出如下信息：

```
Sending build context to Docker daemon  36.37MB
Step 1/5 : FROM java:8
 ---> d23bdf5b1b1b
Step 2/5 : ADD mall-tiny-docker-file-0.0.1-SNAPSHOT.jar /mall-tiny-docker-file.jar
 ---> c920c9e9d045
Step 3/5 : RUN bash -c 'touch /mall-tiny-docker-file.jar'
 ---> Running in 55506f517f19
Removing intermediate container 55506f517f19
 ---> 0727eded66dc
Step 4/5 : EXPOSE 8080
 ---> Running in d67a5f50aa7d
Removing intermediate container d67a5f50aa7d
 ---> 1b8b4506eb2d
Step 5/5 : ENTRYPOINT ["java", "-jar","/mall-tiny-docker-file.jar"]
 ---> Running in 0c5bf61a0032
Removing intermediate container 0c5bf61a0032
 ---> c3614dad21b7
Successfully built c3614dad21b7
Successfully tagged mall-tiny/mall-tiny-docker-file:0.0.1-SNAPSHOT
```

查看docker镜像： ![](https://qiniu.ko25891wan.top/日记软件/obsition/refer_screen_94-107702d4.png)

### [#](https://www.macrozheng.com/mall/reference/docker_file.html#%E8%BF%90%E8%A1%8Cmysql%E6%9C%8D%E5%8A%A1%E5%B9%B6%E8%AE%BE%E7%BD%AE)运行mysql服务并设置

#### [#](https://www.macrozheng.com/mall/reference/docker_file.html#_1-%E4%BD%BF%E7%94%A8docker%E5%91%BD%E4%BB%A4%E5%90%AF%E5%8A%A8)1.使用docker命令启动：

```
docker run -p 3306:3306 --name mysql \
-v /mydata/mysql/log:/var/log/mysql \
-v /mydata/mysql/data:/var/lib/mysql \
-v /mydata/mysql/conf:/etc/mysql \
-e MYSQL_ROOT_PASSWORD=root  \
-d mysql:5.7
```

#### [#](https://www.macrozheng.com/mall/reference/docker_file.html#_2-%E8%BF%9B%E5%85%A5%E8%BF%90%E8%A1%8Cmysql%E7%9A%84docker%E5%AE%B9%E5%99%A8)2.进入运行mysql的docker容器：

```
docker exec -it mysql /bin/bash
```

#### [#](https://www.macrozheng.com/mall/reference/docker_file.html#_3-%E4%BD%BF%E7%94%A8mysql%E5%91%BD%E4%BB%A4%E6%89%93%E5%BC%80%E5%AE%A2%E6%88%B7%E7%AB%AF)3.使用mysql命令打开客户端：

```
mysql -uroot -proot --default-character-set=utf8
```

#### [#](https://www.macrozheng.com/mall/reference/docker_file.html#_4-%E4%BF%AE%E6%94%B9root%E5%B8%90%E5%8F%B7%E7%9A%84%E6%9D%83%E9%99%90-%E4%BD%BF%E5%BE%97%E4%BB%BB%E4%BD%95ip%E9%83%BD%E8%83%BD%E8%AE%BF%E9%97%AE)4.修改root帐号的权限，使得任何ip都能访问：

```
grant all privileges on *.* to 'root'@'%'
```

#### [#](https://www.macrozheng.com/mall/reference/docker_file.html#_5-%E5%88%9B%E5%BB%BAmall%E6%95%B0%E6%8D%AE%E5%BA%93)5.创建mall数据库：

```
create database mall character set utf8
```

#### [#](https://www.macrozheng.com/mall/reference/docker_file.html#_6-%E5%B0%86mall-sql%E6%96%87%E4%BB%B6%E6%8B%B7%E8%B4%9D%E5%88%B0mysql%E5%AE%B9%E5%99%A8%E7%9A%84-%E7%9B%AE%E5%BD%95%E4%B8%8B)6.将mall.sql文件拷贝到mysql容器的/目录下：

```
docker cp /mydata/mall.sql mysql:/
```

#### [#](https://www.macrozheng.com/mall/reference/docker_file.html#_7-%E5%B0%86sql%E6%96%87%E4%BB%B6%E5%AF%BC%E5%85%A5%E5%88%B0%E6%95%B0%E6%8D%AE%E5%BA%93)7.将sql文件导入到数据库：

```
use mall;
source /mall.sql;
```

### [#](https://www.macrozheng.com/mall/reference/docker_file.html#%E8%BF%90%E8%A1%8Cmall-tiny-docker-file%E5%BA%94%E7%94%A8)运行mall-tiny-docker-file应用

```
docker run -p 8080:8080 --name mall-tiny-docker-file \
--link mysql:db \
-v /etc/localtime:/etc/localtime \
-v /mydata/app/mall-tiny-docker-file/logs:/var/logs \
-d mall-tiny/mall-tiny-docker-file:0.0.1-SNAPSHOT
```

访问接口文档地址http://192.168.3.101:8080/swagger-ui.html： ![](undefined)

## [#](https://www.macrozheng.com/mall/reference/docker_file.html#%E9%A1%B9%E7%9B%AE%E6%BA%90%E7%A0%81%E5%9C%B0%E5%9D%80)项目源码地址

[https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-docker-file](https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-docker-file)
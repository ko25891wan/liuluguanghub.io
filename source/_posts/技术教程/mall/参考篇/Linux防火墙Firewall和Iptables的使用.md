---
categories:
  - 技术教程
  - mall
  - 参考篇
title: Linux防火墙Firewall和Iptables的使用
date: 2024-01-21 17:29:18
tags:
---
> Linux 中有两种防火墙软件，ConterOS7.0以上使用的是 firewall，ConterOS7.0以下使用的是 iptables，本文将分别介绍两种防火墙软件的使用。

## [#](https://www.macrozheng.com/mall/reference/linux_firewall.html#firewall)Firewall

- 开启防火墙：

```
systemctl start firewalld
```

- 关闭防火墙：

```
systemctl stop firewalld
```

- 查看防火墙状态：

```
systemctl status firewalld
```

- 设置开机启动：

```
systemctl enable firewalld
```

- 禁用开机启动：

```
systemctl disable firewalld
```

- 重启防火墙：

```
firewall-cmd --reload
```

- 开放端口（修改后需要重启防火墙方可生效）：

```
firewall-cmd --zone=public --add-port=8080/tcp --permanent
```

![image.png](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:47:17_20240202224715.png)


- 查看开放的端口：

```
firewall-cmd --list-ports
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:46:47_2024-02-02_22:46:31_refer_screen_32-db46bfbd.png)

- 关闭端口：

```
firewall-cmd --zone=public --remove-port=8080/tcp --permanent
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:46:47_2024-02-02_22:46:31_refer_screen_33-ef2e6631.png)

## [#](https://www.macrozheng.com/mall/reference/linux_firewall.html#iptables)Iptables

### [#](https://www.macrozheng.com/mall/reference/linux_firewall.html#%E5%AE%89%E8%A3%85)安装

> 由于CenterOS7.0以上版本并没有预装Iptables,我们需要自行装。

- 安装前先关闭firewall防火墙 ![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:46:47_2024-02-02_22:46:31_refer_screen_34-3c43ff7a.png)
- 安装iptables:

```
yum install iptables
```

- 安装iptables-services:

```
yum install iptables-services
```

### [#](https://www.macrozheng.com/mall/reference/linux_firewall.html#%E4%BD%BF%E7%94%A8)使用

- 开启防火墙：

```
systemctl start iptables.service
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:46:47_2024-02-02_22:46:31_refer_screen_35-5cb20a0f.png)

- 关闭防火墙：

```
systemctl stop iptables.service
```

- 查看防火墙状态：

```
systemctl status iptables.service
```

- 设置开机启动：

```
systemctl enable iptables.service
```

- 禁用开机启动：

```
systemctl disable iptables.service
```

- 查看filter表的几条链规则(INPUT链可以看出开放了哪些端口)：

```
iptables -L -n
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:46:47_2024-02-02_22:46:31_refer_screen_36-5a3784e0.png)

- 查看NAT表的链规则：

```
iptables -t nat -L -n
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:46:47_2024-02-02_22:46:31_refer_screen_37-40e17e18.png)

- 清除防火墙所有规则：

```
iptables -F
```

```
iptables -X
```

```
iptables -Z
```

- 给INPUT链添加规则（开放8080端口）：

```
iptables -I INPUT -p tcp --dport 8080 -j ACCEPT
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:46:47_2024-02-02_22:46:31_refer_screen_38-a2d19813.png)

- 查找规则所在行号：

```
iptables -L INPUT --line-numbers -n
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:46:47_2024-02-02_22:46:31_refer_screen_39-2db36c06.png)

- 根据行号删除过滤规则（关闭8080端口）：

```
iptables -D INPUT 1
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:46:47_2024-02-02_22:46:31_refer_screen_40-6651ba15.png)
---
categories:
  - 技术教程
  - mall
  - 参考篇
title: 使用Jenkins一键打包部署前端应用，就是这么6！
date: 2024-01-21 17:29:00
tags:
---
> 上一次我们讲到了使用 Jenkins 一键打包部署 SpringBoot 应用，这一次我们来讲下如何一键打包部署前端应用，以 Vue 前端应用为例，这里我们使用 `mall-admin-web` 中的代码来进行演示。

## [#](https://www.macrozheng.com/mall/reference/jenkins_vue.html#%E5%AD%A6%E5%89%8D%E5%87%86%E5%A4%87)学前准备

> 学习本文需要一些Jenkins和Nginx的知识，对这些不熟悉的小伙伴可以参考以下文章。

- [使用Jenkins一键打包部署SpringBoot应用，就是这么6！](https://www.macrozheng.com/mall/reference/jenkins.html)
- [Nginx的这些妙用，你肯定有不知道的！](https://www.macrozheng.com/mall/reference/nginx.html)

## [#](https://www.macrozheng.com/mall/reference/jenkins_vue.html#jenkins%E4%B8%AD%E7%9A%84%E8%87%AA%E5%8A%A8%E5%8C%96%E9%83%A8%E7%BD%B2)Jenkins中的自动化部署

> Vue前端应用的打包需要依赖NodeJS插件，所以我们先安装并配置该插件，然后创建任务来打包部署。

### [#](https://www.macrozheng.com/mall/reference/jenkins_vue.html#%E5%AE%89%E8%A3%85nodejs%E6%8F%92%E4%BB%B6)安装NodeJS插件

- 在系统设置->插件管理中选择安装插件；

![](https://qiniu.ko25891wan.top/日记软件/obsition/jenkins_vue_01-7445ea4d.png)

- 搜索`NodeJS`插件并进行安装；

![](https://qiniu.ko25891wan.top/日记软件/obsition/jenkins_vue_02-aaaf023f.png)

### [#](https://www.macrozheng.com/mall/reference/jenkins_vue.html#%E9%85%8D%E7%BD%AEnodejs%E6%8F%92%E4%BB%B6)配置NodeJS插件

- 在系统设置->全局工具配置中进行插件配置；

![](https://qiniu.ko25891wan.top/日记软件/obsition/jenkins_vue_03-4db29433.png)

- 选择`新增NodeJS`，配置好版本号以后，点击保存即可完成设置；

![](https://qiniu.ko25891wan.top/日记软件/obsition/jenkins_vue_04-3dffc85f.png)

### [#](https://www.macrozheng.com/mall/reference/jenkins_vue.html#%E5%88%9B%E5%BB%BA%E4%BB%BB%E5%8A%A1)创建任务

> 我们需要创建一个任务来打包部署我们的前端应用，这里以我的`mall-admin-web`项目为例。

- 任务执行流程如下：

![](https://qiniu.ko25891wan.top/日记软件/obsition/jenkins_vue_11-16ffe9c3.png)

- 构建一个自由风格的软件项目：

![](https://qiniu.ko25891wan.top/日记软件/obsition/jenkins_vue_05-3bba4e16.png)

- 在源码管理中添加Git代码仓库相关配置，这里我使用的Gitee上面的代码，地址为：https://gitee.com/macrozheng/mall-admin-web

![](https://qiniu.ko25891wan.top/日记软件/obsition/jenkins_vue_06-57a8375b.png)

- 在构建环境中把我们的`node`环境添加进去：

![](https://qiniu.ko25891wan.top/日记软件/obsition/jenkins_vue_07-6c389087.png)

- 添加一个`执行shell`的构建，用于将我们的前端代码进行编译打包：

![](https://qiniu.ko25891wan.top/日记软件/obsition/jenkins_vue_08-c2e947e0.png)

- 构建脚本如下：

```
# 查看版本信息
npm -v
# 解决存放在Github上的sass无法下载的问题
SASS_BINARY_SITE=https://npm.taobao.org/mirrors/node-sass/ npm install node-sass
# 将镜像源替换为淘宝的加速访问
npm config set registry https://registry.npm.taobao.org
# 安装项目依赖
npm install
# 项目打包
npm run build
```

- 添加一个`使用ssh执行远程脚本`的构建，用于将我们打包后的代码发布到Nginx中去：

![](https://qiniu.ko25891wan.top/日记软件/obsition/jenkins_vue_09-65ac20df.png)

- 远程执行脚本如下：

```
docker stop nginx
echo '----stop nginx----'
rm -rf /mydata/nginx/html
echo '----rm html dir----'
cp -r /mydata/jenkins_home/workspace/mall-admin-web/dist /mydata/nginx/html
echo '----cp dist dir to html dir----'
docker start nginx
echo '----start nginx----'
```

- 点击保存后，直接在任务列表中点击运行即可完成自动化部署：

![](https://qiniu.ko25891wan.top/日记软件/obsition/jenkins_vue_10-ca62f3c8.png)

## [#](https://www.macrozheng.com/mall/reference/jenkins_vue.html#%E9%81%87%E5%88%B0%E7%9A%84%E5%9D%91)遇到的坑

### [#](https://www.macrozheng.com/mall/reference/jenkins_vue.html#node-sass%E6%97%A0%E6%B3%95%E4%B8%8B%E8%BD%BD%E5%AF%BC%E8%87%B4%E6%9E%84%E5%BB%BA%E5%A4%B1%E8%B4%A5)node-sass无法下载导致构建失败

由于node-sass的源使用的是Github上面的，经常无法访问，我们构建的时候需要单独设置node-sass的下载地址。

```
# linux
SASS_BINARY_SITE=https://npm.taobao.org/mirrors/node-sass/ npm install node-sass
# window
set SASS_BINARY_SITE=https://npm.taobao.org/mirrors/node-sass&& npm install node-sass
```

### [#](https://www.macrozheng.com/mall/reference/jenkins_vue.html#%E6%9C%89%E4%BA%9B%E4%BE%9D%E8%B5%96%E6%97%A0%E6%B3%95%E4%B8%8B%E8%BD%BD%E5%AF%BC%E8%87%B4%E6%9E%84%E5%BB%BA%E5%A4%B1%E8%B4%A5)有些依赖无法下载导致构建失败

由于npm源访问慢的问题，有些源可能会无法下载，改用淘宝的npm源即可解决。

```
# 设置为淘宝的镜像源
npm config set registry https://registry.npm.taobao.org
# 设置为官方镜像源
npm config set registry https://registry.npmjs.org
```

## [#](https://www.macrozheng.com/mall/reference/jenkins_vue.html#%E9%A1%B9%E7%9B%AE%E5%9C%B0%E5%9D%80)项目地址

[https://github.com/macrozheng/mall-admin-web](https://github.com/macrozheng/mall-admin-web)
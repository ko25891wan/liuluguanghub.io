---
categories:
  - 技术教程
  - mall
  - 参考篇
title: SpringBoot应用整合ELK实现日志收集
date: 2024-01-21 17:30:08
tags:
---
> ELK 即 Elasticsearch、Logstash、Kibana,组合起来可以搭建线上日志系统，本文主要讲解使用 ELK 来收集 SpringBoot 应用产生的日志。

## [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E5%AD%A6%E4%B9%A0%E5%89%8D%E9%9C%80%E8%A6%81%E4%BA%86%E8%A7%A3%E7%9A%84%E5%86%85%E5%AE%B9)学习前需要了解的内容

- [开发者必备Docker命令](https://www.macrozheng.com/mall/reference/docker_command.html)
- [使用Docker Compose部署SpringBoot应用](https://www.macrozheng.com/mall/reference/docker_compose.html)
- [SpringBoot应用中使用AOP记录接口访问日志](https://www.macrozheng.com/mall/reference/aop_log.html)

## [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#elk%E4%B8%AD%E5%90%84%E4%B8%AA%E6%9C%8D%E5%8A%A1%E7%9A%84%E4%BD%9C%E7%94%A8)ELK中各个服务的作用

- Elasticsearch:用于存储收集到的日志信息；
- Logstash:用于收集日志，SpringBoot应用整合了Logstash以后会把日志发送给Logstash,Logstash再把日志转发给Elasticsearch；
- Kibana:通过Web端的可视化界面来查看日志。

## [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E4%BD%BF%E7%94%A8docker-compose-%E6%90%AD%E5%BB%BAelk%E7%8E%AF%E5%A2%83)使用Docker Compose 搭建ELK环境

### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E9%9C%80%E8%A6%81%E4%B8%8B%E8%BD%BD%E7%9A%84docker%E9%95%9C%E5%83%8F)需要下载的Docker镜像

```
docker pull elasticsearch:6.4.0
docker pull logstash:6.4.0
docker pull kibana:6.4.0
```

### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E6%90%AD%E5%BB%BA%E5%89%8D%E5%87%86%E5%A4%87)搭建前准备

- elasticsearch 启动成功需要特殊配置，具体参考[mall在Linux环境下的部署（基于Docker Compose）open in new window](https://mp.weixin.qq.com/s/JYkvdub9DP5P9ULX4mehUw)中的elasticsearch部分；
- docker-compose.yml文件地址:https://github.com/macrozheng/mall-learning/blob/master/mall-tiny-elk/src/main/docker/docker-compose.yml
- logstash-springboot.conf配置文件地址：https://github.com/macrozheng/mall-learning/blob/master/mall-tiny-elk/src/main/docker/logstash-springboot.conf

### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E5%BC%80%E5%A7%8B%E6%90%AD%E5%BB%BA)开始搭建

#### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E5%88%9B%E5%BB%BA%E4%B8%80%E4%B8%AA%E5%AD%98%E6%94%BElogstash%E9%85%8D%E7%BD%AE%E7%9A%84%E7%9B%AE%E5%BD%95%E5%B9%B6%E4%B8%8A%E4%BC%A0%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6)创建一个存放logstash配置的目录并上传配置文件

##### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#logstash-springboot-conf%E6%96%87%E4%BB%B6%E5%86%85%E5%AE%B9)logstash-springboot.conf文件内容

```
input {
  tcp {
    mode => "server"
    host => "0.0.0.0"
    port => 4560
    codec => json_lines
  }
}
output {
  elasticsearch {
    hosts => "es:9200"
    index => "springboot-logstash-%{+YYYY.MM.dd}"
  }
}
```

##### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E5%88%9B%E5%BB%BA%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6%E5%AD%98%E6%94%BE%E7%9B%AE%E5%BD%95%E5%B9%B6%E4%B8%8A%E4%BC%A0%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6%E5%88%B0%E8%AF%A5%E7%9B%AE%E5%BD%95)创建配置文件存放目录并上传配置文件到该目录

```
mkdir /mydata/logstash
```

#### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E4%BD%BF%E7%94%A8docker-compose-yml%E8%84%9A%E6%9C%AC%E5%90%AF%E5%8A%A8elk%E6%9C%8D%E5%8A%A1)使用docker-compose.yml脚本启动ELK服务

##### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#docker-compose-yml%E5%86%85%E5%AE%B9)docker-compose.yml内容

```
version: '3'
services:
  elasticsearch:
    image: elasticsearch:6.4.0
    container_name: elasticsearch
    environment:
      - "cluster.name=elasticsearch" #设置集群名称为elasticsearch
      - "discovery.type=single-node" #以单一节点模式启动
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m" #设置使用jvm内存大小
    volumes:
      - /mydata/elasticsearch/plugins:/usr/share/elasticsearch/plugins #插件文件挂载
      - /mydata/elasticsearch/data:/usr/share/elasticsearch/data #数据文件挂载
    ports:
      - 9200:9200
      - 9300:9300
  kibana:
    image: kibana:6.4.0
    container_name: kibana
    links:
      - elasticsearch:es #可以用es这个域名访问elasticsearch服务
    depends_on:
      - elasticsearch #kibana在elasticsearch启动之后再启动
    environment:
      - "elasticsearch.hosts=http://es:9200" #设置访问elasticsearch的地址
    ports:
      - 5601:5601
  logstash:
    image: logstash:6.4.0
    container_name: logstash
    volumes:
      - /mydata/logstash/logstash-springboot.conf:/usr/share/logstash/pipeline/logstash.conf #挂载logstash的配置文件
    depends_on:
      - elasticsearch #kibana在elasticsearch启动之后再启动
    links:
      - elasticsearch:es #可以用es这个域名访问elasticsearch服务
    ports:
      - 4560:4560
```

##### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E4%B8%8A%E4%BC%A0%E5%88%B0linux%E6%9C%8D%E5%8A%A1%E5%99%A8%E5%B9%B6%E4%BD%BF%E7%94%A8docker-compose%E5%91%BD%E4%BB%A4%E8%BF%90%E8%A1%8C)上传到linux服务器并使用docker-compose命令运行

```
docker-compose up -d
```

注意：Elasticsearch 启动可能需要好几分钟，要耐心等待。 ![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:50:23_tech_screen_01-52b03edd.png)

#### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E5%9C%A8logstash%E4%B8%AD%E5%AE%89%E8%A3%85json-lines%E6%8F%92%E4%BB%B6)在logstash中安装json_lines插件

```
# 进入logstash容器
docker exec -it logstash /bin/bash
# 进入bin目录
cd /bin/
# 安装插件
logstash-plugin install logstash-codec-json_lines
# 退出容器
exit
# 重启logstash服务
docker restart logstash
```

#### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E5%BC%80%E5%90%AF%E9%98%B2%E7%81%AB%E5%A2%99%E5%B9%B6%E5%9C%A8kibana%E4%B8%AD%E6%9F%A5%E7%9C%8B)开启防火墙并在kibana中查看

```
systemctl stop firewalld
```

访问地址：http://192.168.3.101:5601

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:50:23_tech_screen_02-14052f31.png)

## [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#springboot%E5%BA%94%E7%94%A8%E9%9B%86%E6%88%90logstash)SpringBoot应用集成Logstash

### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E5%9C%A8pom-xml%E4%B8%AD%E6%B7%BB%E5%8A%A0logstash-logback-encoder%E4%BE%9D%E8%B5%96)在pom.xml中添加logstash-logback-encoder依赖

```
<!--集成logstash-->
<dependency>
    <groupId>net.logstash.logback</groupId>
    <artifactId>logstash-logback-encoder</artifactId>
    <version>5.3</version>
</dependency>
```

### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E6%B7%BB%E5%8A%A0%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6logback-spring-xml%E8%AE%A9logback%E7%9A%84%E6%97%A5%E5%BF%97%E8%BE%93%E5%87%BA%E5%88%B0logstash)添加配置文件logback-spring.xml让logback的日志输出到logstash

> 注意appender节点下的destination需要改成你自己的logstash服务地址，比如我的是：192.168.3.101:4560 。

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE configuration>
<configuration>
    <include resource="org/springframework/boot/logging/logback/defaults.xml"/>
    <include resource="org/springframework/boot/logging/logback/console-appender.xml"/>
    <!--应用名称-->
    <property name="APP_NAME" value="mall-admin"/>
    <!--日志文件保存路径-->
    <property name="LOG_FILE_PATH" value="${LOG_FILE:-${LOG_PATH:-${LOG_TEMP:-${java.io.tmpdir:-/tmp}}}/logs}"/>
    <contextName>${APP_NAME}</contextName>
    <!--每天记录日志到文件appender-->
    <appender name="FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <fileNamePattern>${LOG_FILE_PATH}/${APP_NAME}-%d{yyyy-MM-dd}.log</fileNamePattern>
            <maxHistory>30</maxHistory>
        </rollingPolicy>
        <encoder>
            <pattern>${FILE_LOG_PATTERN}</pattern>
        </encoder>
    </appender>
    <!--输出到logstash的appender-->
    <appender name="LOGSTASH" class="net.logstash.logback.appender.LogstashTcpSocketAppender">
        <!--可以访问的logstash日志收集端口-->
        <destination>192.168.3.101:4560</destination>
        <encoder charset="UTF-8" class="net.logstash.logback.encoder.LogstashEncoder"/>
    </appender>
    <root level="INFO">
        <appender-ref ref="CONSOLE"/>
        <appender-ref ref="FILE"/>
        <appender-ref ref="LOGSTASH"/>
    </root>
</configuration>
```

### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E8%BF%90%E8%A1%8Cspringboot%E5%BA%94%E7%94%A8)运行Springboot应用

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:50:23_tech_screen_03-6e5e33d4.png)

## [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E5%9C%A8kibana%E4%B8%AD%E6%9F%A5%E7%9C%8B%E6%97%A5%E5%BF%97%E4%BF%A1%E6%81%AF)在kibana中查看日志信息

### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E5%88%9B%E5%BB%BAindex-pattern)创建index pattern

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:50:23_tech_screen_04-392f4d69.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:50:23_tech_screen_05-038f1c0a.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:50:23_tech_screen_06-b5ff7766.png)

### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E6%9F%A5%E7%9C%8B%E6%94%B6%E9%9B%86%E7%9A%84%E6%97%A5%E5%BF%97)查看收集的日志

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:50:23_tech_screen_07-49481d40.png)

### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E8%B0%83%E7%94%A8%E6%8E%A5%E5%8F%A3%E8%BF%9B%E8%A1%8C%E6%B5%8B%E8%AF%95)调用接口进行测试

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:50:23_tech_screen_08-0208beae.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:50:23_tech_screen_09-688d0645.png)

### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E5%88%B6%E9%80%A0%E4%B8%80%E4%B8%AA%E5%BC%82%E5%B8%B8%E5%B9%B6%E6%9F%A5%E7%9C%8B)制造一个异常并查看

#### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E4%BF%AE%E6%94%B9%E8%8E%B7%E5%8F%96%E6%89%80%E6%9C%89%E5%93%81%E7%89%8C%E5%88%97%E8%A1%A8%E6%8E%A5%E5%8F%A3)修改获取所有品牌列表接口

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:50:23_tech_screen_10-ad07daa0.png)

#### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E8%B0%83%E7%94%A8%E8%AF%A5%E6%8E%A5%E5%8F%A3%E5%B9%B6%E6%9F%A5%E7%9C%8B%E6%97%A5%E5%BF%97)调用该接口并查看日志

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:50:23_tech_screen_11-63b5329a.png)

### [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E6%80%BB%E7%BB%93)总结

搭建了ELK日志收集系统之后，我们如果要查看SpringBoot应用的日志信息，就不需要查看日志文件了，直接在Kibana中查看即可。

## [#](https://www.macrozheng.com/mall/reference/mall_tiny_elk.html#%E9%A1%B9%E7%9B%AE%E6%BA%90%E7%A0%81%E5%9C%B0%E5%9D%80)项目源码地址

[https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-elk](https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-elk)
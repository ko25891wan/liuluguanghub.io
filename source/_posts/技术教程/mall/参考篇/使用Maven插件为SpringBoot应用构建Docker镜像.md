---
categories:
  - 技术教程
  - mall
  - 参考篇
title: 使用Maven插件为SpringBoot应用构建Docker镜像
date: 2024-01-21 17:29:05
tags:
---
> 本文主要介绍如何使用 Maven 插件将 SpringBoot 应用打包为 Docker 镜像，并上传到私有镜像仓库 Docker Registry 的过程。

## [#](https://www.macrozheng.com/mall/reference/docker_maven.html#docker-registry)Docker Registry

### [#](https://www.macrozheng.com/mall/reference/docker_maven.html#docker-registry-2-0%E6%90%AD%E5%BB%BA)Docker Registry 2.0搭建

```
docker run -d -p 5000:5000 --restart=always --name registry2 registry:2
```

如果遇到镜像下载不下来的情况，需要修改 /etc/docker/daemon.json 文件并添加上 registry-mirrors 键值，然后重启docker服务：

```
{
  "registry-mirrors": ["https://registry.docker-cn.com"]
}
```

### [#](https://www.macrozheng.com/mall/reference/docker_maven.html#docker%E5%BC%80%E5%90%AF%E8%BF%9C%E7%A8%8Bapi)Docker开启远程API

#### [#](https://www.macrozheng.com/mall/reference/docker_maven.html#%E7%94%A8vim%E7%BC%96%E8%BE%91%E5%99%A8%E4%BF%AE%E6%94%B9docker-service%E6%96%87%E4%BB%B6)用vim编辑器修改docker.service文件

```
vi /usr/lib/systemd/system/docker.service
```

需要修改的部分：

```
ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
```

修改后的部分：

```
ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H unix://var/run/docker.sock
```

#### [#](https://www.macrozheng.com/mall/reference/docker_maven.html#%E8%AE%A9docker%E6%94%AF%E6%8C%81http%E4%B8%8A%E4%BC%A0%E9%95%9C%E5%83%8F)让Docker支持http上传镜像

```
echo '{ "insecure-registries":["192.168.3.101:5000"] }' > /etc/docker/daemon.json
```

#### [#](https://www.macrozheng.com/mall/reference/docker_maven.html#%E4%BF%AE%E6%94%B9%E9%85%8D%E7%BD%AE%E5%90%8E%E9%9C%80%E8%A6%81%E4%BD%BF%E7%94%A8%E5%A6%82%E4%B8%8B%E5%91%BD%E4%BB%A4%E4%BD%BF%E9%85%8D%E7%BD%AE%E7%94%9F%E6%95%88)修改配置后需要使用如下命令使配置生效

```
systemctl daemon-reload
```

#### [#](https://www.macrozheng.com/mall/reference/docker_maven.html#%E9%87%8D%E6%96%B0%E5%90%AF%E5%8A%A8docker%E6%9C%8D%E5%8A%A1)重新启动Docker服务

```
systemctl stop docker
systemctl start docker
```

#### [#](https://www.macrozheng.com/mall/reference/docker_maven.html#%E5%BC%80%E5%90%AF%E9%98%B2%E7%81%AB%E5%A2%99%E7%9A%84docker%E6%9E%84%E5%BB%BA%E7%AB%AF%E5%8F%A3)开启防火墙的Docker构建端口

```
firewall-cmd --zone=public --add-port=2375/tcp --permanent
firewall-cmd --reload
```

## [#](https://www.macrozheng.com/mall/reference/docker_maven.html#%E4%BD%BF%E7%94%A8maven%E6%9E%84%E5%BB%BAdocker%E9%95%9C%E5%83%8F)使用Maven构建Docker镜像

> 该代码是在mall-tiny-02的基础上修改的。

### [#](https://www.macrozheng.com/mall/reference/docker_maven.html#%E5%9C%A8%E5%BA%94%E7%94%A8%E7%9A%84pom-xml%E6%96%87%E4%BB%B6%E4%B8%AD%E6%B7%BB%E5%8A%A0docker-maven-plugin%E7%9A%84%E4%BE%9D%E8%B5%96)在应用的pom.xml文件中添加docker-maven-plugin的依赖

```
<plugin>
    <groupId>com.spotify</groupId>
    <artifactId>docker-maven-plugin</artifactId>
    <version>1.1.0</version>
    <executions>
        <execution>
            <id>build-image</id>
            <phase>package</phase>
            <goals>
                <goal>build</goal>
            </goals>
        </execution>
    </executions>
    <configuration>
        <imageName>mall-tiny/${project.artifactId}:${project.version}</imageName>
        <dockerHost>http://192.168.3.101:2375</dockerHost>
        <baseImage>java:8</baseImage>
        <entryPoint>["java", "-jar","/${project.build.finalName}.jar"]
        </entryPoint>
        <resources>
            <resource>
                <targetPath>/</targetPath>
                <directory>${project.build.directory}</directory>
                <include>${project.build.finalName}.jar</include>
            </resource>
        </resources>
    </configuration>
</plugin>
```

相关配置说明：

- executions.execution.phase:此处配置了在maven打包应用时构建docker镜像；
- imageName：用于指定镜像名称，mall-tiny是仓库名称，`${project.artifactId}`为镜像名称，`${project.version}`为仓库名称；
- dockerHost：打包后上传到的docker服务器地址；
- baseImage：该应用所依赖的基础镜像，此处为java；
- entryPoint：docker容器启动时执行的命令；
- resources.resource.targetPath：将打包后的资源文件复制到该目录；
- resources.resource.directory：需要复制的文件所在目录，maven打包的应用jar包保存在target目录下面；
- resources.resource.include：需要复制的文件，打包好的应用jar包。

### [#](https://www.macrozheng.com/mall/reference/docker_maven.html#%E4%BF%AE%E6%94%B9application-yml-%E5%B0%86localhost%E6%94%B9%E4%B8%BAdb)修改application.yml，将localhost改为db

> 可以把docker中的容器看作独立的虚拟机，mall-tiny-docker访问localhost自然会访问不到mysql，docker容器之间可以通过指定好的服务名称db进行访问，至于db这个名称可以在运行mall-tiny-docker容器的时候指定。

```
spring:
  datasource:
    url: jdbc:mysql://db:3306/mall?useUnicode=true&characterEncoding=utf-8&serverTimezone=Asia/Shanghai
    username: root
    password: root
```

### [#](https://www.macrozheng.com/mall/reference/docker_maven.html#%E4%BD%BF%E7%94%A8idea%E6%89%93%E5%8C%85%E9%A1%B9%E7%9B%AE%E5%B9%B6%E6%9E%84%E5%BB%BA%E9%95%9C%E5%83%8F)使用IDEA打包项目并构建镜像

> 注意：依赖的基础镜像需要先行下载，否则会出现构建镜像超时的情况，比如我本地并没有java8的镜像，就需要先把镜像pull下来，再用maven插件进行构建。

- 执行maven的package命令:  
    ![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:44:31_refer_screen_68-851c0aaa.png)
- 构建成功:  
    ![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:44:31_refer_screen_66-7614fff7.png)
- 镜像仓库已有该镜像：  
    ![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:44:31_refer_screen_67-29726867.png)

## [#](https://www.macrozheng.com/mall/reference/docker_maven.html#%E8%BF%90%E8%A1%8Cmall-tiny-docker%E9%A1%B9%E7%9B%AE)运行mall-tiny-docker项目

### [#](https://www.macrozheng.com/mall/reference/docker_maven.html#%E5%90%AF%E5%8A%A8mysql%E6%9C%8D%E5%8A%A1)启动mysql服务

- 使用docker命令启动：

```
  docker run -p 3306:3306 --name mysql \
  -v /mydata/mysql/log:/var/log/mysql \
  -v /mydata/mysql/data:/var/lib/mysql \
  -v /mydata/mysql/conf:/etc/mysql \
  -e MYSQL_ROOT_PASSWORD=root  \
  -d mysql:5.7
```

- 进入运行mysql的docker容器：

```
docker exec -it mysql /bin/bash
```

- 使用mysql命令打开客户端：

```
mysql -uroot -proot --default-character-set=utf8
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:44:31_refer_screen_69-eb940bc2.png)

- 修改root帐号的权限，使得任何ip都能访问：

```
grant all privileges on *.* to 'root'@'%'
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:44:31_refer_screen_70-3cd5a99f.png)

- 创建mall数据库：

```
create database mall character set utf8
```

- 将[mall.sqlopen in new window](https://github.com/macrozheng/mall-learning/blob/master/document/sql/mall.sql)文件拷贝到mysql容器的/目录下：

```
docker cp /mydata/mall.sql mysql:/
```

- 将sql文件导入到数据库：

```
use mall;
source /mall.sql;
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:44:31_refer_screen_71-cacfe7ca.png)

### [#](https://www.macrozheng.com/mall/reference/docker_maven.html#%E5%90%AF%E5%8A%A8mall-tiny-docker%E5%BA%94%E7%94%A8%E6%9C%8D%E5%8A%A1)启动mall-tiny-docker应用服务

- 使用docker命令启动（--link表示应用可以用db这个域名访问mysql服务）：

```
  docker run -p 8080:8080 --name mall-tiny-docker \
  --link mysql:db \
  -v /etc/localtime:/etc/localtime \
  -v /mydata/app/mall-tiny-docker/logs:/var/logs \
  -d mall-tiny/mall-tiny-docker:0.0.1-SNAPSHOT
```

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:44:31_refer_screen_72-f46f9820.png)

- 开启8080端口：

```
firewall-cmd --zone=public --add-port=8080/tcp --permanent
firewall-cmd --reload
```

- 进行访问测试，地址：[http://192.168.3.101:8080/swagger-ui.htmlopen in new window](http://192.168.3.101:8080/swagger-ui.html)![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:44:31_refer_screen_73-c6131735.png)

## [#](https://www.macrozheng.com/mall/reference/docker_maven.html#%E9%A1%B9%E7%9B%AE%E6%BA%90%E7%A0%81%E5%9C%B0%E5%9D%80)项目源码地址

[https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-docker](https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-docker)
---
categories:
  - 技术教程
  - mall
  - 技术要点篇
title: MyBatis Generator使用过程中踩过的一个坑
date: 2024-01-21 17:28:02
tags:
---
> 在使用 MyBatis Generator 生成代码的过程中，曾经遇到一个坑，每次生成 mapper.xml 的时候并不是直接覆盖原文件，而是在原文件中追加了新的内容，导致运行项目出错，本文主要讲解如何解决这个问题。

## [#](https://www.macrozheng.com/mall/technology/mybatis_mapper.html#%F0%9F%91%8D-%E7%9B%B8%E5%85%B3%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B)👍 相关视频教程

- [MyBatis Generator使用教程_上篇open in new window](https://t.zsxq.com/0ewFWZVLU)
- [MyBatis Generator使用教程_下篇open in new window](https://t.zsxq.com/0e4PHbsdL)

## [#](https://www.macrozheng.com/mall/technology/mybatis_mapper.html#%E9%97%AE%E9%A2%98%E9%87%8D%E7%8E%B0)问题重现

### [#](https://www.macrozheng.com/mall/technology/mybatis_mapper.html#%E7%A4%BA%E4%BE%8B%E4%BB%A3%E7%A0%81)示例代码

使用的是mall-tiny-02的代码，代码地址：[https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-02open in new window](https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-02)

### [#](https://www.macrozheng.com/mall/technology/mybatis_mapper.html#%E7%9B%B4%E6%8E%A5%E8%BF%90%E8%A1%8Cmalltinyapplication%E7%9A%84main%E5%87%BD%E6%95%B0)直接运行MallTinyApplication的main函数

发现正常运行，启动成功！ ![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:51:43_refer_screen_99-36bc0c43.png)

### [#](https://www.macrozheng.com/mall/technology/mybatis_mapper.html#%E8%BF%90%E8%A1%8C%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90%E5%99%A8)运行代码生成器

运行com.macro.mall.tiny.mbg.Generator的main方法

### [#](https://www.macrozheng.com/mall/technology/mybatis_mapper.html#%E9%87%8D%E6%96%B0%E5%90%AF%E5%8A%A8malltinyapplication%E7%9A%84main%E5%87%BD%E6%95%B0)重新启动MallTinyApplication的main函数

发现已经无法正常运行，其中有这么一行关键性的错误：

```
nested exception is org.apache.ibatis.builder.BuilderException: Error parsing Mapper XML.
The XML location is 'file [D:\developer\github\mall-learning\mall-tiny-02\target\classes\com\macro\mall\tiny\mbg\mapper\PmsBrandMapper.xml]'.
Cause: java.lang.IllegalArgumentException: Result Maps collection already contains value for com.macro.mall.tiny.mbg.mapper.PmsBrandMapper.BaseResultMap
```

**表明了PmsBrandMapper.xml文件解析错误，BaseResultMap重复定义。**

### [#](https://www.macrozheng.com/mall/technology/mybatis_mapper.html#%E6%9F%A5%E7%9C%8Bpmsbrandmapper-xml%E6%96%87%E4%BB%B6)查看PmsBrandMapper.xml文件

从中可以发现MyBatis Generator生成的mapper.xml文件信息是直接追加在原来的文件上的，并不是直接覆盖，导致了这个错误。 ![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:51:43_refer_screen_100-9245d7ec.png)

## [#](https://www.macrozheng.com/mall/technology/mybatis_mapper.html#%E9%97%AE%E9%A2%98%E8%A7%A3%E5%86%B3)问题解决

> 以前一直以为是MyBatis Generator生成的问题，直接删除mapper.xml所在文件夹，重新生成就好了,现在提供一种MyBatis Generator官方提供的解决方法。

### [#](https://www.macrozheng.com/mall/technology/mybatis_mapper.html#%E5%8D%87%E7%BA%A7mybatis-generator%E7%9A%84%E7%89%88%E6%9C%AC)升级MyBatis Generator的版本

MyBatis Generator 在1.3.7版本提供了解决方案，我们目前使用的版本为1.3.3。

```
<!-- MyBatis 生成器 -->
<dependency>
    <groupId>org.mybatis.generator</groupId>
    <artifactId>mybatis-generator-core</artifactId>
    <version>1.3.7</version>
</dependency>
```

### [#](https://www.macrozheng.com/mall/technology/mybatis_mapper.html#%E5%9C%A8generatorconfig-xml%E6%96%87%E4%BB%B6%E4%B8%AD%E6%B7%BB%E5%8A%A0%E8%A6%86%E7%9B%96mapper-xml%E7%9A%84%E6%8F%92%E4%BB%B6)在generatorConfig.xml文件中添加覆盖mapper.xml的插件

```
<!--生成mapper.xml时覆盖原文件-->
<plugin type="org.mybatis.generator.plugins.UnmergeableXmlMappersPlugin" />
```

### [#](https://www.macrozheng.com/mall/technology/mybatis_mapper.html#%E9%87%8D%E6%96%B0%E8%BF%90%E8%A1%8C%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90%E5%99%A8)重新运行代码生成器

发现PmsBrandMapper.xml生成已经正常，应用也可以正常运行了。 ![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:51:43_refer_screen_101-174f20f6.png)

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:51:43_refer_screen_102-e70f65c3.png)

## [#](https://www.macrozheng.com/mall/technology/mybatis_mapper.html#%E9%A1%B9%E7%9B%AE%E6%BA%90%E7%A0%81%E5%9C%B0%E5%9D%80)项目源码地址

[https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-02](https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-02)
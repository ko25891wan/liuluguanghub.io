---
categories:
  - 技术教程
  - mall
  - 技术要点篇
title: RabbitMQ实现延迟消息居然如此简单，整个插件就完事了！
date: 2024-01-21 17:28:05
tags:
---
> RabbitMQ 实现延迟消息的方式有两种，一种是使用 `死信队列` 实现，另一种是使用 `延迟插件` 实现。`死信队列` 实现我们以前曾经讲过，具体参考 [《mall整合RabbitMQ实现延迟消息》](https://www.macrozheng.com/mall/architect/mall_arch_09.html)，这次我们讲个更简单的，使用 `延迟插件` 实现。

## [#](https://www.macrozheng.com/mall/technology/rabbitmq_delay.html#%E5%AD%A6%E5%89%8D%E5%87%86%E5%A4%87)学前准备

学习本文需要对RabbitMQ有所了解，还不了解的朋友可以看下：[《花了3天总结的RabbitMQ实用技巧，有点东西！》](https://www.macrozheng.com/mall/reference/rabbitmq_start.html)

## [#](https://www.macrozheng.com/mall/technology/rabbitmq_delay.html#%E6%8F%92%E4%BB%B6%E5%AE%89%E8%A3%85)插件安装

> 首先我们需要下载并安装RabbitMQ的延迟插件。

- 去RabbitMQ的官网下载插件，插件地址：https://www.rabbitmq.com/community-plugins.html
    
- 直接搜索`rabbitmq_delayed_message_exchange`即可找到我们需要下载的插件，下载和RabbitMQ配套的版本，不要弄错；
    

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:52:34_rabbitmq_delay_01-dc79c3b6.png)

- 将插件文件复制到RabbitMQ安装目录的`plugins`目录下；

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:52:34_rabbitmq_delay_02-bfcf2a84.png)

- 进入RabbitMQ安装目录的`sbin`目录下，使用如下命令启用延迟插件；

```
rabbitmq-plugins enable rabbitmq_delayed_message_exchange
```

- 启用插件成功后就可以看到如下信息，之后重新启动RabbitMQ服务即可。

![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA8IAAADdBAMAAACCrdZNAAAAD1BMVEUMDAzMzMz/AAAzNjctLi8+VbYGAAANsUlEQVR42uycAZbaMAxEpRtIZ+EC9P6X6hOeZJ6rFNsFutmsBmhi2ZId/5cEonXldpfSpVWEr64ifHUV4aurCF9dRfjqKsJXVxG+uorw1VWEry4SNpmTpfKiVvxPHPu76HZ3Nwnp5BRpLiti0IaQri7qggJ2BvHgvyRF/+4uDGqiBzFtpT9bGNOTerV5v/cTRg+YjZ6CzxDmXKo9PjwiV5N4pfnO8ZL/rBibW/X4ByXGXO7PR4RZxbbT8R19ZPui1GYJZ34rhNHc6BuEFR5Dwtl/mTCdlIRTKFvoz2b7z23H8fWJfU0ThB3NxoSzzWYJJ41mfF5HhM2imEPpTyas7qJuuH8abKbmsttpY9nd5LGLj8NGwu1fxGjx6Y9y8lf3KMDf/cg/k8VWxfZjQswYc7Sd6U/DLuEjHjF2/9gPYQ62OOgr4i8dD/zi7aaM/2iLMj70Qf/iCI5xYDxo0xPmuYiPxXa3OcpqShvaxbuZTRwfsXwOi8sew/p4sGV/03g7hqeH/m3W5Igwx4p9F43KYX/uDjv6pT+vV5yDrm1o4XjgZygzfrRFGR+82D9qlOMguyPC+lfCnN3crpVx34OdR2QkHAbEIKGdxrE/jhLecuQPGTctJsZGPxKwyf7oYx1t6QghTkd4Mj7rtJWOCeOwNBMWR2iOY4Kwu4uqxJZHBTuIxH6bYeVsqo0IMwbid4TVns+IumX/o3sax9gO3HsC7G+esLpwn7/MWtkYP2yhyfj7HGtjiZgkzCu1tTbOPmJLwhwH2yTCYG+YQUtXbtn6ZlRtZb4yYbXmxRiMzxpEeTIjcuyPCJnwHoQxty36mybczQVHLQfXu9B8fMY10urOYURDPwjDQ/U0DrbpCftGyjnafB8mYY6cZe3uOxiihb2/j3K2esLZH28f+GOMsHtPmPdh+kz1Rx8Xdfp3hHjcnLO5+P1c60ZYw57vw+hH/eA+zPrYsk3/TMvh42o4GlwVEXX7Lt3atX1xA2F1UVNzfneMGGEyPtNyN8awLt5je+TvYXQd+D/2O8LqWxCLTYuJMYfruD/aw2iiu3/YQooy4uxzJZPxMdfNz3C1Rn+o9lbPNiDIY47J5TgeW7bJmQed/L2li7/ffFCvr9R/wFfTMVuO8/4x2cKTnXGbNcLqY8IfpOAh+3+E3f48Zl3ufv14nLua6jmOXJ/bVPbwR6kIX11F+Oq6/UqXdKS8ZnOl66K/4pFr+x6KJ85hgA13LbX2HVYdDqIP39LUOWyiKc95RNCXCfsoX5qelBh+McKGNhFJt+fcO12rv+CYJayDLBgrF6XDbJo5yKrs24ZWnxIWtC+dnLCqboRRZ4kw7H8hXFfqCcLukvKcXS50IVcqAjvqBvnSnbB7A+cHhN2L8MvncMpzitEWms2VhmCPzyBf2p/D4v057I5cS7z0gHDhnSec8pwdzZVcKfdpy/nSQ8Jh6O/Dm3QnbEbC6nUfniPM/GImrC6jXOkzwqN8KQmj1QphqW9ak4RJJBGOzShX+ozwOF/qsHEMiXCzexF+mTDznOkcncqVZsI+zJeC6p+EYQthPxOO1vVNa/KZVs5zMpe6lCuNQouFtoN86ZbjVfS5f+fGhs+90KdZVCKgSxF++bm0pVzplz9p0Lo4v4Fwzkcm2//Vebr/fqrc0tVVhK+uInx13e5nWutakg8QPtNa15J8gPCZVkKWpAiXlp9pnWmta+n9ut3PtNa1JB8gfKa1riX5AOEzrXUtQW8lfKa1riXorYTPtNa19H7dfp1prWvp/brdz7TWtSQfIHymta4lqnJLpSJcKsI/QfFd2pM1pyL+fU1xpRe/RiQspmppipGBGBBM9am4CE/tqFH92eyr/4N4vCnTg3N4PRVoS83Zd1atb3g3Yfk04fV4tZL0VcJuXMGCfRDG2mC1xTXFiLmtHTbYHvEQEzhZjr6jGDEi7uYb5SL8m70zTG4QBMKoewM4Sy+Q+5+qRSBbJmoLxcmyfS/jDGW1/fHVjPEFdzzhdi1w80zLMpZU7l1THA88cczjZr2x6M/6N8t+EvYa977+fg5fJ7wFkZE1xYeeuGw6p/OvCW8bCU9LOMZ4nnCUoTXFEoI+q1o9c9ouE5ZYfdR+LAlPOoe384RDGFxTnBPWY9QJX5/DmnDAUNz/Lp2qY2uKRefLMQdPgJCDczjk3/tMnyut8YT1u5N6Ld243rRt3WuK97G65Ndr6VTXa2k9Nm35P0lifO4vPJhl6n3pm9cU443PMZDw5ZpiFgmbA7fkHRL2Dgl75+NhsWcuzEzYYs9cmJmwxY6qQMLQlbC5nrkwOWFzPXNhcsLmeubC5ITN9cyFqQkb7JkLkxM21zMXJidsrmcuzEzYYs9cmJmwxZ658AVuCUgYSPgf8PFA7/nmxA+TsBt+todowbUhYe90+eE0Vqer7jfN6xpgPtuaoscP57E63fqSMi9pnnPYGJ1+eC+FXJfXhAMJm6PHD+dx2adxv8ULk7BBuvxwGadX636LFyZhg/T44ZKkhMb9vr5L44ks0eWH93F1ut/cb54vzpdbJbb4tR/G6y5Knx/G664Hbsk7JOwdEvYOftg7+GHv3GEPsYqWIGHv3OaHcx1v/Hbu88O1ijceYwE/XOp44yEW8MO1jjceYQU/XOt44wGW8MO1jjcexbofrnW88Sgr+GH88fu5xQ9L2hd/bAPckndI2Dsk7J0rP8xjhD3Q74fbUtIQBG+ZMXuo03JQRBZagoQ/2zsDLKdhGIhqOIHmBsAVuABw/0OBa8XCTTAJpOA1M7uhiRQbXv9z4maotbqu+cMea+I5t0bMesGsnjCjvLzrs/AUuuQPw5oPvK9QW2Ie7VCO9TxrCl31h5H5jnDsoxyo3uhMuugPE0ZyRxjcPGMRnk0X/WH3pNf5voh2IjybLvjDwbG/D7O/D8MfbaDK7tPogj8chPu5NLJmcLQ3eKwfrcn0DDrtD2sZrTeq3/aH8XysOhxzSt7S6hLh1SXCq+s7YbeUD6w/P2cNwq/Ex+eCWwlFL3/g8VMCESsiCTc66WA0MGhicEyYA8I8Qxh+kfDYm/b2g9gc5bevtGfte1GEN7qu2f1GmJb607rhjmHbfWzsXDqDLKy9VrQYEjbomVoSBu4kbHcSBrARjpwfE3YX4SFhOMtW3z0anZ51gc0y58ZHbMs/RO++I9z8YmPznDPurR1ru5E33QiTFRwPCJMi/AvCnSecbgIN7V6WuYhFPkzj/jvCWZu4ec4ZR7TL44E33Y1hYzeGo/JeuNI4IqypVhBm7yeho7l3+j3zkYv4EeGKryPsPeGRN90TNn+6D29CI+yehEGN4UrY7eeEQUvCGfslYdAaYfg5wvAB4TjrCmHTVboRRv/O8kcaSThjvyKcsQpkSHjsTbPGcvMd4RqnCA8Jx/1vRxh8vg+zxjo62JHBgwxLpHnGe8KxP/Cmg+oz4RprhP2QcHAv2399R/70hUbvPOGYx5Z4JZA5ECXW8m0uXY/TKy7cCTI942jv2Q6P44E3HefTEP22OXe8FMU+Hnn3kowOaSL8GMOj/yPdCL+desLQxfmJ8NePs+u9HUmljP9XwtKO8Oxv4LvZ/4GTS4RX16evH8jjlP/KE97l75GL8M1j2NHDiSP6rzzhUT4Px7F9PvukCN9E+GnYOLoxPJRfSJ3sLk+CCL+IsM1DmCJ8E2G6ofNtA3vzdHHOM8589Ll5vx6xR3+dTxz7zR82386hCN9GmEeOgLd9lPQ5zzj76degjljd73zi2C+Zvk9dpW8k7GPC5sAZz3jvR6En3PtEGctzIcIvI0zy54SJsWe896NyDWpY9J1/D8mNYNuHJ2GSIvyCMWw/J+x+3jPuxmYlnG0Q3R75y41w+RXhv3mVLtkznvH+6ouMR5ufXqUdz/dhzbTueqZloNGf59Kkt32Yn/aMWfupfToLrv1cOn3itg9n9hn9ivDfey592TPWc+lJdJ0w2hjtY00iPJXkLa0uEV5dIry6fk4YPseyLCJ8A+EL60szfOF9/KLgIvxaJeFLqw9jEL8mET6UCEvXCZ9fX9rAfEKJfBZW4pGmxXFs2aaovLJ23vxjb+e7CIduJnyl/rAh3YHdM+Zc5TS3nXsYGaQnnK6DCIduJnx+fek+h3p0RDiaHvrDxn49ahfhTq8lDP81YbIiQrAk2RHOK7V3NYiLSCbh9IS3c0Q49DrCbmPCvRfsSasbw9FbX6M4Z1fcecL1WDOt0EsJj9eXjgyC/EYYJb6/D2eN4v19OPP1iLpKp24nfGV9acKx+cbp/T7ikWbN5zlBMOfSoFl6wpvnrLn0UK9/Lo1rH2qvf99Yn4dT/4Qwi0bPKrHLt4QP83GOCD9J3pIkwpII/w8S4dWldTxWlwivLtV5WF0ivLpEeHWJ8OoS4dUlwqtLhFeXCK8uEV5dIry6RHh1ifDqEuHVdYLwTypfQvUU3oRGhFlJMiMU4TenIOxH4xT5ElLR5rcnEV5dpXYpjbkWdOyXjUl4uJ70/1yRbH6Vuof5fWAc1SeEjdaTViXnyVUIx9UXwWpPeLSetC7ck6tcpSsteBIm2REerCetMnSTq8y0NnJJuDAbj+FGWHV+J9d2H4Y5NsIIikzC4/Wk/+vqr7Pr0+dW69cJMusCg8jiwOP1pEV4Yp1/Lu1z1BeWXkUYVJHfNyl5S6tLhFeXCK8uEV5dIry4vgEdgBScES2VNAAAAABJRU5ErkJggg==)

## [#](https://www.macrozheng.com/mall/technology/rabbitmq_delay.html#%E5%AE%9E%E7%8E%B0%E5%BB%B6%E8%BF%9F%E6%B6%88%E6%81%AF)实现延迟消息

> 接下来我们需要在SpringBoot中实现延迟消息功能，这次依然沿用商品下单的场景。比如说有个用户下单了，他60分钟不支付订单，订单就会被取消，这就是一个典型的延迟消息使用场景。

- 首先我们需要在`pom.xml`文件中添加`AMQP`相关依赖；

```
<!--消息队列相关依赖-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-amqp</artifactId>
</dependency>
```

- 之后在`application.yml`添加RabbitMQ的相关配置；

```
spring:
  rabbitmq:
    host: localhost # rabbitmq的连接地址
    port: 5672 # rabbitmq的连接端口号
    virtual-host: /mall # rabbitmq的虚拟host
    username: mall # rabbitmq的用户名
    password: mall # rabbitmq的密码
    publisher-confirms: true #如果对异步消息需要回调必须设置为true
```

- 接下来创建RabbitMQ的Java配置，主要用于配置交换机、队列和绑定关系；

```
/**
 * 消息队列配置
 * Created by macro on 2018/9/14.
 */
@Configuration
public class RabbitMqConfig {

    /**
     * 订单延迟插件消息队列所绑定的交换机
     */
    @Bean
    CustomExchange  orderPluginDirect() {
        //创建一个自定义交换机，可以发送延迟消息
        Map<String, Object> args = new HashMap<>();
        args.put("x-delayed-type", "direct");
        return new CustomExchange(QueueEnum.QUEUE_ORDER_PLUGIN_CANCEL.getExchange(), "x-delayed-message",true, false,args);
    }

    /**
     * 订单延迟插件队列
     */
    @Bean
    public Queue orderPluginQueue() {
        return new Queue(QueueEnum.QUEUE_ORDER_PLUGIN_CANCEL.getName());
    }

    /**
     * 将订单延迟插件队列绑定到交换机
     */
    @Bean
    public Binding orderPluginBinding(CustomExchange orderPluginDirect,Queue orderPluginQueue) {
        return BindingBuilder
                .bind(orderPluginQueue)
                .to(orderPluginDirect)
                .with(QueueEnum.QUEUE_ORDER_PLUGIN_CANCEL.getRouteKey())
                .noargs();
    }

}
```

- 创建一个取消订单消息的发出者，通过给消息设置`x-delay`头来设置消息从交换机发送到队列的延迟时间；

```
/**
 * 取消订单消息的发出者
 * Created by macro on 2018/9/14.
 */
@Component
public class CancelOrderSender {
    private static Logger LOGGER =LoggerFactory.getLogger(CancelOrderSender.class);
    @Autowired
    private AmqpTemplate amqpTemplate;

    public void sendMessage(Long orderId,final long delayTimes){
        //给延迟队列发送消息
        amqpTemplate.convertAndSend(QueueEnum.QUEUE_ORDER_PLUGIN_CANCEL.getExchange(), QueueEnum.QUEUE_ORDER_PLUGIN_CANCEL.getRouteKey(), orderId, new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws AmqpException {
                //给消息设置延迟毫秒值
                message.getMessageProperties().setHeader("x-delay",delayTimes);
                return message;
            }
        });
        LOGGER.info("send delay message orderId:{}",orderId);
    }
}
```

- 创建一个取消订单消息的接收者，用于处理订单延迟插件队列中的消息。

```
/**
 * 取消订单消息的处理者
 * Created by macro on 2018/9/14.
 */
@Component
@RabbitListener(queues = "mall.order.cancel.plugin")
public class CancelOrderReceiver {
    private static Logger LOGGER =LoggerFactory.getLogger(CancelOrderReceiver.class);
    @Autowired
    private OmsPortalOrderService portalOrderService;
    @RabbitHandler
    public void handle(Long orderId){
        LOGGER.info("receive delay message orderId:{}",orderId);
        portalOrderService.cancelOrder(orderId);
    }
}
```

- 然后在我们的订单业务实现类中添加如下逻辑，当下单成功之前，往消息队列中发送一个取消订单的延迟消息，这样如果订单没有被支付的话，就能取消订单了；

```
/**
 * 前台订单管理Service
 * Created by macro on 2018/8/30.
 */
@Service
public class OmsPortalOrderServiceImpl implements OmsPortalOrderService {
    private static Logger LOGGER = LoggerFactory.getLogger(OmsPortalOrderServiceImpl.class);
    @Autowired
    private CancelOrderSender cancelOrderSender;

    @Override
    public CommonResult generateOrder(OrderParam orderParam) {
        //todo 执行一系类下单操作，具体参考mall项目
        LOGGER.info("process generateOrder");
        //下单完成后开启一个延迟消息，用于当用户没有付款时取消订单（orderId应该在下单后生成）
        sendDelayMessageCancelOrder(11L);
        return CommonResult.success(null, "下单成功");
    }

    @Override
    public void cancelOrder(Long orderId) {
        //todo 执行一系类取消订单操作，具体参考mall项目
        LOGGER.info("process cancelOrder orderId:{}",orderId);
    }

    private void sendDelayMessageCancelOrder(Long orderId) {
        //获取订单超时时间，假设为60分钟(测试用的30秒)
        long delayTimes = 30 * 1000;
        //发送延迟消息
        cancelOrderSender.sendMessage(orderId, delayTimes);
    }

}
```

- 启动项目后，在Swagger中调用下单接口；

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_22:52:34_rabbitmq_delay_04-98f8ae7d.png)

- 调用完成后查看控制台日志可以发现，从消息发送和消息接收处理正好相差了`30s`，我们设置的延迟时间。

```
2020-06-08 13:46:01.474  INFO 1644 --- [nio-8080-exec-1] c.m.m.t.s.i.OmsPortalOrderServiceImpl    : process generateOrder
2020-06-08 13:46:01.482  INFO 1644 --- [nio-8080-exec-1] c.m.m.tiny.component.CancelOrderSender   : send delay message orderId:11
2020-06-08 13:46:31.517  INFO 1644 --- [cTaskExecutor-4] c.m.m.t.component.CancelOrderReceiver    : receive delay message orderId:11
2020-06-08 13:46:31.520  INFO 1644 --- [cTaskExecutor-4] c.m.m.t.s.i.OmsPortalOrderServiceImpl    : process cancelOrder orderId:11
```

## [#](https://www.macrozheng.com/mall/technology/rabbitmq_delay.html#%E4%B8%A4%E7%A7%8D%E5%AE%9E%E7%8E%B0%E6%96%B9%E5%BC%8F%E5%AF%B9%E6%AF%94)两种实现方式对比

> 我们之前使用过死信队列的方式，这里我们把两种方式做个对比，先来聊下这两种方式的实现原理。

### [#](https://www.macrozheng.com/mall/technology/rabbitmq_delay.html#%E6%AD%BB%E4%BF%A1%E9%98%9F%E5%88%97)死信队列

死信队列是这样一个队列，如果消息发送到该队列并超过了设置的时间，就会被转发到设置好的处理超时消息的队列当中去，利用该特性可以实现延迟消息。

### [#](https://www.macrozheng.com/mall/technology/rabbitmq_delay.html#%E5%BB%B6%E8%BF%9F%E6%8F%92%E4%BB%B6)延迟插件

通过安装插件，自定义交换机，让交换机拥有延迟发送消息的能力，从而实现延迟消息。

### [#](https://www.macrozheng.com/mall/technology/rabbitmq_delay.html#%E7%BB%93%E8%AE%BA)结论

由于死信队列方式需要创建两个交换机（死信队列交换机+处理队列交换机）、两个队列（死信队列+处理队列），而延迟插件方式只需创建一个交换机和一个队列，所以后者使用起来更简单。

## [#](https://www.macrozheng.com/mall/technology/rabbitmq_delay.html#%E9%A1%B9%E7%9B%AE%E6%BA%90%E7%A0%81%E5%9C%B0%E5%9D%80)项目源码地址

https://github.com/macrozheng/mall-learning/tree/master/mall-tiny-delay
---
categories:
  - 技术教程
  - mall
  - 技术要点篇
title: 搞定Mall项目中的权限管理功能，弄懂这些问题就妥了！
date: 2024-01-21 17:27:24
tags:
---
> 最近发现很多朋友问我权限管理功能相关的问题，这里整理了下问的比较多的问题，统一答复下！

## [#](https://www.macrozheng.com/mall/technology/mall_permission_question.html#%F0%9F%91%8D-%E7%9B%B8%E5%85%B3%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B)👍 相关视频教程

- [权限模块数据库表解析open in new window](https://t.zsxq.com/0eyQIie0j)
- [权限模块接口设计与实现_上篇open in new window](https://t.zsxq.com/0e8QESTEU)
- [权限模块接口设计与实现_下篇open in new window](https://t.zsxq.com/0envLA5Wv)

### [#](https://www.macrozheng.com/mall/technology/mall_permission_question.html#mall%E9%A1%B9%E7%9B%AE%E4%B8%AD%E7%9A%84%E6%9D%83%E9%99%90%E7%AE%A1%E7%90%86%E5%8A%9F%E8%83%BD%E6%98%AF%E5%A6%82%E4%BD%95%E5%AE%9E%E7%8E%B0%E7%9A%84)mall项目中的权限管理功能是如何实现的？

之前写过几篇文章，包括了权限管理功能介绍、后端实现和前端实现，看一下基本就清楚了！

- [《大家心心念念的权限管理功能，这次安排上了！》](https://www.macrozheng.com/mall/database/mall_permission.html)
- [《手把手教你搞定权限管理，结合Spring Security实现接口的动态权限控制！》](https://www.macrozheng.com/mall/technology/permission_back.html)
- [《手把手教你搞定权限管理，结合Vue实现菜单的动态权限控制！》](https://www.macrozheng.com/mall/technology/permission_front.html)

### [#](https://www.macrozheng.com/mall/technology/mall_permission_question.html#ums-permission%E8%A1%A8%E8%BF%98%E5%9C%A8%E4%BD%BF%E7%94%A8%E4%B9%88)`ums_permission`表还在使用么？

`ums_permission`表已经不再使用了，不再使用的表还包括`ums_admin_permission_relation`和`ums_role_permission_relation`表，最新版已经移除了相关使用代码。

### [#](https://www.macrozheng.com/mall/technology/mall_permission_question.html#mall%E9%A1%B9%E7%9B%AE%E5%8D%87%E7%BA%A7%E4%BB%A3%E7%A0%81%E5%90%8Eums-resource%E8%A1%A8%E6%89%BE%E4%B8%8D%E5%88%B0)mall项目升级代码后`ums_resource`表找不到？

升级代码以后需要同时导入最新版本的SQL脚本，否则会找不到新创建的表，SQL脚本在项目的`document\sql`文件夹下面。

### [#](https://www.macrozheng.com/mall/technology/mall_permission_question.html#%E5%8F%AA%E5%AE%9E%E7%8E%B0%E4%BA%86%E7%AE%A1%E7%90%86%E5%90%8E%E5%8F%B0%E7%9A%84%E6%9D%83%E9%99%90-%E7%A7%BB%E5%8A%A8%E7%AB%AF%E6%9D%83%E9%99%90%E5%A6%82%E4%BD%95%E5%A4%84%E7%90%86%E7%9A%84)只实现了管理后台的权限，移动端权限如何处理的？

移动端只实现了登录认证，暂时不做权限处理。

### [#](https://www.macrozheng.com/mall/technology/mall_permission_question.html#%E5%9C%A8%E7%AE%A1%E7%90%86%E5%90%8E%E5%8F%B0%E6%B7%BB%E5%8A%A0%E4%BA%86%E4%B8%80%E4%B8%AA%E8%8F%9C%E5%8D%95-%E4%B8%BA%E4%BB%80%E4%B9%88%E5%89%8D%E7%AB%AF%E6%B2%A1%E6%9C%89%E6%98%BE%E7%A4%BA)在管理后台添加了一个菜单，为什么前端没有显示？

只有在前端路由中配置了的菜单，在管理后台添加后才会显示，否则没有效果。

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:01:06_mall_permission_question_01-b6765324.png)

### [#](https://www.macrozheng.com/mall/technology/mall_permission_question.html#%E5%89%8D%E7%AB%AF%E8%B7%AF%E7%94%B1%E4%B8%AD%E4%BF%AE%E6%94%B9%E4%BA%86%E8%8F%9C%E5%8D%95%E5%90%8D%E7%A7%B0-%E4%B8%BA%E4%BB%80%E4%B9%88%E8%BF%98%E6%98%AF%E5%8E%9F%E6%9D%A5%E7%9A%84%E5%90%8D%E7%A7%B0)前端路由中修改了菜单名称，为什么还是原来的名称？

菜单名称、图标、是否隐藏都是由管理后台配置的，当管理后台配置好后，前端修改是无效的。

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:01:06_mall_permission_question_02-56ab03c2.png)

### [#](https://www.macrozheng.com/mall/technology/mall_permission_question.html#mall-swarm%E9%A1%B9%E7%9B%AE%E4%B8%AD%E7%9A%84%E6%9D%83%E9%99%90%E7%AE%A1%E7%90%86%E5%8A%9F%E8%83%BD%E6%98%AF%E5%A6%82%E4%BD%95%E5%AE%9E%E7%8E%B0%E7%9A%84)mall-swarm项目中的权限管理功能是如何实现的？

采用了基于Oauth2的的统一认证鉴权方式，通过认证服务进行统一认证，然后通过网关来统一校验认证和鉴权。具体可以参考：[《微服务权限终极解决方案，Spring Cloud Gateway + Oauth2 实现统一认证和鉴权！》](https://www.macrozheng.com/cloud/gateway_oauth2.html)

### [#](https://www.macrozheng.com/mall/technology/mall_permission_question.html#mall%E5%92%8Cmall-swarm%E9%A1%B9%E7%9B%AE%E4%B8%AD%E6%9D%83%E9%99%90%E7%AE%A1%E7%90%86%E7%9A%84%E5%AE%9E%E7%8E%B0%E6%9C%89%E4%BD%95%E4%B8%8D%E5%90%8C)mall和mall-swarm项目中权限管理的实现有何不同？

mall项目实现方式是Spring Security，相当于把安全功能封装成了一个工具包`mall-security`，然后其他模块通过依赖该工具包来实现权限管理，比如`mall-admin`模块。 mall-swarm项目实现方式是Oauth2+Gateway，采用的是统一的认证和鉴权，mall-swarm中的其他模块只需关注自己的相关业务，而无需依赖任何安全工具包，更加适合微服务架构。

### [#](https://www.macrozheng.com/mall/technology/mall_permission_question.html#mall-swarm%E9%A1%B9%E7%9B%AE%E5%AF%B9%E6%8E%A5%E5%89%8D%E7%AB%AF%E9%A1%B9%E7%9B%AE%E6%97%B6%E4%B8%BA%E4%BB%80%E4%B9%88%E4%BC%9A%E6%8F%90%E7%A4%BA%E4%BD%A0%E5%B7%B2%E7%BB%8F%E8%A2%AB%E7%99%BB%E5%87%BA)mall-swarm项目对接前端项目时为什么会提示你已经被登出？

一种情况是前端访问后端接口没有走网关，首先需要修改`mall-admin-web`项目的后端接口访问基础路径，修改文件为项目`config`目录下的`dev.env.js`，将`BASE_API`改为从网关访问`mall-admin`服务的路径。

```
'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_API: '"http://localhost:8201/mall-admin"'
})
```

另一种情况是没有更新到最新代码，需要更新到最新代码并导入最新版本的SQL。之前在`mall-gateway`项目中配置白名单的时候把`/mall-admin/admin/info`接口配置进去了，会导致无法登录的情况，需要去除掉，目前已经修复了。

```
secure:
  ignore:
    urls: #配置白名单路径
     - "/mall-admin/admin/info"
```

还有一点需要注意的是由于`/mall-admin/admin/info`接口配置已经不在白名单中了，所以需要登录的用户需要配置该接口的相应资源，否则会无法登录。

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:01:06_mall_permission_question_03-4180e966.png)

### [#](https://www.macrozheng.com/mall/technology/mall_permission_question.html#mall-swarm%E9%A1%B9%E7%9B%AE%E5%A6%82%E4%BD%95%E8%AE%BF%E9%97%AE%E9%9C%80%E8%A6%81%E7%99%BB%E5%BD%95%E8%AE%A4%E8%AF%81%E7%9A%84%E6%8E%A5%E5%8F%A3)mall-swarm项目如何访问需要登录认证的接口？

由于项目中存在两套不同的用户体系，后台用户和前台用户，认证中心对多用户体系也有所支持。如何访问需要登录的接口，先调用认证中心接口获取token，不同体系下的用户需要传入不同的`client_id`和`client_secret`，后台用户为`admin-app:123456`，前台用户为`portal-app:123456`。

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:01:06_swarm_deploy_windows_05-2123d535.png)

然后将token添加到请求头中，即可访问需要权限的接口了。

![](https://qiniu.ko25891wan.top/日记软件/obsition/2024-02-02_23:01:06_swarm_deploy_windows_06-1f8d5354.png)

当然对原来的登录接口也做了兼容处理，分别会从内部调用认证中心获取Token，依然可以使用。

- 后台用户登录接口：http://localhost:8201/mall-admin/admin/login
    
- 前台用户登录接口：http://localhost:8201/mall-portal/sso/login
    

### [#](https://www.macrozheng.com/mall/technology/mall_permission_question.html#%E5%8F%AA%E6%83%B3%E5%AD%A6%E4%B9%A0%E6%9D%83%E9%99%90%E7%AE%A1%E7%90%86%E5%8A%9F%E8%83%BD-%E6%9C%89%E6%B2%A1%E6%9C%89%E4%BB%80%E4%B9%88%E7%AE%80%E5%8D%95%E7%9A%84%E9%A1%B9%E7%9B%AE%E5%8F%AF%E4%BB%A5%E5%AD%A6%E4%B9%A0%E4%B8%8B)只想学习权限管理功能，有没有什么简单的项目可以学习下？

可以直接学习下`mall-tiny`项目，`mall-tiny`是一款基于SpringBoot+MyBatis-Plus的快速开发脚手架，拥有完整的权限管理功能，可对接Vue前端，开箱即用。具体参考[《还在从零开始搭建项目？手撸了款快速开发脚手架！》](https://www.macrozheng.com/roadmap/mall_tiny_start.html)
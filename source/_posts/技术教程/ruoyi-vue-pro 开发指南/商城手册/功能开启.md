---
categories:
  - 技术教程
  - ruoyi-vue-pro 开发指南
  - 商城手册
title: 功能开启
date: 2024-02-08 15:40:17
tags:
---
进度说明：

① 项目地址：

- uni-app 商城前端，已经基于 Vue3 重构，对应 [https://gitee.com/yudaocode/yudao-mall-uniapp (opens new window)](https://gitee.com/yudaocode/yudao-mall-uniapp)仓库的 `master` 分支
- 管理后台，请使用 [https://gitee.com/yudaocode/yudao-ui-admin-vue3 (opens new window)](https://gitee.com/yudaocode/yudao-ui-admin-vue3)仓库的 `master` 分支
- 后端项目，请使用 [https://gitee.com/zhijiantianya/ruoyi-vue-pro (opens new window)](https://gitee.com/zhijiantianya/ruoyi-vue-pro)仓库的 `master`（JDK8） 或 `master-jdk21` 分支

② 项目进展：

- 主流程，已经跑通，用户登录、商品信息、订单流程、支付流程、退款流程、店铺装修、优惠劵、秒杀、积分、签到、会员 VIP 等等，可以尝试生产使用
- 非主流程，继续优化，主要是拼团、砍价、分销的 uni-app 部分（后端已经完成），期望是年前搞完

商城的功能，由三部分代码组成：

![功能图](https://doc.iocoder.cn/img/common/mall-feature.png)

- 后端实现，对应 [`yudao-module-mall` (opens new window)](https://github.com/YunaiV/ruoyi-vue-pro/blob/master/yudao-module-mall/)模块
- 管理后台，对应 [`@/views/mall` (opens new window)](https://github.com/yudaocode/yudao-ui-admin-vue3/tree/master/src/views/mall)目录
- 用户前台，对应 [https://github.com/yudaocode/yudao-mall-uniapp (opens new window)](https://github.com/yudaocode/yudao-mall-uniapp)项目

![功能图](https://doc.iocoder.cn/img/common/mall-preview.png)

## [#](https://doc.iocoder.cn/mall/build/#_1-%E5%8A%9F%E8%83%BD%E4%BB%8B%E7%BB%8D)1. 功能介绍

主要拆分四大模块：商品中心、交易中心、营销中心、会员中心。如下图所示：

![功能列表](https://doc.iocoder.cn/img/%E5%95%86%E5%9F%8E%E6%89%8B%E5%86%8C/%E5%8A%9F%E8%83%BD%E5%BC%80%E5%90%AF/%E7%AC%AC%E4%B8%89%E6%AD%A5-01.png)

## [#](https://doc.iocoder.cn/mall/build/#_2-%E5%90%8E%E7%AB%AF%E5%BC%80%E5%90%AF)2. 后端开启

友情提示：

① 商城使用到支付，所以需要参考 [《支付手册》](https://doc.iocoder.cn/pay/build) 文档，将支付功能开启。

② 商城使用到会员，所以需要把 `yudao-module-member` 模块也开启。

考虑到编译速度，默认 `yudao-module-mall` 模块是关闭的，需要手动开启。步骤如下：

- 第一步，开启 `yudao-module-mall` 模块
- 第二步，导入商城的 SQL 数据库脚本
- 第三步，重启后端项目，确认功能是否生效

### [#](https://doc.iocoder.cn/mall/build/#_2-1-%E5%BC%80%E5%90%AF%E6%A8%A1%E5%9D%97)2.1 开启模块

① 修改根目录的 [`pom.xml` (opens new window)](https://github.com/YunaiV/ruoyi-vue-pro/blob/master/pom.xml)文件，取消 `yudao-module-mall` 模块的注释。如下图所示：

![取消  模块的注释](https://doc.iocoder.cn/img/%E5%95%86%E5%9F%8E%E6%89%8B%E5%86%8C/%E5%8A%9F%E8%83%BD%E5%BC%80%E5%90%AF/%E7%AC%AC%E4%B8%80%E6%AD%A5-01.png)

② 修改 `yudao-server` 目录的 [`pom.xml` (opens new window)](https://github.com/YunaiV/ruoyi-vue-pro/blob/master/yudao-server/pom.xml)文件，引入 `yudao-module-mall` 模块。如下图所示：

![引入  模块](https://doc.iocoder.cn/img/%E5%95%86%E5%9F%8E%E6%89%8B%E5%86%8C/%E5%8A%9F%E8%83%BD%E5%BC%80%E5%90%AF/%E7%AC%AC%E4%B8%80%E6%AD%A5-02.png)

③ 点击 IDEA 右上角的【Reload All Maven Projects】，刷新 Maven 依赖。如下图所示：

![刷新 Maven 依赖](https://doc.iocoder.cn/img/%E5%85%AC%E4%BC%97%E5%8F%B7%E6%89%8B%E5%86%8C/%E5%8A%9F%E8%83%BD%E5%BC%80%E5%90%AF/%E7%AC%AC%E4%B8%80%E6%AD%A5-03.png)

### [#](https://doc.iocoder.cn/mall/build/#_2-2-%E7%AC%AC%E4%BA%8C%E6%AD%A5-%E5%AF%BC%E5%85%A5-sql)2.2 第二步，导入 SQL

点击 [`mall-2024-01-17.sql.zip` (opens new window)](https://t.zsxq.com/15mDotnaB)下载附件，解压出 SQL 文件，然后导入到数据库中。

友情提示：↑↑↑ mall.sql 是可以点击下载的！ ↑↑↑

### [#](https://doc.iocoder.cn/mall/build/#_2-3-%E7%AC%AC%E4%B8%89%E6%AD%A5-%E9%87%8D%E6%96%B0%E9%A1%B9%E7%9B%AE)2.3 第三步，重新项目

重启后端项目，然后访问前端的商城菜单，确认功能是否生效。如下图所示：

![确认功能是否生效](https://doc.iocoder.cn/img/%E5%95%86%E5%9F%8E%E6%89%8B%E5%86%8C/%E5%8A%9F%E8%83%BD%E5%BC%80%E5%90%AF/%E7%AC%AC%E4%B8%89%E6%AD%A5-01.png)

至此，我们就成功开启了商城的功能 🙂

## [#](https://doc.iocoder.cn/mall/build/#_3-%E5%89%8D%E7%AB%AF%E5%BC%80%E5%90%AF)3. 前端开启

参考 [《快速启动（前端项目）》](https://doc.iocoder.cn/quick-start-front/) 文档的「2. uni-app 商城移动端」小节。

## [#](https://doc.iocoder.cn/mall/build/#_4-%E6%8E%A8%E8%8D%90%E9%98%85%E8%AF%BB)4. 推荐阅读

微信公众号相关：

- [《微信公众号登录》](https://doc.iocoder.cn/member/weixin-mp-login/)
- [《微信公众号支付接入》](https://doc.iocoder.cn/pay/wx-pub-pay-demo/)

微信小程序相关：

- [《微信小程序登录》](https://doc.iocoder.cn/member/weixin-lite-login/)
- [《微信小程序支付接入》](https://doc.iocoder.cn/pay/wx-lite-pay-demo/)
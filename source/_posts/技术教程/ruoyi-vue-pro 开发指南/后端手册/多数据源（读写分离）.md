---
categories:
  - 技术教程
  - ruoyi-vue-pro 开发指南
  - 后端手册
title: 多数据源（读写分离）
date: 2024-02-08 16:09:50
tags:
---
[`yudao-spring-boot-starter-mybatis` (opens new window)](https://github.com/YunaiV/ruoyi-vue-pro/blob/master/yudao-framework/yudao-spring-boot-starter-mybatis/) 技术组件，除了提供 MyBatis 数据库操作，还提供了如下 2 种功能：

- 数据连接池：基于 [Alibaba Druid (opens new window)](https://github.com/alibaba/druid)实现，额外提供监控的能力。
- 多数据源（读写分离）：基于 [Dynamic Datasource (opens new window)](https://github.com/baomidou/dynamic-datasource-spring-boot-starter)实现，支持 Druid 连接池，可集成 [Seata (opens new window)](https://www.iocoder.cn/Seata/install/?yudao)实现分布式事务。

## [#](https://doc.iocoder.cn/dynamic-datasource/#_1-%E6%95%B0%E6%8D%AE%E8%BF%9E%E6%8E%A5%E6%B1%A0)1. 数据连接池

友情提示：

如果你未学习过 Druid 数据库连接池，可以后续阅读 [《芋道 Spring Boot 数据库连接池入门》 (opens new window)](http://www.iocoder.cn/Spring-Boot/datasource-pool/?yudao)文章。

```
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>druid-spring-boot-starter</artifactId>
</dependency>
```

### [#](https://doc.iocoder.cn/dynamic-datasource/#_1-1-druid-%E7%9B%91%E6%8E%A7%E9%85%8D%E7%BD%AE)1.1 Druid 监控配置

在 [`application-local.yaml` (opens new window)](https://github.com/YunaiV/ruoyi-vue-pro/blob/master/yudao-server/src/main/resources/application-local.yaml)配置文件中，通过 `spring.datasource.druid` 配置项，仅仅设置了 Druid **监控**相关的配置项目，具体数据库的设置需要使用 Dynamic Datasource 的配置项。如下图所示：

![配置项](https://doc.iocoder.cn/img/%E5%A4%9A%E6%95%B0%E6%8D%AE%E6%BA%90/01.png)

### [#](https://doc.iocoder.cn/dynamic-datasource/#_1-2-druid-%E7%9B%91%E6%8E%A7%E7%95%8C%E9%9D%A2)1.2 Druid 监控界面

① 访问后端的 `/druid/index.html` 路径，例如说本地的 `http://127.0.0.1:48080/druid/index.html` 地址，可以查看到 Druid 监控界面。如下图所示：

![Druid 监控界面](https://doc.iocoder.cn/img/%E5%A4%9A%E6%95%B0%E6%8D%AE%E6%BA%90/02.png)

② 访问前端的 [基础设施 -> MySQL 监控] 菜单，也可以查看到 Druid 监控界面。如下图所示：

![Druid 监控界面](https://doc.iocoder.cn/img/%E5%A4%9A%E6%95%B0%E6%8D%AE%E6%BA%90/03.png)

补充说明：

前端 [基础设施 -> MySQL 监控] 菜单，通过 iframe 内嵌后端的 `/druid/index.html` 路径。

如果你想自定义地址，可以前往 [基础设置 -> 配置管理] 菜单，设置 key 为 `url.druid` 配置项。

## [#](https://doc.iocoder.cn/dynamic-datasource/#_2-%E5%A4%9A%E6%95%B0%E6%8D%AE%E6%BA%90)2. 多数据源

友情提示：

如果你未学习过多数据源，可以后续阅读 [《芋道 Spring Boot 多数据源（读写分离）入门》 (opens new window)](http://www.iocoder.cn/Spring-Boot/dynamic-datasource/?yudao)文章。

```
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>dynamic-datasource-spring-boot-starter</artifactId>
</dependency>
```

### [#](https://doc.iocoder.cn/dynamic-datasource/#_2-1-%E5%A4%9A%E6%95%B0%E6%8D%AE%E6%BA%90%E9%85%8D%E7%BD%AE)2.1 多数据源配置

在 [`application-local.yaml` (opens new window)](https://github.com/YunaiV/ruoyi-vue-pro/blob/master/yudao-server/src/main/resources/application-local.yaml#L30-L58)配置文件中，通过 `spring.datasource.dynamic` 配置项，配置了 Master-Slave 主从两个数据源。如下图所示：

![配置项](https://doc.iocoder.cn/img/%E5%A4%9A%E6%95%B0%E6%8D%AE%E6%BA%90/04.png)

### [#](https://doc.iocoder.cn/dynamic-datasource/#_2-2-%E6%95%B0%E6%8D%AE%E6%BA%90%E5%88%87%E6%8D%A2)2.2 数据源切换

#### [#](https://doc.iocoder.cn/dynamic-datasource/#_2-2-1-master-%E6%B3%A8%E8%A7%A3)2.2.1 @Master 注解

在方法上添加 [`@Master` (opens new window)](https://github.com/baomidou/dynamic-datasource-spring-boot-starter/blob/master/src/main/java/com/baomidou/dynamic/datasource/annotation/Master.java)注解，使用名字为 `master` 的数据源，即使用【主】库，一般适合【写】场景。示例如下图：

![@Master 注解](https://doc.iocoder.cn/img/%E5%A4%9A%E6%95%B0%E6%8D%AE%E6%BA%90/05.png)

由于项目的 `spring.datasource.dynamic.primary` 为 `master`，默认使用【主】库，所以无需手动添加 `@Master` 注解。

#### [#](https://doc.iocoder.cn/dynamic-datasource/#_2-2-2-slave-%E6%B3%A8%E8%A7%A3)2.2.2 @Slave 注解

在方法上添加 [`@Slave` (opens new window)](https://github.com/baomidou/dynamic-datasource-spring-boot-starter/blob/master/src/main/java/com/baomidou/dynamic/datasource/annotation/Slave.java)注解，使用名字为 `slave` 的数据源，即使用【从】库，一般适合【读】场景。示例如下图：

![@Slave 注解](https://doc.iocoder.cn/img/%E5%A4%9A%E6%95%B0%E6%8D%AE%E6%BA%90/06.png)

#### [#](https://doc.iocoder.cn/dynamic-datasource/#_2-2-3-ds-%E6%B3%A8%E8%A7%A3)2.2.3 @DS 注解

在方法上添加 [`@DS` (opens new window)](https://github.com/baomidou/dynamic-datasource-spring-boot-starter/blob/master/src/main/java/com/baomidou/dynamic/datasource/annotation/DS.java)注解，使用指定名字的数据源，适合多数据源的情况。示例如下图：

![@DS 注解](https://doc.iocoder.cn/img/%E5%A4%9A%E6%95%B0%E6%8D%AE%E6%BA%90/07.png)

### [#](https://doc.iocoder.cn/dynamic-datasource/#_2-3-%E5%88%86%E5%B8%83%E5%BC%8F%E4%BA%8B%E5%8A%A1)2.3 分布式事务

在使用 Spring `@Transactional` 声明的事务中，无法进行数据源的切换，此时有 3 种解决方案：

① 拆分成多个 Spring 事务，每个事务对应一个数据源。如果是【写】场景，可能会存在多数据源的事务不一致的问题。

② 引入 Seata 框架，提供完整的分布式事务的解决方案，可学习 [《芋道 Seata 极简入门 》 (opens new window)](https://www.iocoder.cn/Seata/install/?yudao)文章。

③ 使用 Dynamic Datasource 提供的 [`@DSTransactional` (opens new window)](https://github.com/baomidou/dynamic-datasource-spring-boot-starter/blob/master/src/main/java/com/baomidou/dynamic/datasource/annotation/DSTransactional.java)注解，支持多数据源的切换，不提供绝对可靠的多数据源的事务一致性（强于 ① 弱于 ②），可学习 [《DSTransactional 实现源码分析 》 (opens new window)](https://www.yinxiang.com/everhub/note/ac0175c8-35f5-4d66-8cd3-c662d7a16441)文章。

## [#](https://doc.iocoder.cn/dynamic-datasource/#_3-%E5%88%86%E5%BA%93%E5%88%86%E8%A1%A8)3. 分库分表

建议采用 ShardingSphere 的子项目 Sharding-JDBC 完成分库分表的功能，可阅读 [《芋道 Spring Boot 分库分表入门 》 (opens new window)](https://www.iocoder.cn/Spring-Boot/sharding-datasource/?yudao)文章，学习如何整合进项目。
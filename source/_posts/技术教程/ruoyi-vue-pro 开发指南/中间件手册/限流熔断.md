---
categories:
  - 技术教程
  - ruoyi-vue-pro 开发指南
  - 中间件手册
title: 限流熔断
date: 2024-02-08 16:10:35
tags:
---
[`yudao-spring-boot-starter-protection` (opens new window)](https://github.com/YunaiV/ruoyi-vue-pro/blob/master/yudao-framework/yudao-spring-boot-starter-protection/) 技术组件，基于 [Resilience4j (opens new window)](https://resilience4j.readme.io/) 轻量级的容错组件，实现熔断器、限流器、舱壁隔离、重试、限时器的功能。

## [#](https://doc.iocoder.cn/server-protection/#_1-%E9%85%8D%E7%BD%AE%E8%AF%B4%E6%98%8E)1. 配置说明

为了保证项目的启动速度，Resilience4j 默认未开启。如果你需要使用，可以通过按照下图操作来开启：

![配置开启](https://doc.iocoder.cn/img/%E9%99%90%E6%B5%81%E7%86%94%E6%96%AD/%E9%85%8D%E7%BD%AE%E5%BC%80%E5%90%AF.png)

## [#](https://doc.iocoder.cn/server-protection/#_2-%E4%BD%BF%E7%94%A8%E7%A4%BA%E4%BE%8B)2. 使用示例

使用的示例，参见 [《芋道 Spring Boot 服务容错 Resilience4j 入门 》 (opens new window)](https://www.iocoder.cn/Spring-Boot/Resilience4j/?yudao)文章。